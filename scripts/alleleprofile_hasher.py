#!/usr/bin/env python3

import os
import pandas as pd  # pandas data frames
import zlib  # for hash
import argparse
from Bio import SeqIO  # read fasta file
#from progressbar import ProgressBar


# written by Carlus Deneke, BfR
# version 0.1

def allele_hash(seq):
    """CRC32 unsigned integer from allele nucleotide sequence.
    The "& 0xffffffff" is used to generate an unsigned integer and
    will generate the same number for both Python 2 and 3
    (https://docs.python.org/2/library/zlib.html#zlib.crc32).

    Args:
        seq (str): nucleotide string

    Returns:
        int: CRC32 checksum as unsigned 32bit integer
    """
    seq = str(seq).encode()
    return zlib.crc32(seq) & 0xffffffff


def compute_hashdb_for_locus(ref_fasta):
    hash_db = {}
    with open(ref_fasta, "r") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            record.allelenumber = record.id.split('_')[-1]
            record.hash = allele_hash(str(record.seq))
            hash_db[record.allelenumber] = record.hash
    hash_db = {str(k): str(v) for k, v in hash_db.items()}  # convert all values in dictionary to string
    return (hash_db)


def hash_alleleprofiles(profile_file, path2db, file_out = "", index_col = "#FILE", verbose = False):
    # read in as pandas data frame
    with open(profile_file) as handle:
        profile = pd.read_csv(handle, sep = "\t", dtype = 'str')  # open always as string to be able to deal with '-' etc

    profile_hashed = []

    for column in profile.drop(columns = index_col):
        profile_locus = profile[column]
        ref_fasta = os.path.join(path2db, column)
        if not os.path.exists(ref_fasta):
            print("file", ref_fasta, "cannot be found")
            exit(1)
        else:
            if verbose == True:
                print("Processing file", ref_fasta)

            locus_hash_db = compute_hashdb_for_locus(ref_fasta)  # get hash of all alleles in locus
            locus_hashed = profile_locus.replace(locus_hash_db)  # replace keeps original value if not found
            # locus_hashed = profile_locus.map(locus_hash_db) # mapping returns NaN if not mapped
            profile_hashed.append(locus_hashed)

    profile_hashed_df = pd.concat(profile_hashed, ignore_index = True, axis = 1)  # merge columns to df
    profile_hashed_df.insert(0, index_col, profile.loc[:, index_col])  # add sample column
    profile_hashed_df.columns = profile.dtypes.index  # add column names

    # write to csv

    if file_out != "":
        dir_out = os.path.dirname(file_out)  # check if dir exists
        if not os.path.exists(dir_out):
            try:
                os.makedirs(dir_out)
            except:
                print("Directory" + dir_out + "could NOT be created")
                sys.exit(1)
        profile_hashed_df.to_csv(file_out, sep = "\t", index = False)
        return ()
    else:
        return (profile_hashed_df)


# main ------------------------------------------------------


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help = 'Path to allele profile in tsv with first column #File', required = True)
    parser.add_argument('-d', '--database', help = 'Path to location of all loci fasta files', required = True)
    parser.add_argument('-o', '--outfile', help = 'Path to new allele profile containing the hash values', required = True)
    parser.add_argument('-v', '--verbose', help = 'Print verbose statements', default = False, action = 'store_true', required = False)
    parser.add_argument('-f', '--force', help = 'Force overwrite of existing result files', default = False, action = 'store_true', required = False)
    args = parser.parse_args()

    # check input
    if not os.path.exists(args.profile):
        print("Profile file", args.profile, "does NOT exist. exit")
        exit(1)

    # check if first column name is #FILE
    with open(args.profile) as handle:
        profile = pd.read_csv(handle, sep = "\t")
        index_col_name = profile.dtypes.index[0]
        # if not profile.dtypes.index[0] == '#FILE':
        # print("Warning: first column header in file",args.profile,"not named #FILE (chewbbaca convention) but",index_col_name )

    if not os.path.exists(args.database):
        print("Database directory", args.database, "does NOT exist. exit")
        exit(1)

    # check whether output exists
    if os.path.exists(args.outfile) and args.force == False:
        print("Output file", args.outfile, "already exists. Use --force to overwrite. Exit")
        exit(1)

    hash_alleleprofiles(profile_file = args.profile, path2db = args.database, file_out = args.outfile, index_col = index_col_name, verbose = args.verbose)

    print("Thank you for using alleleprofile_hasher")


if __name__ == '__main__':
    main()
