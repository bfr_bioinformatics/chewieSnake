#!/usr/bin/env python3

print("""
chewieNS_wrapper.py - a script from the chewieSnake repository:
https://gitlab.com/bfr_bioinformatics/chewieSnake.git
""")

# %% Packages #################################################################

# Python Standard Packages
import sys
import traceback
import warnings
import logging
import argparse
import json
import urllib.request
from urllib.error import URLError
from requests.packages.urllib3.exceptions import InsecureRequestWarning

# ScriptInfo
from pathlib import Path
from datetime import datetime

# Other Packages
import pandas as pd
from helper_functions import *
from utils.chewiens_requests import *


# %% Exception Handling #######################################################

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.error(msg = ''.join(["Uncaught exception: ", *traceback.format_exception(exc_type, exc_value, exc_traceback)]))


# Install exception handler
sys.excepthook = handle_exception


# %% Warning Behaviour ########################################################

def _warning(message, category = UserWarning, filename = '', lineno = -1, file = '', line = ''):
    """
    Omit excessive warning message on STDOUT
    """
    logging.info(msg = message)


warnings.simplefilter('error', RuntimeWarning)  # test with 'always', arm with 'error'
warnings.simplefilter('always', UserWarning)
warnings.showwarning = _warning

# Disable Request Warning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# %% Script Info ##############################################################

class ScriptInfo:
    repo_path: Path
    script_name: str
    timestamp: datetime
    debug_switch: bool

    def __init__(self, debug_switch: bool = False):
        self.repo_path = Path(__file__).resolve().parent.parent
        self.script_name = Path(__file__).name
        self.timestamp = datetime.now()
        self.debug_switch = debug_switch


global script_info


# %% Define Source of Arguments ###############################################

def parse_arguments() -> argparse.Namespace:
    global script_info
    parser = argparse.ArgumentParser()

    # optional switch
    parser.add_argument('-q', '--query', default = False, action = 'store_true', required = False, help = 'Query available full species names')
    # required input
    parser.add_argument('-s', '--species', default = 'Salmonella enterica', type = str, required = False, help = 'Full species name; default: "Salmonella enterica"')
    # params
    parser.add_argument('-up', '--use_proxy', default = False, action = 'store_true', required = False, help = 'Use proxy for requests; default: False')
    parser.add_argument('-pu', '--proxy_url', default = '', type = str, required = False, help = 'Use proxy dictionary for requests instead of default environment settings')
    # output
    parser.add_argument('-d', '--schema_dir', default = script_info.repo_path / "schema", type = Path, required = False, help = f'Path to the schema parent directory; default: {script_info.repo_path / "schema"}')
    # logging
    parser.add_argument('-l', '--log_dir', default = script_info.repo_path / "schema" / "log", type = Path, required = False, help = f'Path for the logfile; default: {script_info.repo_path / "schema" / "log"}')

    _args = parser.parse_args()

    return _args


# Create Arguments for Debugging
def create_arguments() -> argparse.Namespace:
    global script_info
    debug_proxy = True
    debug_proxy_url = {'http': 'http://proxy.bfr.bund.de:8080', 'https': 'http://proxy.bfr.bund.de:8080'}
    debug_species = "Salmonella_enterica"  # "Escherichia_coli" "Listeria_monocytogenes" "Salmonella_enterica"

    _args = argparse.Namespace(
        query = True,
        species = debug_species,
        use_proxy = debug_proxy,
        proxy_url = debug_proxy_url,
        schema_dir = script_info.repo_path / "schema",
        log_dir = script_info.repo_path / "schema" / "log"
    )

    return _args


# Translate Snakemake to ArgParse Object
def snakemake_to_args(snakemake) -> argparse.Namespace:
    _args = argparse.Namespace(
        query = snakemake.input['query'],
        species = snakemake.input['species'],
        use_proxy = snakemake.output['use_proxy'],
        schema_dir = snakemake.output['schema_dir'],
        log_dir = snakemake.log['log_dir']
    )
    return _args


# Define Source of Arguments
def python_or_snakemake_args() -> argparse.Namespace:
    global script_info
    try:
        snakemake.input[0]
    except NameError:
        if script_info.debug_switch:
            _args = create_arguments()
        else:
            _args = parse_arguments()
    else:
        _args = snakemake_to_args(snakemake = snakemake)
    return _args


# %% REST Functions ###########################################################

def get_chewie_ns(base_url: str = "https://chewbbaca.online/NS/api", content_type: str = "json") -> dict:
    """
    get_chewie_ns
    :param base_url:
    :param endpoint:
    :param content_type:
    :return:
    """
    endpoint = '/stats/species'
    request_parameters = ''
    url = base_url + endpoint + request_parameters
    content_type_str = 'application/octet-stream' if content_type == "byte" else 'application/json'
    headers = {
        'Content-Type' : content_type_str,
        'Cache-Control': 'no-cache',
    }

    # Request
    logging.info(f'get_chewie_ns() contacting API at endpoint: {endpoint}')
    req = urllib.request.Request(url = url, headers = headers)
    req.get_method = lambda: 'GET'

    try:
        response = urllib.request.urlopen(req)
    except URLError as e:
        warnings.warn(message = f"get_chewie_ns() URLError: {e.reason.strerror}", category = UserWarning)
        return {'status': e.reason.strerror}
    except Exception as e:
        response = e

    response_read = response.read().decode("utf-8")
    response_code = response.getcode()

    if response.getcode() == 200:
        response_dict = json.loads(response_read)
        logging.info(f'get_chewie_ns() Response: {response_dict}')
    else:
        response_dict = {}
        error_string = json.dumps(json.loads(response_read), indent = 4, sort_keys = True)
        logging.critical(msg = f"get_chewie_ns() Server Error {response_code}:\n{error_string}")

    return response_dict


# %% Main #####################################################################

def main():
    # %% Setup #####

    # Script Information
    global script_info
    script_info = ScriptInfo(debug_switch = False)

    # Parse Arguments
    args = python_or_snakemake_args()

    # Configure Logging
    args.log_dir.mkdir(parents = True, exist_ok = True)
    log_file = args.log_dir / script_info.script_name.replace(".py", ".log")
    logging.basicConfig(filename = log_file,
                        # encoding = 'utf-8',  # NOTE: enabled since upgrade to python v3.9
                        level = logging.INFO,
                        format = '%(asctime)s %(levelname)-8s %(message)s',
                        datefmt = '%Y-%m-%d %H:%M:%S')

    # add additional STDOUT handler for logger
    root = logging.getLogger()
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    root.addHandler(handler)

    # Proxy
    if args.use_proxy:
        proxies = {'http': args.proxy_url, 'https': args.proxy_url} if args.proxy_url else urllib.request.getproxies()
    else:
        proxies = None

    # %% Workflow #####

    logging.info(f"[START] {__file__}")
    warnings.warn(message = f"{script_info.script_name} is running in DEBUG mode", category = UserWarning) if script_info.debug_switch else None
    logging.info(msg = args) if script_info.debug_switch else None
    logging.info(msg = f"Using proxy settings: {proxies}") if args.use_proxy else None

    # Get species list #####

    # define chewiens_requests parameters
    base_url = "https://chewbbaca.online/NS/api"
    headers_get = {
        'accept'       : 'application/octet-stream',
        'Cache-Control': 'no-cache',
        'Authorization': None
    }
    headers_get_json = {
        'accept'       : 'application/json',
        'Cache-Control': 'no-cache',
        'Authorization': None
    }

    species_dict = species_list(base_url = base_url, headers_get = headers_get, endpoint_list = ["stats", "species"], proxies = proxies)

    if args.query:
        species_df = pd.DataFrame.from_dict(species_dict, orient = "index").sort_index()
        species_df.to_string(sys.stdout, header = False, index = True, float_format = '%.4f')
        print()
        sys.exit(0)

    # Perform specific Request #####
    species_schema_dir = args.schema_dir / args.species.replace(' ', '_')
    unique_receipt = species_schema_dir / f"{args.species.replace(' ', '_')}_{script_info.timestamp.strftime('%Y%m%d-%H%M%S')}.json"
    species_schema_dir.mkdir(exist_ok = True)
    schema_id = "1"

    # species_response = get_chewie_ns(url_ns = "/stats/species")
    # species_dict_clean = [{k: v['value'] for k,v in element.items()} for element in species_response['message']]
    # species_id = [os.path.basename(element['species']) for element in species_dict_clean if element['name'] == args.species].pop()
    # schema_id = [element['schemas'] for element in species_dict_clean if element['name'] == args.species][0]

    species_id = species_dict[args.species]

    species_url, schemas_response = simple_get_request(url = base_url, headers = headers_get, endpoint_list = ["species", species_id, "schemas", schema_id], proxies = proxies)
    schema_clean = [{k: v['value'] for k, v in element.items()} for element in schemas_response.json()][0]

    # ptf_url, prodigal_response = simple_get_request(url = species_url, headers = headers_get, endpoint_list = ["ptf"])
    # with open(f"{species_schema_dir}/{args.species.replace(' ', '_')}.trn", "wb") as f:
    #     f.write(prodigal_response.content)

    logging.info(f"Downloading prodigal training file for {args.species}")  # TODO: this may be unnecessary because compressed schema zips often contain trn file
    ptf_url, prodigal_response = simple_get_request(url = base_url, headers = headers_get, endpoint_list = ["download", "prodigal_training_files", schema_clean["prodigal_training_file"]])
    with open(f"{species_schema_dir}/{args.species.replace(' ', '_')}.trn", "wb") as f:
        f.write(prodigal_response.content)

    logging.info(f"Downloading compressed schema #{schema_id} ({schema_clean['name']}) for {args.species}")
    compressed_schemas_url, compressed_schemas_response = simple_get_request(url = base_url, headers = headers_get, endpoint_list = ["download", "compressed_schemas", species_id, schema_id, schema_clean["last_modified"]])  # Note: maybe dateEntered
    with open(f"{species_schema_dir}/{args.species.replace(' ', '_')}_{schema_clean['name']}.zip", "wb") as f:
        f.write(compressed_schemas_response.content)

    schema_clean.update(
        {
            'Schema_path'               : species_schema_dir,
            'Schema_url'                : compressed_schemas_url,
            'prodigal_training_file_url': compressed_schemas_url
        }
    )

    logging.info(f"Download receipt is saved to {unique_receipt}")
    with open(unique_receipt, 'w') as file:
        json.dump(
            obj = strictJsonObj(
                nestedObj = json.loads(
                    s = nestedDict_to_json(
                        nestedObj = schema_clean)),
                uriCompliant = False),
            fp = file,
            indent = 4,
            sort_keys = False,
            ensure_ascii = False,
            allow_nan = False)

    logging.info(f"[STOP] {__file__}")


# %% Main Call ################################################################

if __name__ == '__main__':
    main()
