#!/usr/bin/env python3

print('''
json_writer.py - a script from the chewieSnake pipeline:
https://gitlab.com/bfr_bioinformatics/chewieSnake.git
''')

# GOAL: export allele profiles to json
# Components
# * sample information
# * allele statistics
# * allele profile (hashed; or full allele sequence)
# * detailed allele properties: source of error,


# input
# Details on contig info
# /home/carlus/BfR/Projects/Pipeline_test/chewieSnake/cgmlst/GMI-17-001-DNA/results_contigsInfo.tsv
# link with contig depth

# allele details unhashed
# "/home/carlus/BfR/Projects/Pipeline_test/chewieSnake/cgmlst/GMI-17-001-DNA/results_alleles.tsv"

# paralogs:
# /home/carlus/BfR/Projects/Pipeline_test/chewieSnake/cgmlst/GMI-17-001-DNA/RepeatedLoci.txt

json_version = "0.0.5"

# %% Packages

# Python Standard Packages
import sys
import os
import logging
import traceback
import socket
import glob
import argparse
import datetime
import json
import hashlib
# from time import strftime, localtime

# Other Packages
import pandas as pd
import numpy as np
import yaml


# %% Debugging and Logging

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.error(''.join(["Uncaught exception: ", *traceback.format_exception(exc_type, exc_value, exc_traceback)]))


def enable_logging(logfilename):
    if logfilename:
        logging.basicConfig(filename = logfilename,
                            format = '%(asctime)s %(levelname)-8s %(message)s',
                            datefmt = '%Y-%m-%d %H:%M:%S',
                            # encoding='utf-8',  # TODO: enable this with upgrade to python v3.9
                            level = logging.INFO)
    else:
        logging.basicConfig(format = '%(asctime)s %(levelname)-8s %(message)s',
                            datefmt = '%Y-%m-%d %H:%M:%S',
                            # encoding='utf-8',  # TODO: enable this with upgrade to python v3.9
                            level = logging.INFO,
                            handlers = [logging.StreamHandler()])


def logprint(string):
    logging.info(string)
    print(string)


# %% Functions

def convert_np2number(obj):
    if isinstance(obj, np.integer):
        return int(obj)
    elif isinstance(obj, np.floating):
        return float(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()
    elif isinstance(obj, datetime.datetime):
        return obj.__str__()


def checksum_md5(filename, buffer_size = (128 * (2 ** 9))):
    """Create MD5 checksum of file.

    Args:
        filename (str): file path
        buffer_size (int): read data in 64k chunks, exponent is variable

    Returns:
        str: HEX digest
    """
    md5sum = hashlib.md5()
    with open(filename, 'rb') as f:
        for chunk in iter(lambda: f.read(buffer_size), b''):
            md5sum.update(chunk)
    return md5sum.hexdigest()


def checksum_md5s(string):
    """Create MD5 checksum of string.

    Args:
        string (str): string

    Returns:
        str: HEX digest
    """
    md5sum = hashlib.md5()
    md5sum.update(string.encode('utf-8'))
    return md5sum.hexdigest()


def compute_locus_stats(stats):
    """Computes allele quality metrics from chewBBaca."""
    loci_found = stats['EXC'] + stats['INF']
    loci_missing = stats['LNF'] + stats['PLOT'] + stats['NIPH'] + stats['ALM'] + stats['ASM']
    loci_total = loci_found + loci_missing
    loci_missing_fraction = loci_missing / loci_total
    loci_info = {
        'loci_found'           : loci_found.iloc[0],
        'loci_missing'         : loci_missing.iloc[0],
        'loci_total'           : loci_total.iloc[0],
        'loci_missing_fraction': loci_missing_fraction.iloc[0]
    }
    return loci_info


def collect_allele_profile(allele_profile_file):
    """Load allele profile of single sample."""
    profile = pd.read_csv(allele_profile_file, sep = "\t", dtype = {0: str}).set_index('#FILE', inplace = False).transpose()  # use two steps to set first column as index_col in case samples are integers or a scientific notation

    # check that only single sample
    if profile.shape[1] > 1:
        logprint("Only profile files containing a single sample (single profile row) are permitted.")
        sys.exit(1)

    profile.columns = profile.columns.astype('str', copy = False)  # in case samples are integers, need to convert them to str
    profile.columns.values[0] = 'allele_crc32'

    profile.index.names = ['locus']
    profile.reset_index(level = 0, inplace = True)
    # profile['locus'] = profile['locus'].str.replace('.fasta', '')
    allele_numbers = profile.to_dict("records")
    return allele_numbers


def collect_allele_stats(args, config):
    """Load allele stats."""
    stats = pd.read_csv(args.allele_stats, sep = "\t", dtype = {0: str}).set_index('sample', inplace = False)  # use two steps to set first column as index_col in case samples are integers or a scientific notation
    stats_locus = compute_locus_stats(stats)

    # args.remove_frameshifts # NOTE does this change the allele stats? How?

    json_module_allele_stats = {
        **stats.to_dict(orient = 'records')[0],
        **stats_locus,
        'hashid'                   : extract_from_tsv(args.sample_name, "hashID", args.hashid),
        'max_fraction_missing_loci': config["parameters"]["chewbbaca"]["max_fraction_missing_loci"],
        'allele_qc'                : "PASS" if stats_locus['loci_missing_fraction'] <= config["parameters"]["chewbbaca"]["max_fraction_missing_loci"] else "FAIL"
    }
    return json_module_allele_stats


def create_scheme_information(config, do_write = False):
    scheme_information = {
        'scheme_name': os.path.basename(config["parameters"]["chewbbaca"]["scheme_dir"]),
        'species'    : os.path.basename(config["parameters"]["chewbbaca"]["prodigal_training_file"]).strip(".trn"),
        'scheme_dir' : config["parameters"]["chewbbaca"]["scheme_dir"],
        'scheme_md5' : create_scheme_hash(loci = glob.glob1(config["parameters"]["chewbbaca"]["scheme_dir"], "*.fasta")),
        'count_loci' : len(glob.glob1(config["parameters"]["chewbbaca"]["scheme_dir"], "*.fasta"))
    }

    if (do_write):
        scheme_yaml = os.path.join(config["parameters"]["chewbbaca"]["scheme_dir"], "info", "scheme_info.yaml")
        os.makedirs(os.path.dirname(scheme_yaml), exist_ok = True)
        with open(scheme_yaml, 'w') as outfile:
            yaml.dump(data = scheme_information,
                      stream = outfile,
                      default_flow_style = False,
                      sort_keys = False)
    return scheme_information


def create_scheme_hash(loci: list) -> str:
    """Hash of loci names.

    All locus names of a scheme are sorted and separated by a whitespace, then MD5 hashed.

    Args:
        scheme_dir (str): file path

    Returns:
        str: MD5 digest
    """
    loci_names = [locus.strip(".fasta") for locus in loci]
    loci_names.sort()
    loci_string = ' '.join(loci_names)
    loci_hash = checksum_md5s(string = loci_string)
    return loci_hash


def get_fasta_path(sample_name, config):
    """Extract cell value from sample sheet tsv.

    First column is the sample name.

    Args:
        sample_name (str): string in first column
        config (dict): config_chewiesnake
    """
    samples = pd.read_csv(config["samples"], sep = "\t", dtype = str).set_index('sample', inplace = False)  # use two steps to set first column as index_col="sample" in case samples are integers or a scientific notation; sample sheet is all str
    fasta_path = samples.loc[sample_name, 'assembly']
    return fasta_path


def extract_from_tsv(sample_name, column, tsv_file):
    """Extract cell value from tsv.

    First column is the sample name.

    Args:
        sample_name (str): string in first column
        column (str): header
        tsv_file (str): file path
    """
    data = pd.read_csv(tsv_file, sep = "\t", dtype = {0: str}).set_index('sample', inplace = False)  # use two steps to set first column as index_col="sample" in case samples are integers or a scientific notation
    myvalue = data.loc[sample_name, column]

    return myvalue


def collect_run_metadata(args, config):
    """Make a sample description."""

    # Module Sample Description

    # Variant A: get values from args
    timestamp = extract_from_tsv(sample_name = args.sample_name, column = "date", tsv_file = args.timestamps)
    sample_name = args.sample_name
    profile_loci = [item['locus'] for item in collect_allele_profile(allele_profile_file = args.allele_profile) if "locus" in item]

    # Variant B: get values from allele_profile
    # timestamp = strftime("%Y-%m-%d", localtime(os.path.getmtime(args.allele_profile)))
    # profile = pd.read_csv(args.allele_profile, sep = "\t", dtype = {0: str}).set_index('#FILE', inplace = False)
    # sample_name = ''.join(profile.index).strip('.fasta')

    json_module_sample_description = {
        'sample'            : sample_name,
        'fasta_file'        : get_fasta_path(args.sample_name, config),
        'fasta_md5'         : checksum_md5(filename = get_fasta_path(args.sample_name, config)),
        'allele_profile'    : args.allele_profile,
        'allele_profile_md5': create_scheme_hash(loci = profile_loci),
        'timestamp_analysis': timestamp
    }

    # Module Pipeline Metadata
    json_module_pipeline_metadata = {
        'host_system': socket.gethostname(),
        'config'     : config
    }

    # Module Scheme Information
    scheme_yaml = os.path.join(config["parameters"]["chewbbaca"]["scheme_dir"], "info", "scheme_info.yaml")
    if os.path.exists(scheme_yaml):
        logprint("File " + scheme_yaml + " exists.")
        with open(scheme_yaml, "r") as handle:
            json_module_scheme_information = yaml.load(handle, Loader = yaml.FullLoader)
    else:
        json_module_scheme_information = create_scheme_information(config, do_write = True)

    # Module Technical Metadata
    json_module_run_metadata = {
        'pipeline'            : "chewieSnake",
        'timestamp'           : str(datetime.datetime.now()),
        'sample_description'  : json_module_sample_description,
        'pipeline_metadata'   : json_module_pipeline_metadata,
        'database_information': json_module_scheme_information
    }
    return json_module_run_metadata


# %% Main Function

def main():
    parser = argparse.ArgumentParser()

    # sample info
    parser.add_argument('--sample_name', help = 'Sample name', required = True, type = str)
    # required files  # actually it is sufficient to provide sample name and config file
    parser.add_argument('--allele_profile', '-p', help = 'Path to hashed allele profile for a single sample (tsv)', required = True, type = os.path.abspath)
    parser.add_argument('--allele_stats', '-s', help = 'Path to allele stats for a single sample (tsv)', required = True, type = os.path.abspath)
    parser.add_argument('--timestamps', '-t', help = 'Path to timestamps file for a single sample (tsv)', required = True, type = os.path.abspath)
    parser.add_argument('--hashid', help = 'Path to hashid file for all samples (tsv)', default = "", required = True, type = os.path.abspath)  # make optional?
    parser.add_argument('--config', help = 'Path to config.yaml', required = True, type = os.path.abspath)
    # output
    parser.add_argument('--outfile', '-o', help = 'Output file', required = True, type = os.path.abspath)
    parser.add_argument('--logfile', '-l', help = 'Logfile', required = False, type = os.path.abspath)
    # other
    # parser.add_argument('--scheme_information', help='Path to predefined scheme information (yaml)', required=True, type=os.path.abspath)
    # parser.add_argument('--pipeline_metadata', help='Path to predefined pipeline metadata (yaml)', required=True, type=os.path.abspath)
    # parser.add_argument('--json_input', help='Input pipeline metadata and scheme information are in json instead of yaml format', default=False, action='store_true',  required=False)
    # parser.add_argument('--yaml', help='Output yaml instead of json', default=False, action='store_true',  required=False)

    if not True:  # remove 'not' for debugging
        debug_sample = "GMI-17-001-DNA"
        debug_basepath = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "test_data/chewiesnake/test_202201231700_wrapper")
        args = argparse.Namespace(
            sample_name = debug_sample,
            allele_profile = os.path.join(debug_basepath, "cgmlst", debug_sample, "allele_profiles.tsv"),
            allele_stats = os.path.join(debug_basepath, "cgmlst", debug_sample, "allele_statistics.tsv"),
            timestamps = os.path.join(debug_basepath, "cgmlst", debug_sample, "timestamps.tsv"),
            hashid = os.path.join(debug_basepath, "cgmlst", "hashids.tsv"),
            config = os.path.join(debug_basepath, "config_chewiesnake.yaml"),
            logfile = os.path.join(debug_basepath, "cgmlst", debug_sample, "outfile.log"),
            outfile = os.path.join(debug_basepath, "cgmlst", debug_sample, "outfile.json")
        )
    else:
        args = parser.parse_args()

    # enable logging
    sys.excepthook = handle_exception  # Install exception handler
    if args.logfile:
        enable_logging(logfilename = args.logfile)
    else:
        enable_logging(logfilename = None)
    logprint("This is chewieSnake json_writer version " + json_version)

    # collect more infos from config.yaml
    with open(args.config, "r") as handle:
        config = yaml.load(handle, Loader = yaml.FullLoader)

    # TODO make some checks
    # sample_name in allele profile
    # sample_name in allele stats
    # sample_name in sample sheet
    # sample_name in time stamps

    # IDEA
    # This could also work for multi allele files when selecting the sample from first column (using pd.loc(sample_name,col))

    # %% JSON Aggregation

    json_assembly = {
        'run_metadata'  : collect_run_metadata(args, config),
        'allele_profile': collect_allele_profile(allele_profile_file = args.allele_profile),
        'allele_stats'  : collect_allele_stats(args, config)
    }

    # integrity check
    if json_assembly['run_metadata']['database_information']['scheme_md5'] == json_assembly['run_metadata']['sample_description']['allele_profile_md5']:
        logprint("Scheme and profile locus names are identical.")
    else:
        logprint("WARNING: Allele profile locus names do not have the same MD5 digest as the schema.")

    # %% Export JSON Output

    logprint("Writing output to: " + args.outfile)
    logprint("WARNING: outfile " + args.outfile + " exists and will be overwritten") if os.path.exists(path = args.outfile) else None
    os.makedirs(name = os.path.dirname(args.outfile), exist_ok = True)

    with open(args.outfile, "w") as file:
        json.dump(obj = json_assembly,
                  fp = file,
                  indent = 4,
                  sort_keys = False,
                  ensure_ascii = False,
                  allow_nan = False,
                  default = convert_np2number)
    logprint("Thank you for using the chewieSnake to JSON converter.")


# %% Call

if __name__ == '__main__':
    main()

# %% Scribble

# define outname
# if args.outdir != "/":
# outdir = args.outdir
# print("outdir specified as " + outdir)
# else:
# outdir = os.path.dirname(args.allele_profile)
# print("outdir inferred from file " + outdir)

# if args.sample_name != "NA":
# sample_name = args.sample_name
# print("Sample name provided")
# else:
# sample_name = run_metadata['sample_description']['sample_name']
# print("Sample name inferred from technical metadata")

# sample_info
# name
# name, hashed
# analysis date
# sender
# contact
# assembler
# e.g. shovill
# assembler_version: software version
# assembler_parameters: spades, kmers, extra, defaults
# trimming
# e.g. fastp
# trimming parameters: minimum length, quality
# sequencing instrument
# sequencing kit
# average depth
# allele caller
# e.g.
# allele_caller version
# allele_caller
# scheme name
# scheme genes
# scheme data
# scheme source
# sample metadata
