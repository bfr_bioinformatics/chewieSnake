#!/usr/bin/env python3

# [Goal]  hash_ID from unique combination of alleles: hashed sequence type

# written by Carlus Deneke, BfR
# version 0.1

import os
import argparse
import zlib  # for hash
import pandas as pd  # pandas data frames #requires pandas 1.1.14 or higher


def get_hashid(profile, index_col_name):
    df_names = profile.iloc[:, 0]
    df = profile.drop(columns = index_col_name)
    df_concat = df.apply(lambda x: ':'.join(x.dropna().values.tolist()), axis = 1)
    hashid_multi = [(zlib.crc32(str(profile_concat).encode()) & 0xffffffff) for profile_concat in df_concat]
    hashid_multi = dict(zip(df_names, hashid_multi))

    return (hashid_multi)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--profile', help = 'Path to allele profile in tsv with first column #File', required = True)
    parser.add_argument('-o', '--outfile', help = 'Path to hash id output containing a hash sequence type', required = True)
    parser.add_argument('-f', '--force', help = 'Force overwrite of existing result files', default = False, action = 'store_true', required = False)
    args = parser.parse_args()

    # check input
    if not os.path.exists(args.profile):
        print("Profile file", args.profile, "does NOT exist. exit")
        exit(1)

    # check whether output exists
    if os.path.exists(args.outfile) and args.force == False:
        print("Output file", args.outfile, "already exists. Use --force to overwrite. Exit")
        exit(1)

    with open(args.profile) as handle:
        myprofile = pd.read_csv(handle, sep = "\t", dtype = 'str')
        index_col_name = myprofile.dtypes.index[0]

    myhashid = get_hashid(myprofile, index_col_name = index_col_name)

    # write
    with open(args.outfile, 'w') as f:
        f.write("sample\thashID\n")
        for key in myhashid.keys():
            f.write("%s\t%s\n" % (key, myhashid[key]))

    print("Thank you for using hashID")


if __name__ == '__main__':
    main()
