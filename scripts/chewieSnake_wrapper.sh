#!/bin/bash

## [Goal] Run chewieSnake in batches; Wrapper for chewieSnake.py, direct Snakemake calls or Docker container
## [Author] Holger Brendebach
pipeline="chewieSnake"

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name [OPTIONS]

${bold}Description:${normal}
Run ${pipeline} in batch mode based on entries in the ${pipeline}_runs.tsv table.
Any script arguments that do not match the ones in the next section will be appended to the run command, e.g. "--unlock".
This wrapper may evaluate several run strategies in its run_modules() function: ${pipeline}.py, direct Snakemake calls or Docker container runs.

${bold}Optional Named Arguments:${normal}
  -e, --env_name             Override conda environment name derived from envs/${pipeline,,}.yaml (default=${pipeline,,})
  -p, --profile              Set a Snakemake profile, e.g. workstation (default=bfr.hpc)
  -c, --from_config {file}   Deactivate batch mode and use path to chewieSnake config file to update the run (mode=config)
  -o, --onerun               Deactivate batch mode and use hardcoded single run parameters (mode=single)
  -s, --snakemake            Skip the ${pipeline,,}.py wrapper script and use a customized Snakemake command; this also skips config_${pipeline,,}.yaml recreation
  -cdb, --use_ceph_db        ${grey}[BfR-only] Use the databases on CephFS${normal}
  -cex, --use_ceph_exec      ${grey}[BfR-only] Use pipeline repository on CephFS for executing ${pipeline,,}.py${normal}
  --dryrun                   Perform a dryrun and include a Snakemake dryrun for DAG calculation (default=false)
  -n, --norun                Perform a dryrun without Snakemake commands (default=false), implies --auto
  -a, --auto                 Skip interactive confirmation dialogue (default=false), e.g. for tmux-wrapped script calls

${bold}Notes:${normal}
Check the manual_override() function to tweak script arguments for repetitive use.
Check the tweak_settings() function to review potential system-specific expert settings.
If you like script fail notifications by email, set the environment variable "email" to activate the error_handler() function.
Schema definition and prodigal training file are set in this order of mode: "config" ${yellow}>${normal} --use_ceph_db ${yellow}>${normal} "batch" (default) ${yellow}&${normal} "single"

${bold}Example:${normal}
bash $script_real -p bfr.hpc -a -n

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  repo_path=$(dirname "$script_path")
  profile="bfr.hpc"
  conda_recipe="$repo_path/envs/${pipeline,,}.yaml"
  dag_search="$repo_path/profiles/smk_directives.yaml"
  docker_image="bfrbioinformatics/${pipeline,,}:latest"

  ## default values of switches
  mode="batch"
  snakemake=false
  use_ceph_db=false
  use_ceph_exec=false
  norun=false
  dryrun=false
  interactive=true

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -e | --env_name)
        conda_recipe_env_name="$2"
        shift 2
        ;;
      -p | --profile)
        profile="$2"
        shift 2
        ;;
      -o | --onerun)
        mode="single"
        shift
        ;;
      -c | --from_config)
        mode="config"
        configfile="$2"
        shift 2
        ;;
      -s | --snakemake)
        snakemake=true
        shift
        ;;
      -cdb | --use_ceph_db)  # BfR-specific
        use_ceph_db=true
        shift
        ;;
      -cex | --use_ceph_executable)  # BfR-specific
        use_ceph_exec=true
        shift
        ;;
      -n | --norun)
        norun=true
        dryrun=true
        interactive=false
        user_args+="--dryrun "
        shift
        ;;
      --dryrun)
        dryrun=true
        user_args+="--dryrun "
        shift
        ;;
      -a | --auto)
        interactive=false
        shift
        ;;
      -?*)
        user_args+="${1-} "
        shift
        ;;
      *) break ;;
    esac
  done

  ## checks
  [[ -f $conda_recipe ]] || die "The conda recipe does not exist: $conda_recipe"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"
  [[ $dryrun == false ]] && echo -e "\n${bold}${magenta}-->  This is NOT a dryrun!  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname "$script_real")
script_name=$(basename "$script_real")
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
# shellcheck source=./helper_functions.sh
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Notification on unexpected behaviour
[[ $(declare -F error_handler) == error_handler ]] && [[ -n ${email+x} ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM # ERR # TODO: read HEREDOC causes an unknown error

manual_override() {
  ## (1) Hardcoded Parameters that will override defaults, comment-in to activate

  ## Conda Environment
#  [[ "$profile" == "workstation" ]] && conda_recipe_env_name="chewiesnake3"  # NOTE: fix for non-standard env names

  ## Code Base
#  repo_path="/cephfs/abteilung4/NGS/Pipelines/chewiesnake"

  ## Redirect to other Databases
  [[ "$use_ceph_db" == true ]] && dir_schema="/home/NGSAdmin/HPC_referencedb/cgmlst/active_schemes_user/Salmonella/enterobase_cgmlst/enterobase_senterica_cgmlst"  # TODO: this is not pathogen-specific yet
  [[ "$use_ceph_db" == true ]] && dir_prodigal_training="/cephfs/abteilung4/NGS/Pipelines/chewieSnake/chewBBACA/CHEWBBACA/prodigal_training_files"

  ## Snakemake Profiles (see config.yaml within)
#  profile="bfr.hpc.greedy"
#  profile="bfr.hpc"
#  profile="workstation"

  ## Resources
  [[ "$profile" == "bfr.hpc.greedy" ]] && threads_sample=6
  [[ "$profile" == "bfr.hpc"        ]] && threads_sample=4
  [[ "$profile" == "workstation"    ]] && threads_sample=3

  ## DAG Job Prioritization
#  dag_search=$repo_path/profiles/smk_directives_depthfirst.yaml
#  dag_search=$repo_path/profiles/smk_directives_breadthfirst.yaml
#  dag_search=$repo_path/profiles/smk_directives_breadthfirst_groups.yaml
#  dag_search=$repo_path/profiles/smk_directives_depthfirst_groups.yaml

  ## Docker Images
#  docker_image="bfrbioinformatics/${pipeline,,}:3.2.1"

  ## chewieSnake Database Options
#  dir_schema=$(realpath $repo_path/schema-BfR/enterobase_senterica_cgmlst)
#  dir_schema=$(realpath $repo_path/schema/enterobase_senterica_cgmlst)
#  prodigal_training_file="Salmonella_enterica.trn"

  true
}

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] Manual Run Parameters Definition

  ## Conda Environment
  conda_recipe_env_name=${conda_recipe_env_name:-$(head -n1 "$conda_recipe" | cut -d' ' -f2)}

  ## Resources
  profile_base="$repo_path/profiles"
  profile="$profile_base/$profile"
  [[ -n ${SNAKEMAKE_PROFILE:-} ]] && unset SNAKEMAKE_PROFILE  # unset to avoid conflicts

  ## Logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="$script_path/$logfile_name"
  [[ $dryrun == false ]] && logfile_global=/dev/null

  ## Run variables; if in mode "batch" {run_name,dir_samples,dir_data,etc.} will be read from run_table
  run_table="$script_path/${pipeline}_runs.tsv"

  ## chewieSnake default database options
  dir_schema=${dir_schema:-"$repo_path/schema/Salmonella_enterica/Salmonella_enterica_INNUENDO_cgMLST"}
  dir_prodigal_training=${dir_prodigal_training:-"$repo_path/chewBBACA/CHEWBBACA/prodigal_training_files"}
  prodigal_training_file="Salmonella_enterica.trn"
}

set_run_paths() {
  [[ -d $dir_data                  ]] || logexec mkdir -p "$dir_data"
  logecho "cd $dir_data"
  cd "$dir_data" || die "Cannot change to working directory: $dir_data"

  [[ -f "$dir_samples/samples.tsv" ]] && sample_list="$dir_samples/samples.tsv"
  [[ -z "${configfile:-}"          ]] && configfile="$dir_data/config_${pipeline,,}.yaml"
  [[ -z "${run_name:-}"            ]] && run_name="test_data_$(date +%F)_$(hostname)_$mode"

  # use BfR accredited version
  [[ $use_ceph_exec == true && -d "/cephfs/abteilung4/NGS/Pipelines/chewieSnake" ]] && repo_path="/cephfs/abteilung4/NGS/Pipelines/chewieSnake"

  # checks
  [[ -d $dir_schema                 ]] || die "Schema directory not found: $dir_schema"
  [[ -f $prodigal_training_filepath ]] || die "Prodigal training file not found: $prodigal_training_filepath"
  true
}

## Info --------------------------------------------------

show_info() {
  logheader "General Settings:"
  echo "
Mode:|$mode
Repository:|$repo_path
Conda Recipe:|$conda_recipe
Conda Recipe Env Name:|$conda_recipe_env_name
Threads per Sample:|$threads_sample
Profile:|$profile
DAG Priority:|$dag_search
Snakemake User Args:|${user_args:-}
Logfile:|$logfile
" | column -t -s="|" | tee >(logglobal 1> /dev/null) && sleep 0.2
  true
}

show_info_on_run() {
  ## print current run infos
  cat <<- TXT | tee -a "$logfile" "$logfile_global"

${yellow}#################################################################### Processing:${normal}

Run:           ${inverted}$run_name${normal}
SampleList:    ${bold}${sample_list:-not yet created}${normal}
WorkDir:       ${bold}$dir_data${normal}
Schema:        ${bold}$dir_schema${normal}
TrainingFile:  ${bold}$prodigal_training_filepath${normal}

${blue}To monitor the Snakemake log use:${normal}
tail -F -s3 \$(find $dir_data/.snakemake/log -name "*.snakemake.log" -print0 | xargs -r -0 ls -1 -t | head -1)
TXT
}

show_info_on_tmux() {
  ## Prerequisite for tmux wrapping: add `set-option -g default-shell "/bin/bash"` to ~/.tmux.conf
  cat <<- TXT | tee -a "$logfile" "$logfile_global"

${blue}Best experience with tmux:${normal}
tmux new -d -s ${pipeline,,}_\$USER
tmux send-keys -t ${pipeline,,}_\${USER}.0 'bash $script_real --auto' ENTER

All steps were logged to: $logfile and $logfile_global (global)
TXT
}

## Checks --------------------------------------------------

run_checks() {
  [[ ! -d $profile ]] && die "The snakemake profile directory does not exist: $profile"
  [[ $mode == "batch"  && ! -f $run_table  ]] && die "The run table does not exist: $run_table"
  [[ $mode == "config" && ! -f $configfile ]] && die "The config file does not exist: $configfile"
  # TODO: add more for novices
  true
}

## More Settings --------------------------------------------------

tweak_settings() {
  ## more privacy towards NCBI
  declare -x BLAST_USAGE_REPORT=false # see https://github.com/tseemann/mlst/issues/115

  # # System Stack Limit - this avoids reporting.smk|report.Rmd Error: C stack usage  7970548 is too close to the limit
#  ulimit -s 8192  # default
  ulimit -s 16384
#  ulimit -s 32768

  ## append environment libs to LD path, if dependencies are not found
  [[ "$profile" =~ "saga" ]] && export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$conda_env_path/lib/ && logecho "Conda libraries appended to LD_LIBRARY_PATH"
  ## NOTE: this may cause a "/bin/bash: <conda-prefix>/envs/aquamis/lib/libtinfo.so.6: no version information available (required by /bin/bash)"
  ## NOTE: checkout Issues #13 and #15 at https://gitlab.com/bfr_bioinformatics/AQUAMIS/-/issues/?state=all
  true
}

activate_conda() {
  ## activate environment
  if [[ "$conda_env_active" != "$conda_env_name" ]]; then
    first_conda_env=$(head -n1 <<< "$conda_env_path")  # use first entry if multiple envs with the same name exist, e.g. from other users
    logecho "Conda environment $conda_env_name has not been activated yet. Activating Conda environment \"$first_conda_env\""
    set -u && source "$conda_base/etc/profile.d/conda.sh" && set +u
    conda activate "$first_conda_env"
  elif [[ $use_ceph_exec == true ]]; then  # BfR-specific
    logwarn "Overriding first conda environment path with CEPH environment path because the flag --use_ceph_executable was set"
    conda_env_path="/home/NGSAdmin/miniconda3/envs/chewiesnake"
    [[ ! -d "$conda_env_path" ]] && die "The conda environment does not exist: chewiesnake"
    set -u && source "$conda_base/etc/profile.d/conda.sh" && set +u
    conda activate $conda_env_path
  else
    logecho "Conda environment $conda_env_name was already activated"
  fi

  ## check
  logecho "Using $(conda info | grep "active env location" | sed -e "s/^\s*//")"
}

## Pre-Run Modules --------------------------------------------------

check_slurm() {
  printf "Checking SLURM environment ... "
  [[ ! -x "$(command -v sinfo)" ]] && echo "${red}SLURM not detected${normal}" && return 0
  [[ ! -x "$(command -v module)" ]] && echo "${red}Lmod not detected${normal}" && return 0
  echo "SLURM system with Lmod detected."
  conda_module_loaded=0
  [[ $(module -t list | grep -c Miniconda) -gt 0 ]] && conda_module_loaded=1 && echo "Miniconda module already loaded."
  conda_module_avail=0
  [[ $(module -t avail Miniconda | grep -c Miniconda) -gt 0 ]] && conda_module_avail=1
  [[ "$conda_module_loaded" == 0 && "$conda_module_avail" == 1 ]] && echo "Loading Miniconda module." && module load Miniconda3/4.9.2
}

## Modules --------------------------------------------------

run_chewieSnake_fromContigs() {
  logheader "Running ${pipeline} via ${pipeline,,}.py:"
  chewiesnake_args+="--scheme $dir_schema --prodigal $prodigal_training_filepath "
  read -r -d '' wrapper_cmd <<- EOF
  python3 $repo_path/${pipeline}.py
    --profile $profile
    --working_directory $dir_data
    --rule_directives $dag_search
    --threads_sample $threads_sample
    --sample_list $sample_list
    ${chewiesnake_args:-}
    ${user_args:-}
EOF
#    --clustering_method average
#    --noreport
#    --json
#    --dryrun
#    --force all_json
#    --version
#    --conda_frontend
#    --condaprefix $conda_base/envs
  run_command_array $wrapper_cmd
}

run_snakemake_report() {
  logheader "Generating Snakemake Report:"
  read -r -d '' wrapper_cmd <<- EOF
  snakemake
    --profile $profile
    --configfile $configfile
    --snakefile $repo_path/${pipeline}_fromcontigs.smk
    --forceall
    --rulegraph | dot -Tpdf -o $dir_data/rulegraph-all.pdf
EOF
#    --force all_json
#    --force all_noreport
#    --dag | dot -Tpdf -o $dir_data/dag-all.pdf
#    --report $dir_data/reports/smk_report.html
  run_command_array $wrapper_cmd
}

run_docker_compose() {
  logheader "Running chewieSnake via DOCKER-compose:"

  # git image
  app_name="chewiesnake_app"
  compose_yaml="$repo_path/docker/docker-compose.yaml"

  # bioconda image
#  app_name="chewiesnake_bioconda_app"
#  compose_yaml="$repo_path/docker/docker-compose-bioconda.yaml"

  # fire up docker containers
  export LOCAL_USER_ID=$(id -u "$USER")
  export CHEWIESNAKE_HOST_PATH=$dir_data
  grep -q $app_name <(docker ps --filter "name=$app_name" --format '{{.Names}}') || logheader "Docker compose UP:"
  grep -q $app_name <(docker ps --filter "name=$app_name" --format '{{.Names}}') || logexec docker compose -f "$compose_yaml" up --no-build --pull missing --detach

  # wait for self-inflation of databases to finish
  while [[ $(docker compose -f "$compose_yaml" ps -q databases) ]] ; do echo "building database" && sleep 1; done

  # create sample sheet
  [[ -f $dir_data/samples.tsv ]] || logheader "Creating $pipeline Sample Sheet via DOCKER:"
  read -r -d '' wrapper_cmd <<- EOF
  docker exec --user $LOCAL_USER_ID $app_name
    /chewieSnake/scripts/create_sampleSheet.sh
    --force
    --mode assembly
    --fastxDir $dir_samples
    --outDir /chewieSnake/analysis
EOF
  [[ -f $dir_data/samples.tsv ]] || run_command_array $wrapper_cmd

  # run pipeline
  chewiesnake_args+="--scheme $dir_schema --prodigal $dir_schema/$prodigal_training_file "
  read -r -d '' wrapper_cmd <<- EOF
  docker exec --user $LOCAL_USER_ID $app_name
    micromamba run -n chewiesnake
      python3 /chewieSnake/chewieSnake.py
        --profile $profile
        --working_directory /chewieSnake/analysis
        --rule_directives /chewieSnake/profiles/$(basename "$dag_search")
        --threads_sample $threads_sample
        --sample_list /chewieSnake/analysis/samples.tsv
        ${chewiesnake_args:-}
        ${user_args:-}
EOF
  # legacy incompatible options
#    --profile $profile
#    --rule_directives /chewieSnake/profiles/$(basename "$dag_search")
  # TODO: missing options
#    --docker $dir_data
  # for database mapping when using only bfrbioinformatics/chewiesnake:latest
#    -v $(realpath $repo_path/reference_db/confindr):/chewieSnake/reference_db/confindr
#    -v $(realpath $repo_path/reference_db/kraken):/chewieSnake/reference_db/kraken
#    -v $(realpath $repo_path/reference_db/mash):/chewieSnake/reference_db/mash
#    -v $(realpath $repo_path/reference_db/taxonkit):/chewieSnake/reference_db/taxonkit
#    -v $(realpath $repo_path/reference_db/trimmomatic):/chewieSnake/reference_db/trimmomatic
  run_command_array $wrapper_cmd
}

run_modules() {
  ## [Rationale] Define per-run module set - choose between Python wrapper, Snakemake or Docker
  set_run_paths
  show_info_on_run
  [[ $snakemake == false ]] && run_chewieSnake_fromContigs
#  [[ $snakemake == true  ]] && run_snakemake  # TODO: not yet used
#  run_snakemake_report
#  run_docker_compose
}

## Sample Mode --------------------------------------------------

runs_from_run_table() {
  local run_table

  run_table=${1-}

  ## loop through run_table
  mapfile -t runArray <"$run_table"
  for line in "${runArray[@]}"; do # loop through runs in run_table
    IFS=$'\t' field=($line) IFS=$' \t\n'
    [[ "${field[0]}" =~ ^#.* ]] && continue # skip commented samples
    run_name=$(eval "echo \"${field[0]}\"")
    dir_samples=$(realpath "$(eval "echo \"${field[1]}\"")")
    dir_data=$(realpath -m "$(eval "echo \"${field[2]}\"")")
    prodigal_training_file="${field[3]}"
    dir_schema=${dir_schema:-"$(eval "echo ${field[4]}")"}
    prodigal_training_filepath="$dir_prodigal_training/$prodigal_training_file"
    run_modules
  done
}

run_from_configfile() {
  local configfile

  configfile=${1:-}
  chewiesnake_args+="--config $configfile "

  ## get values from configfile
  sample_list=$(grep -E "^samples:" "$configfile" | awk -F': ' '{print $2}')
  dir_samples=$(dirname "$sample_list")
  dir_data=$(grep -E "^workdir:" "$configfile" | awk -F': ' '{print $2}')
  threads_sample=$(grep -E "^  threads:" "$configfile" | awk -F': ' '{print $2}')
  prodigal_training_filepath=$(grep -E "^    prodigal_training_file:" "$configfile" | awk -F': ' '{print $2}')
  dir_schema=$(grep -E "^    scheme_dir:" "$configfile" | awk -F': ' '{print $2}')
  run_modules
}

run_from_hardcoded() {
  dir_samples="$repo_path/test_data/chewiesnake/data"
  dir_data="$repo_path/test_data/chewiesnake/analysis_test_data_$(date +%F)"
  prodigal_training_filepath="$dir_prodigal_training/$prodigal_training_file"
  run_modules
}

## (3) Main

manual_override
define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info
run_checks

## Workflow
#check_slurm
check_conda "$conda_recipe_env_name"
activate_conda
[[ $interactive == true ]] && interactive_query
tweak_settings

## Switch between single-run or batch mode
case ${mode} in
  batch)
    logecho "Processing all active runs in ${magenta}run table${normal}: $run_table"
    runs_from_run_table "$run_table"
    ;;
  single)
    logecho "Processing a ${magenta}single run${normal}"
    run_from_hardcoded
    ;;
  config)
    logecho "Processing run defined in ${pipeline} ${magenta}config file${normal}: $configfile"
    run_from_configfile "$configfile"
    ;;
  *) die "The {mode} was not specified or recognized. Check ${blue}bash $script_real --help${normal} for available parameters" ;;
esac

show_info_on_tmux
logglobal "[STOP] $script_real"
