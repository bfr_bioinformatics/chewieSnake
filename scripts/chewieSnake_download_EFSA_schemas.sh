#!/bin/bash

## [Goal] Download chewieNS wgMLST schemas via the chewieNS REST API and subset loci according to EFSA schema definition
## [Author] Holger Brendebach

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, # ADD e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name [Options]

${bold}Description:${normal}
Download chewieNS wgMLST schemas via the chewieNS REST API and subset loci according to EFSA schema definition.

${bold}Required Named Arguments:${normal}
-d, --schema_dir                Parent directory of chewieSnake wgMLST schemas, e.g. ${repo_path}/schema

${bold}Optional Named Arguments:${normal}
-n, --dryrun                    Perform a dryrun to review the commands (default=false)

${bold}Notes:${normal}
This script is a wrapper for the more flexible chewieNS_wrapper.py

${bold}Example:${normal}
bash $script_real --schema_dir ${repo_path}/schema

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  repo_path=$(dirname $script_path)
  schema_dir=""

  ## default values of switches
  dryrun=false

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -d | --schema_dir)
        schema_dir="$2"
        shift 2
        ;;
      -n | --dryrun)
        dryrun=true
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required params and arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. Try ${blue}bash $script_real --help${normal}"
  [[ -d ${schema_dir-} ]] || die "The schema directory does not exist: ${schema_dir-}"
  [[ -f $script_path/chewieNS_wrapper.py ]] || die "The Python script chewiens_wrapper.py was not found"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "ERROR: File not found: $script_path/helper_functions.sh" && kill -s TERM $$
# shellcheck source=./helper_functions.sh
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Notification on unexpected behaviour
#[[ $(declare -F error_handler) == error_handler ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM ERR  # TODO: enable if needed, disable in BfR-ABC repositories

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths()

  ## Logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile=$schema_dir/$logfile_name
  [[ $dryrun == false ]] && logfile_global=/dev/null
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
schema_dir:|$schema_dir
dryrun:|$dryrun
" | column -t -s "|" | tee -a $logfile $logfile_global
}

## Modules --------------------------------------------------

query_schemas() {
  ## [Rationale] query_schemas()
  logheader "chewieNS schemas:"
  python $script_path/chewieNS_wrapper.py --query
  echo
}

download_schema() {
  ## [Rationale] download_schema()
  local species="$1"
  [[ -d $schema_dir/${species/ /_} ]] && logwarn "The schema parent directory already has a subdirectory ./${species/ /_}" && return
  logexec python $script_path/chewieNS_wrapper.py --schema_dir $schema_dir --species \"$species\"
}

unzip_schema() {
  ## [Rationale] unzip_schema()
  local species="${1/ /_}"
  local zip_file=""
  [[ -d $schema_dir/$species ]] || die "The directory $schema_dir/$species does not exist. The API download may have not succeeded."
  [[ -d $schema_dir/$species ]] && zip_file=$(find $schema_dir/$species -maxdepth 1 -type f -name "*.zip" -print -quit)
  [[ -z $zip_file ]] && die "There is no ZIP file in $schema_dir/$species. The API download may have not succeeded."
  [[ -d ${zip_file/\.zip/} ]] && logwarn "The $species schema was already extracted to ${zip_file/\.zip/}" && return
  logheader "Unzipping $zip_file:"
  logexec unzip -q $zip_file -d ${zip_file/\.zip/}
}

get_efsa_loci_subset() {
  ## [Rationale] get_efsa_loci_subset()
  local zip_file="$schema_dir/chewbbaca_cgMLST_gene_lists.zip"
  [[ -f $zip_file ]] && logwarn "Skipping filter download. The EFSA schema subset list ZIP file already exists: $zip_file"
  [[ -f $zip_file ]] || wget https://zenodo.org/record/6655441/files/Annex%20A%20-%20chewbbaca_cgMLST_gene_lists.zip?download=1 -O $zip_file
  [[ -d $schema_dir/chewbbaca_filters ]] && logwarn "Skipping filter extraction. The EFSA schema subset list files already exists in $schema_dir/chewbbaca_filters."
  [[ -d $schema_dir/chewbbaca_filters ]] || logexec unzip $zip_file -d $schema_dir
}

intersect_schema() {
  ## [Rationale] intersect_schema()
  local species="${1/ /_}"

  [[ -d $schema_dir/$species ]] || die "The directory $schema_dir/$species does not exist. The API download may have not succeeded."
  [[ -d $schema_dir/$species ]] && extracted_schema_dir=$(find $schema_dir/$species -maxdepth 1 -type d -name "${species}_*" -print -quit)
  [[ -z $extracted_schema_dir ]] && die "There is no extracted schema in $schema_dir/$species. The API download and extraction may have not succeeded."

  [[ "$species" == "Escherichia_coli" ]] && local filter_list="$schema_dir/chewbbaca_filters/cgMLST_ecoli.txt"
  [[ "$species" == "Salmonella_enterica" ]] && local filter_list="$schema_dir/chewbbaca_filters/cgMLST_salmonella.txt"
  [[ -z ${filter_list:-} ]] && logwarn "The schema for $species does not need loci filtering" && return
  [[ -d $extracted_schema_dir/removed_loci ]] && logwarn "The schema in $extracted_schema_dir was already filtered for EFSA loci"

  logheader "Loci set analysis for $(basename $extracted_schema_dir):"
  loci_sets=$(comm --check-order --total -1 -2 -3 <(find $extracted_schema_dir/* -maxdepth 0 -type f -name "*.fasta" | sed -e "s@$extracted_schema_dir/@@g" | sort ) <(cat $filter_list | sort ))
  move_set=$(comm --check-order -2 -3 <(find $extracted_schema_dir/* -maxdepth 0 -type f -name "*.fasta" | sed -e "s@$extracted_schema_dir/@@g" | sort ) <(cat $filter_list | sort ))
  echo "
Unique to extracted schema:|$(echo "$loci_sets" | cut -f1)
Unique to $(basename $filter_list):|$(echo "$loci_sets" | cut -f2)
Shared by both:|$(echo "$loci_sets" | cut -f3)
" | column -t -s "|" | tee -a $logfile $logfile_global

  if [[ $(echo "$loci_sets" | cut -f1) -gt 0 ]]; then
    move_set_short=${move_set//.fasta/_short.fasta}
    logheader "Filtering $(basename $extracted_schema_dir):"
    logexec mkdir -p $extracted_schema_dir/removed_loci/short
    cd $extracted_schema_dir
    mv --verbose -t $extracted_schema_dir/removed_loci $move_set |& tee -a  $logfile > /dev/null
    cd $extracted_schema_dir/short
    mv --verbose -t $extracted_schema_dir/removed_loci/short $move_set_short |& tee -a  $logfile > /dev/null
  else
    logwarn "There are no surplus loci to be filtered."
  fi
  [[ $(echo "$loci_sets" | cut -f2) -gt 0 ]] && die "The schema misses EFSA loci: $extracted_schema_dir"
}

## (3) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info

## Workflow
query_schemas
efsa_species_list=("Escherichia coli" "Listeria monocytogenes" "Salmonella enterica")
get_efsa_loci_subset
for species in "${efsa_species_list[@]}"; do
  download_schema "$species"
  unzip_schema "$species"
  intersect_schema "$species"
done

logglobal "All steps were logged to: $logfile"
logglobal "[STOP] $script_real"
