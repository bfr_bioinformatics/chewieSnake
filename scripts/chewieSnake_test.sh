#!/bin/bash

## [Goal] Test chewieSnake and chewieSnake_join run scenarios with test_data
## [Author] Holger Brendebach

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e

#region ## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name [Options]

${bold}Description:${normal}
Test various options of the chewieSnake workflow. Results will be written to output directories in $repo_path/test_data

${bold}Optional Named Arguments:${normal}
-o, --outDir                    Prefix for the test result directory name, e.g. "test_2020-12-24".
                                A prefix will be created if none is provided.
-n                              Perform a dryrun of the test script to review commands and environment information
--dryrun                        Create outDirs and perform a dryrun to of the snakemake workflow
-a, --auto                      Do not ask before starting the modules

${bold}Notes:${normal}
This script requires a test result directory name prefix as an argument, e.g. bash chewieSnake_test.sh "test_2020-12-24".

${bold}Example:${normal}
bash chewieSnake_test.sh test_2020-12-24

USE
  exit 0
}

parse_args() {
  local args=("$@")

  # default values of variables

  ## Snakemake Profiles (see config.yaml within)
  profile="workstation"
  profile="bfr.hpc"

  ## Conda Environment
  conda_recipe="chewiesnake_noR.yaml"  # requires "chewieSnake.py --noreport"
  conda_recipe="chewiesnake.yaml"  # TODO: eventually replace with chewiesnake_fromconda.yaml
  conda_recipe_env_name=$(head -n1 "$repo_path/envs/$conda_recipe" | cut -d' ' -f2)

  ## chewieSnake Database Options
  dir_schema="$(realpath $repo_path/schema/enterobase_senterica_cgmlst)"
  dir_prodigal_training="$repo_path/chewBBACA/CHEWBBACA/prodigal_training_files"
  prodigal_training_file="Salmonella_enterica.trn"

  ## Resources
  [[ "$profile" == "saga"        ]] && cores_sample=4
  [[ "$profile" == "bfr.hpc"     ]] && cores_sample=4
  [[ "$profile" == "workstation" ]] && cores_sample=3

  profile_base="$repo_path/profiles"
  profile="$profile_base/$profile"
  [[ -n ${SNAKEMAKE_PROFILE:-} ]] && unset SNAKEMAKE_PROFILE  # unset to avoid conflicts

  ## DAG Job Prioritization
  dag_search=$repo_path/profiles/smk_directives_depthfirst_groups.yaml
  dag_search=$repo_path/profiles/smk_directives.yaml

  ## Docker Images
  docker_image="bfrbioinformatics/chewiesnake:latest"

  ## default values of switches
  norun=false
  dryrun=false
  interactive=true
  docker=false

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -o | --outDir)
        outDir="$2"
        shift 2
        ;;
      -n)
        norun=true
        dryrun=true
        shift
        ;;
      --dryrun)
        dryrun=true
        shift
        ;;
      -a | --auto)
        interactive=false
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"
  [[ $dryrun == false ]] && echo -e "\n${bold}${magenta}-->  This is NOT a dryrun!  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
repo_path=$(dirname $script_path)
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "ERROR: File not found: $script_path/helper_functions.sh" && kill -s TERM $$
# shellcheck source=./helper_functions.sh
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

#endregion ## (1) Argument Logic
#region ## (2) Script Logic

## Notification on unexpected behaviour
#[[ $(declare -F error_handler) == error_handler ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM ERR  # TODO: enable if needed, disable in BfR-ABC repositories

## Define Paths --------------------------------------------------

define_paths() {
  printf -v date '%(%Y-%m-%d)T' -1
  target_dir=${outDir:-"test_$date"}

  ## logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="$script_path/$logfile_name"
  [[ $dryrun == false ]] && logfile_global=/dev/null
}

## Info --------------------------------------------------

show_info() {
  logheader "Workflow Parameters:"
  echo "
Working Directory:|$dir_test
Schema Path:|$dir_schema
Prodigal Training File:|$dir_prodigal_training/$prodigal_training_file
Conda Recipe:|$conda_recipe
Profile:|$profile
Cores per sample:|$cores_sample
DAG Priority:|$dag_search
Switch norun:|$norun
Switch dryrun:|$dryrun
Switch interactive:|$interactive
Switch docker:|$docker
" | column -t -s="|" | tee -a $logfile $logfile_global
  echo
  [[ $interactive == true ]] && interactive_query  # from helper_functions.sh
}

## Modules --------------------------------------------------

activate_conda() {
  ## activate environment
  if [[ "$conda_env_active" != "$conda_env_name" ]]; then
    first_conda_env=$(head -n1 <<< $conda_env_path)  # use first entry if multiple envs with the same name exist, e.g. from other users
    echo "Conda environment $conda_env_name has not been activated yet. Activating Conda environment \"$first_conda_env\""
    set -u && source $conda_base/etc/profile.d/conda.sh && set +u
    conda activate $first_conda_env
  else
    echo "Conda environment $conda_env_name was already activated."
  fi

  ## check
  logecho "Using $(conda info | grep "active env location" | sed -e "s/^\s*//")"
  echo

  ## append environment libs to LD path, if dependencies are not found
  ## NOTE: this may cause a "/bin/bash: <conda-prefix>/envs/aquamis/lib/libtinfo.so.6: no version information available (required by /bin/bash)"
  ## NOTE: checkout Issues #13 and #15 at https://gitlab.com/bfr_bioinformatics/AQUAMIS/-/issues/?state=all
  [[ "${profile-}" =~ "saga" ]] && export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$conda_env_path/lib/ && logecho "Conda libraries appended to LD_LIBRARY_PATH"
}

#endregion ## (2) Script Logic
#region ## (3) Modules

test_chewieSnake_docker() {

  logecho "Running module ${green}test_chewieSnake_docker${normal}"

  ## static docker paths
  dir_analysis_docker="/chewieSnake/analysis"
  dir_schema_docker="/chewieSnake/schema/enterobase_senterica_cgmlst"
  dir_prodigal_training_docker="/chewieSnake/chewBBACA/CHEWBBACA/prodigal_training_files"

  ## update image
  docker pull bfrbioinformatics/chewiesnake:latest || true
  docker_image_id=$(docker image list | grep "chewiesnake" | grep "latest" | awk '{ print $3 }')

  ## create test dir
  dir_data="$repo_path/test_data/chewiesnake"
  dir_test="$dir_data/${target_dir}_docker"
  show_info
  [[ $norun == false && ! -d $dir_test ]] && mkdir -p $dir_test
  [[ $norun == false ]] && cp --force --recursive $dir_data/data $dir_test

  ## create sample sheet
  read -r -d '' wrapper_cmd <<- EOF
  docker run --rm
    -v ${dir_test}:${dir_analysis_docker}
    --entrypoint bash
    $docker_image_id
    /chewieSnake/scripts/create_sampleSheet.sh
    --force
    --mode assembly
    --fastxDir $dir_analysis_docker/data
    --outDir $dir_analysis_docker/data
EOF
  run_command_array $wrapper_cmd

  ## run analysis
  read -r -d '' wrapper_cmd <<- EOF
  docker run --rm
    -v ${dir_test}:${dir_analysis_docker}
    -e LOCAL_USER_ID=$(id -u $USER)
    $docker_image_id
    --condaprefix /opt/conda/envs
    --sample_list $dir_analysis_docker/data/samples.tsv
    --working_directory $dir_analysis_docker
    --scheme $dir_schema_docker
    --prodigal $dir_prodigal_training_docker/$prodigal_training_file
    --threads
EOF
  [[ $dryrun == true ]] && wrapper_cmd+=" --dryrun"
  run_command_array $wrapper_cmd
}

test_chewieSnake_wrapper() {

  ## full conda test via wrapper: 7 min 30 sec on 7 threads
  logecho "Running module ${green}test_chewieSnake_wrapper${normal}"

  ## create test dir
  dir_data="$repo_path/test_data/chewiesnake"
  dir_test="$dir_data/${target_dir}_wrapper"
  show_info
  [[ $norun == false && ! -d $dir_test ]] && mkdir -p $dir_test
  [[ $norun == false ]] && $script_path/create_sampleSheet.sh --force --mode assembly --fastxDir $dir_data/data --outDir $dir_test

  read -r -d '' wrapper_cmd <<- EOF
  $repo_path/chewieSnake.py
    --sample_list $dir_test/samples.tsv
    --working_directory $dir_test
    --profile $profile
    --rule_directives $dag_search
    --threads_sample $cores_sample
    --scheme $dir_schema
    --prodigal $dir_prodigal_training/$prodigal_training_file
EOF
  [[ $dryrun == true ]] && wrapper_cmd+=" --dryrun"
  run_command_array $wrapper_cmd

  ## export graph
  read -r -d '' wrapper_cmd <<- EOF
  snakemake
    --profile $profile
    --snakefile $repo_path/chewieSnake_fromcontigs.smk
    --configfile $dir_test/config_chewiesnake.yaml
    --forceall
    --rulegraph | dot -Tpdf > $dir_test/rulegraph-all.pdf
EOF
#    --dag | dot -Tpdf > $dir_test/dag-all.pdf
#    --report $dir_test/smk_report.html
  cd $dir_test  # for logdir
  run_command_array $wrapper_cmd
}

test_chewieSnake_snakemake() {

  logecho "Running module ${green}test_chewieSnake_snakemake${normal}"

  ## create test dir
  dir_data="$repo_path/test_data/chewiesnake"
  dir_test="$dir_data/${target_dir}_snakemake"
  show_info
  [[ $norun == false && ! -d $dir_test ]] && mkdir -p $dir_test
  [[ $norun == false ]] && $script_path/create_sampleSheet.sh --force --mode assembly --fastxDir $dir_data/data --outDir $dir_test

  ## create config yaml
  read -r -d '' wrapper_cmd <<- EOF
  $repo_path/chewieSnake.py
    --sample_list $dir_test/samples.tsv
    --working_directory $dir_test
    --profile $profile
    --rule_directives $dag_search
    --threads_sample $cores_sample
    --scheme $dir_schema
    --prodigal $dir_prodigal_training/$prodigal_training_file
    --dryrun
EOF
  run_command_array $wrapper_cmd

  read -r -d '' wrapper_cmd <<- EOF
  snakemake
    --profile $profile
    --snakefile $repo_path/chewieSnake_fromcontigs.smk
    --configfile $dir_test/config_chewiesnake.yaml
EOF
  [[ $dryrun == true ]] && wrapper_cmd+=" --dryrun --quiet"
  cd $dir_test  # for logdir
  run_command_array $wrapper_cmd
}

test_chewieSnake_alleleHashing() {

  logecho "Running module ${green}test_chewieSnake_alleleHashing${normal}"

  ## create test dir
  dir_data="$repo_path/test_data/chewiesnake"
  dir_test="$dir_data/${target_dir}_wrapper"
  show_info

  ## Single Allele Hashing for result set
  ## NOTE: choose either allele_profiles.tsv or allele_profiles.fsremoved.tsv
  read -r -d '' wrapper_cmd <<- EOF
  $repo_path/scripts/alleleprofile_hasher.py
    --profile $dir_test/cgmlst/GMI15-002-DNA/allele_profiles.tsv
    --database $dir_schema
    --outfile $dir_test/allele_hashing/GMI15-002-DNA_allele_profiles.tsv
    --force
EOF
  [[ $dryrun == false ]] && run_command_array $wrapper_cmd

  ## Multi Allele Hashing for result set
  read -r -d '' wrapper_cmd <<- EOF
  $repo_path/scripts/alleleprofile_hasher.py
    --profile $dir_test/cgmlst/allele_profiles_unhashed.tsv
    --database $dir_schema
    --outfile $dir_test/allele_hashing/multi_allele_profiles.tsv
    --force
EOF
  [[ $dryrun == false ]] && run_command_array $wrapper_cmd
}

test_chewieSnake_frameshift() {

  logecho "Running module ${green}test_chewieSnake_frameshift${normal}"

  ## create test dir
  dir_data="$repo_path/test_data/chewiesnake"
  dir_test="$dir_data/${target_dir}_frameshifts"
  show_info
  [[ $norun == false && ! -d $dir_test ]] && mkdir -p $dir_test
  [[ $norun == false ]] && $script_path/create_sampleSheet.sh --force --mode assembly --fastxDir $dir_data/data --outDir $dir_test

  read -r -d '' wrapper_cmd <<- EOF
  $repo_path/chewieSnake.py
    --sample_list $dir_test/samples.tsv
    --working_directory $dir_test
    --profile $profile
    --rule_directives $dag_search
    --threads_sample $cores_sample
    --scheme $dir_schema
    --prodigal $dir_prodigal_training/$prodigal_training_file
    --remove_frameshifts
EOF
  [[ $dryrun == true ]] && wrapper_cmd+=" --dryrun"
  run_command_array $wrapper_cmd
}

test_chewieSnake_comparison() {

  logecho "Running module ${green}test_chewieSnake_comparison${normal}"

  ## create test dir
  dir_data="$repo_path/test_data/chewiesnake"
  dir_test="$dir_data/${target_dir}_comparison"
  show_info
  [[ $norun == false && ! -d $dir_test ]] && mkdir -p $dir_test
  [[ $norun == false ]] && cp --force --recursive $dir_data/data $dir_test
  [[ $norun == false ]] && $script_path/create_sampleSheet.sh --force --mode assembly --fastxDir $dir_test/data --outDir $dir_test

  read -r -d '' wrapper_cmd <<- EOF
  $repo_path/chewieSnake.py
    --sample_list $dir_test/samples.tsv
    --working_directory $dir_test
    --profile $profile
    --rule_directives $dag_search
    --threads_sample $cores_sample
    --scheme $dir_schema
    --prodigal $dir_prodigal_training/$prodigal_training_file
    --comparison
    --comparison_db $dir_data/data/comparison_allele_profiles.tsv
EOF
#    --omit-from comparison_report
  ## TODO: --comparison is an all rule with report; use dryrun to create config and then snakemake with "--omit-from comparison_report"
  [[ $dryrun == true ]] && wrapper_cmd+=" --dryrun"
  run_command_array $wrapper_cmd
}

test_chewieSnake_join() {

  logecho "Running module ${green}test_chewieSnake_join${normal}"

  # Prerequisite: prepare dataset
  # * need allele profiles, allele stats and timestamps from at least 2 partners
  # * need to rename samples
  # * works with empty allele_profiles?

  ## create test dir
  dir_data="$repo_path/test_data/chewiesnake_join"
  dir_test="$dir_data/${target_dir}_joinedDB"
  show_info
  [[ $norun == false && ! -d $dir_test ]] && mkdir -p $dir_test
  [[ $norun == false && ! -d $dir_test/data ]] && cp --force --recursive $dir_data/data $dir_test

  read -r -d '' wrapper_cmd <<- EOF
  $repo_path/chewieSnake_join.py
    --sample_list $dir_test/data/samples.tsv
    --working_directory $dir_test
    --serovar_info $dir_data/sample_serovars.tsv
    --external_cluster_names $dir_data/epiclusters.tsv
    --force all_merge
EOF
  [[ $dryrun == true ]] && wrapper_cmd+=" --dryrun"
  run_command_array $wrapper_cmd

  ## export graph
  read -r -d '' wrapper_cmd <<- EOF
  snakemake
    --profile $profile
    --snakefile $repo_path/chewieSnake_join.smk
    --configfile $dir_test/config_chewiesnake_join.yaml
    --forceall
    --rulegraph | dot -Tpdf > $dir_test/rulegraph-all.pdf
EOF
#    --dag | dot -Tpdf > $dir_test/dag-all.pdf
#    --report $dir_test/smk_report.html
  cd $dir_test  # for logdir
  run_command_array $wrapper_cmd

  # Notes from @carlus :
  # -e EXTERNAL_CLUSTER_NAMES OR --external_cluster_names EXTERNAL_CLUSTER_NAMES
  # HELP: A tab seperated file with external cluster names, e.g. Epi clusers. Must contain one representative sample per cluster: sample <tab >cluster_name
  #
  # --cluster_names
  # HELP: A tab seperated file with serovar or clade names: sample <tab >cluster_name
  #
  # --cluster
  # HELP:
}

#endregion ## (3) Modules
#region ## (4) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
[[ $docker == false ]] && check_conda $conda_recipe_env_name
[[ $docker == false ]] && activate_conda

## Workflow
[[ $docker == true ]] && test_chewieSnake_docker
test_chewieSnake_wrapper
test_chewieSnake_snakemake
test_chewieSnake_alleleHashing
test_chewieSnake_frameshift
test_chewieSnake_comparison
test_chewieSnake_join

logheader "Analysis Status:"
cd "$repo_path/test_data" && find . -name "pipeline_status_chewiesnake*.txt" -exec grep -HP "^.*$" {} \; | column -t -s ":"
echo

logglobal "[STOP] $script_real"

#endregion ## (4) Main
