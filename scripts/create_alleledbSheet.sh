#!/bin/bash

## [Goal] Create sample sheet for all fastq files in a folder
## [Author] Carlus Deneke, Holger Brendebach
version=0.2.0

## Shell Behaviour --------------------------------------------------
set -Eeuo pipefail # to exit immediately, # ADD e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name <inputdir> [Options]

${bold}Description:${normal}
Create allele_database sheet from allele profile files in a specified folder (version: $version).

${bold}Required Positional Arguments:${normal}
<inputdir>                      Base path with allele_profiles.tsv in subdirectories

${bold}Optional Named Arguments:${normal}
-f, --force                     Re-build database (default=false)
-n, --dryrun                    Perform a dryrun to review the commands (default=false)
-i, --interactive               Ask before starting the program (default=false)

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## handle positional arguments
  local named_args=("${args[@]:1}")

  ## default values of variables
  inputdir=${1:-}

  ## default values of switches
  force=false
  dryrun=false
  interactive=false

  for arg in "${named_args[@]}"; do
    case "${arg-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -f | --force)
        force=true
        shift
        ;;
      -n | --dryrun)
        dryrun=true
        shift
        ;;
      -i | --interactive)
        interactive=true
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required params and arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. Try ${blue}bash $script_real --help${normal}"
  [[ ! -d "$inputdir" ]] && die "The inputdir does not exist: $inputdir"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"
  #  [[ $dryrun == false ]] && echo -e "\n${bold}${magenta}-->  This is NOT a dryrun!  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
# shellcheck source=./helper_functions.sh
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Notification on unexpected behaviour
#[[ $(declare -F error_handler) == error_handler ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM ERR  # TODO: enable if needed, disable in BfR-ABC repositories

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths() use realpaths for proper awk splitting

  outfile="$inputdir/samples.tsv"

  ## checks
  [[ -f ${outfile} && ${force} != true ]] && die "The sample sheet already exists. Use --force option to replace it: $outfile"
  [[ ${dryrun} == true ]] && outfile="/dev/null"

  ## logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="${inputdir}/${logfile_name}"
  [[ $dryrun == false ]] && logfile_global=/dev/null

  return 0
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
input directory:|$inputdir
output file:|$outfile
force:|$force
dryrun:|$dryrun
interactive:|$interactive
" | column -t -s "|" | tee >(logglobal 1> /dev/null) && sleep 0.2
  echo
}

## Modules --------------------------------------------------

create_alleledb_samplesheet(){
  ## [Rationale] create_alleledb_samplesheet()

  ## init table
  echo -e "sender\tprofiles\tstatistics\ttimestamps" > $outfile

  for profile_set in $inputdir/*/allele_profiles.tsv; do

    [[ ! -f $profile_set ]] && logerror "File not found: $profile_set" && continue

    ## derived variables
    profile_directory=$(dirname "$profile_set")
    sender=$(basename "$profile_directory")
    statistics="${profile_directory}/allele_statistics.tsv"
    timestamps="${profile_directory}/timestamps.tsv"

    logecho "Processing allele_profile from sender \"$sender\": $profile_set"

    ## check exemptions
    [[ $profile_directory =~ "logs" ]] && logecho "Skipping logs" && continue
    [[ $profile_directory =~ "merged_db" ]] && logecho "Skipping merged_db" && continue
    [[ ! -f $profile_directory/allele_statistics.tsv ]] && touch "${profile_directory}/allele_statistics.tsv"
    [[ ! -f $profile_directory/timestamps.tsv ]] && timestamps="NA" && touch "${profile_directory}/timestamps.tsv" # TODO: clarify logic (HB)

    ## add line to table
    echo -e "$sender\t$profile_set\t$statistics\t$timestamps" >> $outfile

  done

  return 0
}

## (3) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info
[[ $interactive == true ]] && interactive_query # from helper_functions.sh

## Workflow
create_alleledb_samplesheet

logglobal "All steps were logged to: $logfile"
logglobal "[STOP] $script_real"
