#!/bin/bash

## [Goal] Install mamba, conda env, complement conda env, download databases, download test data for AQUAMIS
## [Author] Holger Brendebach
pipeline="chewieSnake"

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name [OPTIONS]

${bold}Description:${normal}
This script completes the installation of the ${pipeline} pipeline. The openssl library is required for hashing the downloaded files.
For ${pipeline} installations from Bioconda use the option set [--databases] or
For ${pipeline} installations from Gitlab   use the option set [--mamba --databases].
For more information, please visit https://gitlab.com/bfr_bioinformatics/${pipeline,,}

${bold}Options:${normal}
  -s, --status               Show installation status
  -e, --env_name             Override conda environment name derived from envs/${pipeline,,}.yaml (default: ${pipeline,,})
  -m, --mamba                Install the latest version of 'mamba' to the Conda base environment and
                             create the ${pipeline} environment from the git repository recipe
  -d, --databases            Download and extract EFSA databases with the Python script chewieSnake_download_EFSA_schemas.sh
  --schema_dir               Change the default schema root directory (default: ${repo_path}/schema)
  -cdb, --symlinks_to_ceph   ${grey}[BfR-only] Create symbolic links to databases on CephFS${normal}
  -r, --runtest              Run a test analysis with the Python script chewieSnake_wrapper.sh
  -a, --auto                 Do not ask for interactive confirmation, e.g. for tmux-wrapped script calls
  -n, --dryrun               Perform a dryrun to review the commands
  -h, --help                 Show this help

${bold}Example:${normal}
bash $script_real --env_name ${pipeline,,} --mamba --databases --dryrun

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  repo_path=$(dirname "$script_path")
  conda_recipe="$repo_path/envs/${pipeline,,}.yaml"
  schema_dir="$repo_path/schema"
  arg_dryrun=""
  arg_runtest_auto=""

  ## default values of switches
  conda_status=false
  mamba_status=false
  bioconda_status=false

  arg_mamba=false
  arg_databases=false
  arg_ceph_symlinks=false
  arg_status=false
  arg_runtest=false
  dryrun=false
  interactive=true

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -e | --env_name)
        conda_recipe_env_name="$2"
        shift 2
        ;;
      -m | --mamba)
        arg_mamba=true
        shift
        ;;
      -d | --databases)
        arg_databases=true
        shift
        ;;
      --schema_dir)
        schema_dir="$2"
        shift 2
        ;;
      -r | --runtest)
        arg_runtest=true
        shift
        ;;
      -cdb | --symlinks_to_ceph)  # this is a BfR-specific option
        arg_ceph_symlinks=true
        shift
        ;;
      -s | --status)
        arg_status=true
        shift
        ;;
      -a | --auto)
        interactive=false
        arg_runtest_auto=" --auto"
        shift
        ;;
      -n | --dryrun)
        dryrun=true
        arg_dryrun=" --dryrun"
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. For the manual, type ${blue}bash $script_real --help${normal}"
  [[ "$arg_databases" == true && "$arg_ceph_symlinks" == true ]] && die "The options --databases and --symlinks_to_ceph are mutually exclusive. Please choose only one."

  ## checks
  [[ $(basename "$script_path") != "scripts" ]] && die "This setup script does not reside in its original installation directory. This is a requirement for proper execution. Aborting..."
  [[ -f $conda_recipe ]] || die "The conda recipe does not exist: $conda_recipe"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname "$script_real")
script_name=$(basename "$script_real")
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Clean-up on unexpected behaviour

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  true
}

[[ $(declare -F cleanup) == cleanup ]] && logecho "Clean up of download directory is enabled" && trap cleanup SIGINT SIGTERM EXIT ERR

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths()

  ## Conda Environment
  conda_recipe_env_name=${conda_recipe_env_name:-$(head -n1 $conda_recipe | cut -d' ' -f2)}

  ## Logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="$script_path/$logfile_name"
  [[ $dryrun == false ]] && logfile_global=/dev/null

  return 0
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
Repository:|$repo_path
Conda Recipe:|$conda_recipe
Conda Recipe Env Name:|$conda_recipe_env_name
Logfile:|$logfile
" | column -t -s="|" | tee -a $logfile $logfile_global
}

## Modules --------------------------------------------------

setup_mamba() {
  # checks
  [[ "$bioconda_status" == false ]] && logecho "Installing the latest version of \"mamba\" to the Conda base environment"
  [[ "$bioconda_status" == true  ]] && die "This is a BioConda installation. There is no need to setup Mamba or an environment for ${pipeline}"

  source $conda_base/etc/profile.d/conda.sh
  [[ "$mamba_status" == false ]] && logexec conda install --channel conda-forge mamba
  [[ "$mamba_status" == true ]] && logwarn "Skipping Mamba installation. Mamba is already detected"

  # Create Conda Environment for pipeline
  logecho "Creating the ${pipeline} environment \"${conda_recipe_env_name}\" with the Conda recipe: $conda_recipe"

  set +eu  # workaround: see https://github.com/conda/conda/issues/8186
  [[ -z "${conda_env_path}" ]] && logexec "mamba env create -n ${conda_recipe_env_name} -f $conda_recipe || true"
  [[ -n "${conda_env_path}" && "$arg_force" == true && ! -d "${conda_base}/envs/${conda_env_name}" ]] && logexec "mamba env create -p ${conda_base}/envs/${conda_env_name} -f $conda_recipe || true"  # corner case: there is already a conda env with the same name in another conda_base, e.g. NGSAdmin
  set -eu

  [[ -n "${conda_env_path}" ]] && logwarn "A Conda environment with the name \"$conda_env_name\" is already present. Skipping environment creation" && return 0
  logsuccess "Environment installation completed"
}

download_databases() {
  # checks
  [[ -f $script_path/chewieSnake_download_EFSA_schemas.sh ]] || die "The Python script chewieSnake_download_EFSA_schemas.sh was not found"

  # Create Subdirectories
  [[ ! -d $schema_dir ]] && logexec mkdir -p $schema_dir

  # Download EFSA Schemas
  logexec $script_path/chewieSnake_download_EFSA_schemas.sh --schema_dir "$schema_dir" $arg_runtest_auto $arg_dryrun

  logsuccess "Database download completed"
}

symlink_to_ceph_databases() {
  # Create Subdirectories
  [[ ! -d $schema_dir/Salmonella_enterica ]] && logexec mkdir -p "$schema_dir/Salmonella_enterica"
  [[ ! -d $schema_dir/Listeria_monocytogenes ]] && logexec mkdir -p "$schema_dir/Listeria_monocytogenes"
  [[ ! -d $schema_dir/Escherichia_coli ]] && logexec mkdir -p "$schema_dir/Escherichia_coli"

  cd "$schema_dir" || die "Could not change to $schema_dir"
  # Salmonella
  [[ -d /home/NGSAdmin/HPC_referencedb/cgmlst/active_schemes/Salmonella/Salmonella_enterica_EFSA_cgMLST && ! -d $schema_dir/Salmonella_enterica/Salmonella_enterica_INNUENDO_cgMLST ]] && logexec "ln -s /home/NGSAdmin/HPC_referencedb/cgmlst/active_schemes/Salmonella/Salmonella_enterica_EFSA_cgMLST $schema_dir/Salmonella_enterica/Salmonella_enterica_INNUENDO_cgMLST"
  # Listeria
  [[ -d /home/NGSAdmin/HPC_referencedb/cgmlst/active_schemes/Listeria/Listeria_monocytogenes_Pasteur_cgMLST && ! -d $schema_dir/Listeria_monocytogenes/Listeria_monocytogenes_Pasteur_cgMLST ]] && logexec "ln -s /home/NGSAdmin/HPC_referencedb/cgmlst/active_schemes/Listeria/Listeria_monocytogenes_Pasteur_cgMLST $schema_dir/Listeria_monocytogenes/Listeria_monocytogenes_Pasteur_cgMLST"
  # Ecoli
  [[ -d /home/NGSAdmin/HPC_referencedb/cgmlst/active_schemes/Ecoli/Enterobase_Ecoli_cgmlst && ! -d $schema_dir/Escherichia_coli/Enterobase_Ecoli_cgmlst ]] && logexec "ln -s /home/NGSAdmin/HPC_referencedb/cgmlst/active_schemes/Ecoli/Enterobase_Ecoli_cgmlst $schema_dir/Escherichia_coli/Enterobase_Ecoli_cgmlst"

  logsuccess "Database symbolic linking complete"
}

run_test() {
  # checks
  [[ -f $script_path/chewieSnake_wrapper.sh ]] || die "The Python script chewieSnake_wrapper.sh was not found"

  # run analysis of test data
  logheader "Analysis of test data:"
  logexec $script_path/chewieSnake_wrapper.sh $arg_runtest_auto $arg_dryrun

  logsuccess "Analysis of test data completed"
}

check_success() {
  [[ -d ${conda_env_path:-} ]] && status_conda_env="${green}OK${normal}"
  [[ -d $schema_dir/Salmonella_enterica ]] && status_schema_salmonella="${green}OK${normal}"
  [[ -d $schema_dir/Listeria_monocytogenes ]] && status_schema_listeria="${green}OK${normal}"
  [[ -d $schema_dir/Escherichia_coli ]] && status_schema_ecoli="${green}OK${normal}"
  logheader "Database Status:"
  echo "
status_conda_env:|${status_conda_env:-"${red}FAIL${normal}"}
status_schema_salmonella:|${status_schema_salmonella:-"${red}FAIL${normal}"}
status_schema_listeria:|${status_schema_listeria:-"${red}FAIL${normal}"}
status_schema_ecoli:|${status_schema_ecoli:-"${red}FAIL${normal}"}
" | column -t -s "|"
  echo
}

## (3) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info

## Workflow
check_conda $conda_recipe_env_name
[[ "$interactive" == true ]] && interactive_query
[[ "$arg_mamba" == true ]] && setup_mamba
[[ "$arg_databases" == true ]] && download_databases
[[ "$arg_ceph_symlinks" == true ]] && symlink_to_ceph_databases
[[ "$arg_runtest" == true ]] && run_test
[[ "$arg_status" == false ]] && check_conda $conda_recipe_env_name  # check changes to conda
show_info
check_success

logglobal "All steps were logged to: $logfile"
logglobal "[STOP] $script_real"
echo "Thank you for installing ${pipeline}"
