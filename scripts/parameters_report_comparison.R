# parameters for manual debugging of report_cluster.Rmd
require(yaml, quietly = T, warn.conflicts = F)


#    workdir:
#workdir <- "/cephfs/abteilung4/Projects_NGS/AutoQC/Listeria/cgmlst"
#workdir <- "/cephfs/abteilung4/NRL_Listeria/Outbreaks/Chi1"
#workdir <- "/cephfs/abteilung4/NRL_Salmonella/Outbreaks/SE_Impfstamm_IDT"
#workdir <- "/cephfs/abteilung4/NRL_Salmonella/Outbreaks/SAL_Blockley_2021"
#workdir <- "/cephfs/abteilung4/NRL_Salmonella/Outbreaks/2019_SE_CT1734"
#workdir <- "/cephfs/abteilung4/NRL_Salmonella/Outbreaks/2019_Hadar"
workdir <- "/cephfs/abteilung4/NRL_Listeria/Outbreaks/Ny9"
workdir <- "/cephfs/abteilung4/NRL_Salmonella/Outbreak_matching_service/A05/20220111/chewiesnake"


#    input:
distance_matrix_file          <- file.path(workdir, "cgmlst_joined/distance_matrix.tsv")
distance_matrix_filtered_file <- file.path(workdir, "cgmlst_joined/distance_matrix_filtered.tsv")
allele_stats_file             <- file.path(workdir, "cgmlst/allele_statistics.tsv")
sample_meta_file              <- file.path(workdir, "cgmlst_joined/samples_related_meta.tsv")
samples_related_file          <- file.path(workdir, "cgmlst_joined/samples_related.txt")
match_overview_file           <- file.path(workdir, "cgmlst_joined/match_summary.txt")
nwk_all_file                  <- file.path(workdir, "cgmlst_joined/grapetree_mstree.tre")
nwk_filtered_file             <- file.path(workdir, "cgmlst_joined/grapetree_mstree_filtered.tre")
hashid_file                   <- file.path(workdir, "cgmlst/hashids.tsv")


#    params:
name   <- "external"
outdir <- "cgmlst_joined/"


#    config:
config.yaml <- file.path(workdir, "config_chewiesnake.yaml")
stopifnot(file.exists(config.yaml))
config <- read_yaml(config.yaml)

#    fix config: read_yaml() interpretes "address_range" wrongly as integer - not as string, hence, a manual correction in the next line
.x <- readr::read_lines(config.yaml)
config$parameters$clustering$address_range <- gsub(pattern = "    address_range: ", replacement = "", x = .x[grepl(pattern = "address_range", x = .x)])

#    overwrite config:
# config$parameters$clustering$address_range <- "1,5,10,20,50,100,200,1000"  # "1,3,7,10,15,30,50,100,200,1000"  "1,7,10,15,20,30"
# if (grepl("NRL_Listeria",   workdir)) {config$parameters$clustering$comparison_allele_database <- "/cephfs/abteilung4/Projects_NGS/AutoQC/Listeria/cgmlst/cgmlst/allele_profiles.tsv"}
# if (grepl("NRL_Salmonella", workdir)) {config$parameters$clustering$comparison_allele_database <- "/cephfs/abteilung4/NGS/Databases_autoqc/Salmonella/cgmlst/cgmlst/allele_profiles.tsv"}

# log <- file.path(workdir, "logs", "cluster_report.log")