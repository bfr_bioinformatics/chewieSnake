2020-04-20 (New version tag 2.1.0)
* Combined contig and read based wrapper into single wrapper; new option `--reads` when assembly is required
* Increased modularity: Splitting of code into topic snakemake files see `rules/`
* add pandoc to rmarkdown environment
* improved readme including link to more schemes, instructions with channel conda-forge and biopython dependencies
* --logdir parameter Option to redirect `.snakemake` log files
* --condaprefix Defaults to environment
* Outputs only a single allele distance metric. New parameter --distance_method
* No longer outputs all results in hashed and unhashed version

Tag 2.0.0
* New version uses snakemake --use-conda option. Simpler installation only requires snakemake software
* Includes interactive html report with `--report` option
* Computes cluster types and cluster address for a set of samples
* New read-based flavour: cgmlst analysis from raw reads including trimming (using fastp) and assembly (using shovill) as intermediate steps
* Read-based option has option `--compare`. Allows comparison to external hashed allele database for quick peer-to-peer allele comparison
* Wrappers for both flavours, assembly-based and read-based
