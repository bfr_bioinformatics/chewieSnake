# %% ChewieSnake: a Snakemake pipeline for cgMLST allele calling and allele hashing
# Input is a set of reports

# Python Standard Packages
import os
from datetime import datetime
import shutil

# Other Packages
import pandas as pd
import yaml

# %% Settings -------------------------------------------------------

shell.executable("bash")

# Set snakemake main workdir variable
workdir: config["workdir"]

# Collect sample names from sample list
samples_raw = pd.read_csv(config["samples"], sep = "\t", dtype = str).set_index('sender', inplace = False)  # use two steps to set first column as index_col in case samples are integers or a scientific notation
samples = samples_raw[~samples_raw.index.str.match('^#.*')]  # remove samples that are commented-out
samples_column = "assembly"

# Legacy Fallback for old config_chewiesnake.yaml files  # TODO: activate when refactoring to params is complete; remove legacy check after a year
# if not config.get('params', None):
#   config['params'] = config.pop('parameters')


# %% Functions ------------------------------------------------------

def _get_profile(wildcards, file = 'profiles'):
    return samples.loc[(wildcards.sender), [file]].dropna()[0]


def _get_assembly(wildcards, column_name = samples_column):
    return samples.loc[wildcards.sample, [column_name]].dropna()[0]


# %% All and Local Rules  -------------------------------------------

# only merging, no report
rule all_merge:
    input:
        "merged_db/allele_profiles.tsv",
        "merged_db/allele_statistics.tsv",
        "merged_db/sender_sample_info.tsv",
        "merged_db/distance_matrix.tsv",
        "merged_db/timestamps.tsv",
        "merged_db/orphan_samples.tsv", # from rule clustering
        "merged_db/sample_cluster_information.tsv", # from rule clustering
        #"merged_db/listofreports.txt", # result from checkpoint # TODO remove?
        "merged_db/report.html"



rule all_merge_with_clusterreports:
    input:
        "merged_db/allele_profiles.tsv",
        "merged_db/allele_statistics.tsv",
        "merged_db/sender_sample_info.tsv",
        "merged_db/distance_matrix.tsv",
        "merged_db/timestamps.tsv",
        "merged_db/orphan_samples.tsv", # from rule clustering
        "merged_db/listofreports.txt", # result from checkpoint # TODO remove?
        "merged_db/report.html"

rule all_noreport:
    input:
        "merged_db/allele_profiles.tsv",
        "merged_db/allele_statistics.tsv",
        "merged_db/sender_sample_info.tsv",
        "merged_db/distance_matrix.tsv",
        "merged_db/timestamps.tsv",

rule all_clustering:
    input:
        "merged_db/allele_profiles.tsv",
        "merged_db/allele_statistics.tsv",
        "merged_db/sender_sample_info.tsv",
        "merged_db/distance_matrix.tsv",
        "merged_db/timestamps.tsv",
        "merged_db/orphan_samples.tsv", # from rule clustering
        "merged_db/sample_cluster_information.tsv", # from rule clustering
        #"merged_db/samples2clusters.csv" # result from checkpoint
        #"merged_db/listofreports.txt", # result from checkpoint
        "merged_db/report.html"

rule all_reportonly:
    input:
        "merged_db/report.html"

# %% Main rules -----------------------------------------------------

## rule is necessary since the lambda function cannot be expanded
rule copy_profiles:
    input:
        profiles = lambda wildcards: _get_profile(wildcards,'profiles')
    output:
        merged_db = temporary("merged_db/tmp/allele_profiles_{sender}.tsv")
    message: "Copying all allele profiles"
    shell:
        """
        cat {input.profiles} | sed 's/^FILE/#FILE/' > {output}
        """

rule join_profiles:
    input:
        profiles = expand("merged_db/tmp/allele_profiles_{sender}.tsv",sender = samples.index)
    output:
        profiles = "merged_db/allele_profiles.tsv"
    message: "Merging all allele profiles"
    run:
        with open(output.profiles,'wb') as outfile:
            for i, fname in enumerate(input.profiles):
                with open(fname,'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile,outfile)  # Block copy rest of file from input to output without parsing

rule sample_sender_relation:
    input:
        profiles = "merged_db/tmp/allele_profiles_{sender}.tsv"
    output:
        info = temporary("merged_db/tmp/sender_sample_{sender}.tsv")
    message: "Extracting sender sample info"
    shell:
        """
        awk -F ' ' -v sender={wildcards.sender} '{{print $1"\t"sender}}' {input.profiles} > {output}
        """

rule sample_sender_relation_merge:
    input:
        info = expand("merged_db/tmp/sender_sample_{sender}.tsv",sender = samples.index)
    output:
        info = "merged_db/sender_sample_info.tsv"
    message: "Merging all sender info files"
    run:
        with open(output.info,'wb') as outfile:
            for i, fname in enumerate(input.info):
                with open(fname,'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile,outfile)  # Block copy rest of file from input to output without parsing

# rule is necessary since the lambda function cannot be expanded
rule copy_statistics:
    input:
        profiles = lambda wildcards: _get_profile(wildcards,'statistics')
    output:
        merged_db = temporary("merged_db/tmp/allele_statistics_{sender}.tsv")
    message: "Copying all allele statistics"
    shell:
        """
        cat {input.profiles} > {output}
        """

rule join_statistics:
    input:
        partner_stats = expand("merged_db/tmp/allele_statistics_{sender}.tsv",sender = samples.index)
    output:
        merged_stats = "merged_db/allele_statistics.tsv"
    message: "Merging all allele statistics"
    run:
        with open(output.merged_stats,'wb') as outfile:
            for i, fname in enumerate(input.partner_stats):
                with open(fname,'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile,outfile)  # Block copy rest of file from input to output without parsing

rule copy_timestamps:
    input:
        profiles = lambda wildcards: _get_profile(wildcards,'timestamps')
    output:
        merged_db = temporary("merged_db/tmp/timestamps_{sender}.tsv")
    message: "Copying all timestamps"
    shell:
        """
        cat {input.profiles} > {output}
        """

rule join_timestamps:
    input:
        profiles = expand("merged_db/tmp/timestamps_{sender}.tsv",sender = samples.index)
    output:
        merged_timestamps = "merged_db/timestamps.tsv"
    message: "Merging all timestamps"
    shell:
        "cat {input.profiles} | grep -Pv '^sample\tdate' > {output}"

# %% re-used rules from here on -------------------------------------

rule run_grapetree_merged_distance_matrix:
    input:
        merged_db = "merged_db/allele_profiles.tsv"
    output:
        distance_matrix_tsv = "merged_db/distance_matrix.tsv",
    conda: "envs/grapetree3.yaml"
    message: "Running rule grapetree"
    params:
        distance_option = config["parameters"]["grapetree_distance_method"],#
    shell:
        """
        grapetree -p {input} -m distance --missing {params.distance_option} | sed -E 's/[[:space:]]{{2,}}/ /' | tail -n+2 > {output.distance_matrix_tsv}
        """

rule run_grapetree_merged_mstree:
    input:
        merged_db = "merged_db/allele_profiles.tsv"
    output:
        mstree = "merged_db/grapetree_mstree.tre",
    conda: "envs/grapetree3.yaml"
    message: "Running rule grapetree for minimum spanning tree"
    params:
        distance_option = config["parameters"]["grapetree_distance_method"],#
        method = "MSTreeV2"
    shell:
        """
        grapetree -p {input} -m MSTreeV2 > {output.mstree}
        """

# %% checkpoint clustering ------------------------------------------

checkpoint clustering:
    input:
        distance_matrix_tsv = "merged_db/distance_matrix.tsv",
        mapping_sample2clustername_file = "merged_db/mapping_sample2clustername.tsv"
    output:
        clustering_dir = directory("merged_db/clustering"),
        orphansamples_file = "merged_db/orphan_samples.tsv"
    conda: "envs/rmarkdown.yaml"
    message: "Clustering of distance matrix"
    log: "logs/clustering.log"
    params:
        functions_file = {workflow.basedir},
        mapping_sample2clustername_file = "merged_db/mapping_sample2clustername.tsv",
        distance_threshold = config["parameters"]["distance_threshold"],# 20
        clustering_method = config["parameters"]["clustering_method"],#"average",
        potential_names_file = config["parameters"]["cluster_names_reservoir"]
    script:
        "scripts/clustering.R"

# match epi cluster names; only perform if file exists; make this output an optional input for report rule
rule match_external_cluster_names:
    input:
        external_cluster_names = config["parameters"]["external_cluster_names"],
        mapping_sample2clustername_file = "merged_db/mapping_sample2clustername.tsv",
        orphansamples_file = "merged_db/orphan_samples.tsv",# not used, but sets order of rule
    output:
        mapping_sample2externalclustername_file = "merged_db/mapping_sample2externalclustername.tsv",
    conda: "envs/rmarkdown.yaml"
    message: "Matching externally provided cluster names to internal clusters"
    log: "logs/match_external_cluster_names.log"
    script:
        "scripts/match_clusternames.R"

rule match_serovar:
    input:
        external_cluster_names = config["parameters"]["serovar_info"],
        mapping_sample2clustername_file = "merged_db/mapping_sample2clustername.tsv",
        orphansamples_file = "merged_db/orphan_samples.tsv",# not used, but sets order of rule
    output:
        mapping_sample2externalclustername_file = "merged_db/mapping_sample2serovar.tsv",
    message: "Matching externally provided serovar names to internal clusters"
    log: "logs/match_serovar_names.log"
    script:
        "scripts/match_clusternames.R"

#sub_distance matrices
rule cluster_distance_matrix:
    input:
        samples_in_cluster = "merged_db/clustering/samples_{i}.tsv",
        distance_matrix_tsv = "merged_db/distance_matrix.tsv",
    output:
        distance_matrix_tsv = "merged_db/clustering/{i}/distance_matrix.tsv"
    message: "Clustering of sub distance matrices"
    log: "logs/cluster_distance_matrix_{i}.log"
    params:
        cluster_matrix = True,
        functions_file = {workflow.basedir},
    script:
        "scripts/cluster_distancematrix.R"

# %% subcluster rules -----------------------------------------------

# persistent subclusters; make this optional ???
rule subclustering:
    input:
        distance_matrix_cluster_tsv = "merged_db/clustering/{i}/distance_matrix.tsv",
    output:
        subclusters = "merged_db/clustering/{i}/mapping_sample2subclustername.tsv"
    conda: "envs/rmarkdown.yaml"
    message: "Clustering of distance matrix for cluster"
    log: "logs/subclustering_{i}.log"
    params:
        functions_file = {workflow.basedir},
        mapping_sample2clustername_file = "merged_db/clustering/{i}/mapping_sample2subclustername.tsv",
        distance_thresholds = config["parameters"]["subcluster_distance_thresholds"],# [10,3],
        name_types = config["parameters"]["subcluster_name_types"],# ["numerical","greek-alphabet"], #
        clustering_method = config["parameters"]["clustering_method"],#"single",
    script:
        "scripts/subclustering.R"

# %% cluster_report rule --------------------------------------------

rule cluster_report:
    input:
        samples2cluster = "merged_db/clustering/samples_{i}.tsv",
        distance_matrix_tsv = "merged_db/clustering/{i}/distance_matrix.tsv",
        subclusters = "merged_db/clustering/{i}/mapping_sample2subclustername.tsv",
        mapping_sample2externalclustername_file = "merged_db/mapping_sample2externalclustername.tsv",
        sender_info = "merged_db/sender_sample_info.tsv",
        timestamps = "merged_db/timestamps.tsv",
    output:
        "merged_db/clustering/{i}/clusterreport.html"
    conda: "envs/rmarkdown.yaml"
    message: "Creating Rmarkdown report for cluster"
    log: "logs/cluster_report_{i}.log"
    params:
        workdir = config["workdir"],
        cluster = "{i}",
        export = True,
        distance_threshold = config["parameters"]["distance_threshold"],
        clustering_method = config["parameters"]["clustering_method"],
        mapping_sample2serovar = "merged_db/mapping_sample2serovar.tsv" if config["parameters"][
            "do_serovar_info"] else {},# param or input?,
        functions_file = {workflow.basedir},
        species_shortname = config["parameters"]["species_shortname"]  # "SE"
    script:
        "scripts/cluster_report.Rmd"


# %% collection rule ------------------------------------------------

# aggregate rules for collection of checkpoint dynamic files:
# important: expand results from intermediate files BUT glob_wildcards from checkpoint!!!
def aggregate_clusterreport(wildcards):
    checkpoint_output = checkpoints.clustering.get(**wildcards).output[0]
    return expand("merged_db/clustering/{i}/clusterreport.html",
        i = glob_wildcards(os.path.join(checkpoint_output,"samples_{i}.tsv")).i)


def aggregate_subclusters(wildcards):
    checkpoint_output = checkpoints.clustering.get(**wildcards).output[0]
    return expand("merged_db/clustering/{i}/mapping_sample2subclustername.tsv",
        i = glob_wildcards(os.path.join(checkpoint_output,"samples_{i}.tsv")).i)


# an aggregation over all produced clusters
rule aggregate_clusterreport:
    input:
        aggregate_clusterreport
    output:
        "merged_db/listofreports.txt"
    shell:
        """
        for file in {input}; do
            echo $file >> {output}
        done
        """

# %% gather_cluster_information rule --------------------------------

rule gather_cluster_information:
    input:
        sender_info = "merged_db/sender_sample_info.tsv",
        timestamps = "merged_db/timestamps.tsv",
        mapping_sample2externalclustername_file = "merged_db/mapping_sample2externalclustername.tsv",
        mapping_sample2clustername_file = "merged_db/mapping_sample2clustername.tsv",
        mapping_sample2serovar = "merged_db/mapping_sample2serovar.tsv" if config["parameters"][
            "do_serovar_info"] else {},# param or input?
        orphansamples_file = "merged_db/orphan_samples.tsv",# not used, but sets order of rule
        subclusters = aggregate_subclusters,# dynamic
    output:
        clustering_info = "merged_db/sample_cluster_information.tsv",
        orphan_info = "merged_db/sample_orphan_information.tsv"
    message: "Gathering cluster information"
    log: "logs/cluster_information.log"
    params:
        workdir = config["workdir"],
    script:
        "scripts/cluster_information.R"

# %% report_merged rule ---------------------------------------------

rule report_merged:
    input:
        distance_matrix = "merged_db/distance_matrix.tsv",
        allele_stats = "merged_db/allele_statistics.tsv",
        nwk = "merged_db/grapetree_mstree.tre",
        sender_info = "merged_db/sender_sample_info.tsv",
        timestamps = "merged_db/timestamps.tsv",
        mapping_sample2externalclustername_file = "merged_db/mapping_sample2externalclustername.tsv",
        clustering_info = "merged_db/sample_cluster_information.tsv",
        orphan_info = "merged_db/sample_orphan_information.tsv",
        # report_links = "merged_db/listofreports.txt", # TODO remove this to prevent generation of cluster reports
        # cluster_report = aggregate_clusterreport # TODO remove this to prevent generation of cluster reports
    output:
        "merged_db/report.html"
    conda: "envs/rmarkdown.yaml"
    message: "Creating Rmarkdown report for all samples"
    log: "logs/rmarkdown_report.log"
    params:
        workdir = config["workdir"],
        exportpath = "merged_db/exported_trees",
        export = True,
        distance_threshold = config["parameters"]["distance_threshold"],# default = 30
        clustering_method = config["parameters"]["clustering_method"],# default = "single",
        mapping_sample2serovar = "merged_db/mapping_sample2serovar.tsv",
        sample2serovar = config["parameters"]["serovar_info"],
        parameters = config["parameters"],
        species_shortname = config["parameters"]["species_shortname"], # "SE"
        functions_file = {workflow.basedir},
        # add_report_links = False, # TODO add to config, make this optional
        # report_links = "merged_db/listofreports.txt", # TODO make this an optional parameter
    script:
        "scripts/report_joined.Rmd"

# %% Logging Information --------------------------------------------

def pipeline_status_message(status: str) -> tuple:
    RESET = "\033[0m"
    RED = "\033[31m"
    GREEN = "\033[32m"

    timestamp = datetime.now()
    script_id = os.environ.get('script_id', 'unset')
    status_file = os.path.join(config['workdir'], f"pipeline_status_{config['pipeline'].lower()}.txt")
    status_chunk = f"{timestamp.strftime('%F %H:%M')}\t{snakemake.logging.logger.logfile}\t{os.environ.get('CONDA_PREFIX', 'CONDA_PREFIX_is_unset')}\t{os.environ.get('USER', 'unknown')}:{os.uname().nodename}\tShellScriptId:[{script_id}]\n"

    if status == "running":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"[START] {os.path.abspath(__file__)}"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "success":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{GREEN}[SUCCESS]{RESET} Pipeline status: Workflow finished successfully"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "error":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{RED}[ERROR]{RESET} Pipeline status: An error occurred"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"

    return status_file, status, message


onstart:
    status_file, status, message = pipeline_status_message(status = 'running')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onsuccess:
    status_file, status, message = pipeline_status_message(status = 'success')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onerror:
    status_file, status, message = pipeline_status_message(status = 'error')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)
