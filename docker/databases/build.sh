#!/bin/bash
set -euo pipefail

## Locate Self
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
repo_path=$(dirname $(dirname $script_path))

## Select branch or tag
git_branch="master"
db_facet="-efsa-20230501"

## Pull the latest version of the image, in order to populate the build cache:
docker pull bfrbioinformatics/chewiesnake:databases${db_facet} || true

## Build the databases:
echo "Building bfrbioinformatics/chewiesnake:databases${db_facet}" | tee -a $script_path/build_databases.log
printf -v buildbegin '%(%Y-%m-%d %H:%M:%S)T' -1
docker buildx build  \
  --file $script_path/Dockerfile  \
  --progress=plain  \
  --target databases  \
  --cache-from=bfrbioinformatics/chewiesnake:databases${db_facet}  \
  --build-arg GIT_BRANCH=$git_branch  \
  --build-arg BUILD_DATE=$(date +"%Y%m%d%H%M%S")  \
  --tag bfrbioinformatics/chewiesnake:databases${db_facet}  \
  $script_path |& tee -a $script_path/build_databases.log
printf -v buildfinish '%(%Y-%m-%d %H:%M:%S)T' -1

## Build duration on Gandalf: 755s
echo "Building time from $buildbegin till $buildfinish" | tee -a $script_path/build_databases.log

## Push the new versions:
echo "Pushing bfrbioinformatics/chewiesnake:databases${db_facet}"
docker push bfrbioinformatics/chewiesnake:databases${db_facet}
