#!/usr/bin/env python3

# Python Standard Packages
import argparse
import subprocess
import os
import sys

# Other Packages
from yaml import dump as yaml_dump, safe_load as yaml_load


# %% Functions

# TODO: Things to add [allele_caller, scheme.json, hashing_function, max_fraction_missing_loci = 5]

def argument_parser(pipeline: str = "chewieSnake",
                    repo_path: str = os.path.dirname(os.path.realpath(__file__))) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    # region # parse arguments
    # path arguments
    parser.add_argument('-l', '--sample_list', default = "/", type = os.path.abspath, help = '[REQUIRED] List of samples to analyze, as a two column tsv file with columns sample and assembly paths. Can be generated with provided script create_sampleSheet.sh')
    parser.add_argument('-d', '--working_directory', default = "/", type = os.path.abspath, help = '[REQUIRED] Working directory where results are saved')
    parser.add_argument('-c', '--config', default = "/", help = 'Path to pre-defined config file (other parameters will be ignored)')
    # docker arguments
    # parser.add_argument('--docker', default = "", help = 'Mapped volume path of the host system')  # TODO: add mode
    # chewbbaca arguments
    parser.add_argument('--scheme', default = "/", type = os.path.abspath, help = '[REQUIRED] Path to directory of the cgMLST scheme')
    parser.add_argument('--prodigal', default = "/", type = os.path.abspath, help = f'[REQUIRED] Path to prodigal_training_file (provided in chewbbaca, e.g. {repo_path}/CHEWBBACA/prodigal_training_files/Salmonella_enterica.trn')
    parser.add_argument('--reads', default = False, action = 'store_true', help = 'Input data is reads. Assemble (using shovill) prior to allele calling; default: "contigs"')
    parser.add_argument('--bsr_threshold', default = 0.6, type = float, help = 'blast scoring ratio threshold to use; default: 0.6')
    parser.add_argument('--size_threshold', default = 0.2, type = float, help = 'size threshold, default at 0.2 means alleles with size variation of +-20 percent will be tagged as ASM/ALM; default: 0.2')
    parser.add_argument('--max_fraction_missing_loci', default = 0.05, type = float, help = 'maximum fraction of missing loci. If a sample has fewer loci than it is marked as FAIL; default: 0.05')
    # grapetree arguments
    parser.add_argument('--distance_method', default = 3, type = int, help = 'Grapetree distance method; default: 3')
    parser.add_argument('--clustering_method', default = "single",
                        help = 'The agglomeration method to be used for hierarchical clustering. This should be (an unambiguous abbreviation of) one of "ward.D", "ward.D2", "single", "complete", "average" (= UPGMA), "mcquitty" (= WPGMA), "median" (= WPGMC) or "centroid" (= UPGMC); default: "single"')
    parser.add_argument('--distance_threshold', default = 10, type = int, help = 'A single distance threshold for the extraction of sub-trees; default: 10')
    parser.add_argument('--address_range', default = "1,5,10,20,50,100,200,1000", type = str, help = 'A comma separated set of cutoff values for defining the clustering address; default: "1,5,10,20,50,100,200,1000"')
    # chewieSnake comparison arguments
    parser.add_argument('--comparison', default = False, action = 'store_true', help = 'Compare these results to pre-computed allele database')
    parser.add_argument('--comparison_db', default = "NULL", help = 'Path to pre-computed allele database for comparison')
    parser.add_argument('--joining_threshold', default = 30, type = int, help = 'A distance threshold for joining data with comparsion_db; default: 30')
    # chewieSnake frameshifts arguments
    parser.add_argument('--remove_frameshifts', default = False, action = 'store_true', help = 'Remove frameshift alleles by deviating allele length')
    parser.add_argument('--allele_length_threshold', default = 0.1, type = float, help = 'Maximum tolerated allele length deviance compared to median allele length of locus; choose integer for "absolute frameshift mode and float (0..1) for "relative" frameshift mode; default: 0.1')
    parser.add_argument('--frameshift_mode', default = "relative", help = 'Whether to consider absolute or relative differences of allele length for frameshifts removal. Choose from "absolute" and "relative"; default: "relative"')
    # conditional trimming and assembly arguments
    parser.add_argument('--min_trimmed_length', default = 15, type = int, help = 'Minimum length of a read to keep; default: 15')
    parser.add_argument('--assembler', default = "spades", help = 'Assembler to use in shovill, choose from megahit velvet skesa spades; default: "spades"')
    parser.add_argument('--shovill_output_options', default = "", help = 'Extra output options for shovill; default: ""')
    parser.add_argument('--shovill_extraopts', default = "", help = 'Extra options for shovill; default: ""')
    parser.add_argument('--shovill_modules', default = "--noreadcorr", help = 'Module options for shovill, choose from --noreadcorr --trim --nostitch --nocorr --noreadcorr; default: "--noreadcorr"')
    parser.add_argument('--shovill_depth', default = 100, type = int, help = 'Sub-sample --R1/--R2 to this depth. Disable with --depth 0; default: 100')
    parser.add_argument('--shovill_tmpdir', default = "/tmp/", help = 'Fast temporary directory; default: ""')
    # chewiesnake arguments
    parser.add_argument('--noreport', default = False, action = 'store_true', help = 'Do not create HTML report')
    # parser.add_argument('--ephemeral', default = False, action = 'store_true', help = 'Remove most intermediate data. Analysis results are reduced to JSONs and reports')  # TODO: add mode
    parser.add_argument('--json', default = False, action = 'store_true', help = 'Only keep allele hashes and JSONs (implies --ephemeral). High-Throughput mode')
    parser.add_argument('--rule_directives', default = os.path.join(repo_path, "profiles", "smk_directives.yaml"), type = os.path.abspath,
                        help = f'Process DAG with rule grouping and/or rule prioritization via Snakemake rule directives YAML; default: {os.path.join(repo_path, "profiles", "smk_directives.yaml")} equals no directives')
    # conda arguments
    parser.add_argument('--use_conda', default = False, action = 'store_true', help = 'Utilize the Snakemake "--useconda" option, i.e. Smk rules require execution with a specific conda env')
    parser.add_argument('--conda_frontend', default = False, action = 'store_true', help = 'Do not use mamba but conda as frontend to create individual conda environments')
    parser.add_argument('--condaprefix', default = os.path.join(repo_path, "conda_env"), help = f'Path of default conda environment, enables recycling of built environments; default: {os.path.join(repo_path, "conda_env")}', metavar = 'CONDA_PREFIX')
    # cpu arguments
    parser.add_argument('-t', '--threads', default = 0, type = int,
                        help = f'Number of Threads/Cores to use. This overrides the "<{pipeline}>/profiles" settings. Note that samples can only be processed sequentially due to the required database access. However, the allele calling can be executed in parallel for the different loci')
    parser.add_argument('-p', '--threads_sample', default = 4, type = int, help = 'Number of Threads to use per sample in multi-threaded rules; default: 1', metavar = 'THREADS')
    parser.add_argument('-b', '--batch', default = "", type = str, help = 'Compute sample list in batches. Please state a fraction string, e.g. 1/3', metavar = 'FRACTION')
    parser.add_argument('-f', '--force', default = False, type = str, help = 'Snakemake force. Force recalculation of output (rule or file) specified here', metavar = 'RULE')
    parser.add_argument('--forceall', default = False, type = str, help = 'Snakemake force. Force recalculation of all steps', metavar = 'RULE')
    parser.add_argument('--profile', default = "none", type = str, help = f'(A) Full path or (B) directory name under "<{pipeline}>/profiles" of a Snakemake config.yaml with Snakemake parameters, e.g. available CPUs and RAM; default: "bfr.hpc"')
    parser.add_argument('-n', '--dryrun', default = False, action = 'store_true', help = 'Snakemake dryrun. Only calculate graph without executing anything')
    parser.add_argument('--unlock', default = False, action = 'store_true', help = 'Unlock a Snakemake execution folder if it had been interrupted')
    parser.add_argument('--logdir', default = "/", type = os.path.abspath, help = 'Path to directory where .snakemake output is to be saved')
    parser.add_argument('-V', '--version', default = False, action = 'store_true', help = 'Print program version')
    # endregion # parse arguments
    return parser


def check_scheme_lock(scheme):
    lockfile = os.path.join(scheme, ".lock")
    if os.path.exists(lockfile):
        print(f"ERROR: Scheme is locked! .lock file present in {scheme}")
        print(f"In multi-user environments, please review the file ownerships:")
        print(f"  ls -lA {scheme} | grep -vP '^total' | awk  '{{print $3}}' | sort | uniq -c")
        print(f"  find {scheme} -maxdepth 1 -user $USER")
        print(f"Afterwards, please remove the lockfile:")
        print(f"  rm -r {lockfile}")
        sys.exit(1)
    else:
        print("Scheme is not locked")


def evaluate_arguments(args: argparse.Namespace) -> argparse.Namespace:
    # version
    args.version_fixed = open(os.path.join(args.repo_path, 'VERSION'), 'r').readline().strip()
    try:
        args.version_git = subprocess.check_output(["git", "describe", "--always"], stderr = subprocess.DEVNULL, cwd = args.repo_path).decode('ascii').strip()
    except:
        args.version_git = args.version_fixed

    if args.version:
        print(f"{args.pipeline} version\t{args.version_fixed}")
        if not args.version_fixed == args.version_git:
            print(f"commit      version\t{args.version_git.lstrip('v')}")
        sys.exit(0)
    else:
        print(f"You are using {args.pipeline} version {args.version_fixed} ({args.version_git})")

    # check required arguments
    required_paths_to_check = {
        "-d/--working_directory": args.working_directory,
        "-l/--sample_list"      : args.sample_list,
        "--scheme"              : args.scheme,
        "--prodigal"            : args.prodigal
    }
    missing_required_paths = {key: path for key, path in required_paths_to_check.items() if path == '/'}
    if missing_required_paths:
        for key, path in missing_required_paths.items():
            print(f"ERROR: the following argument is required: {key}")
        sys.exit(1)

    # check dependency path - keep in sync with check_config()
    dependency_paths_to_check = {
        "-l/--sample_list": args.sample_list,
        "-c/--config"     : args.config,  # defaults to "/", hence, always exists
        "--scheme"        : args.scheme,
        "--prodigal"      : args.prodigal,
        "--comparison_db" : args.comparison_db if args.comparison else "/"
    }
    missing_dependency_paths = {key: path for key, path in dependency_paths_to_check.items() if not os.path.exists(path)}
    if missing_dependency_paths:
        for key, path in missing_dependency_paths.items():
            print(f"ERROR: path for argument {key} does not exist: {path}")
        sys.exit(1)

    # create working directory if it does not exist
    if not os.path.exists(args.working_directory):
        os.makedirs(args.working_directory)

    # set snakemake logdir
    args.logdir = args.working_directory if args.logdir == "/" else args.logdir
    print(f"Snakemake logs are saved in {args.logdir}/.snakemake/log")

    # find snakemake profile: first check relative paths, then absolute paths to make args.profile absolute
    if os.path.exists(os.path.join(args.repo_path, "profiles", args.profile)):
        smk_profile = os.path.join(args.repo_path, "profiles", args.profile)
    elif os.path.exists(os.path.realpath(args.profile)):
        smk_profile = os.path.realpath(args.profile)
    elif os.path.exists(os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.join(args.repo_path, "profiles", "bfr.hpc")):
        smk_profile = os.path.join(args.repo_path, "profiles", "bfr.hpc")
    else:
        print(f"ERROR: path to Snakemake config.yaml {args.profile} does not exist")
        sys.exit(1)
    args.profile = smk_profile

    # set snakefile according to input sequence type
    args.snakefile = os.path.join(args.repo_path, "chewieSnake_readbased.smk") if args.reads else os.path.join(args.repo_path, "chewieSnake_fromcontigs.smk")

    # return modified args
    return args


def check_sample_list(sample_list):
    # Check whether sample list has header
    type1 = "sample\tfq1\tfq2\n"
    type2 = "sample\tassembly\n"
    type3 = "sample\tlong\tshort1\tshort2\n"

    with open(sample_list, 'r') as handle:
        header_line = handle.readline()

    if header_line != type1 and header_line != type2:
        print(f"ERROR: sample list {sample_list} does not have a proper header. Make sure that first line is tab-separated:\n{type1} -- or --\n{type2}")
        sys.exit(1)
    elif header_line == type1:
        print("Input is fastq")
    else:
        print("Input is assembly")


def create_config(args: argparse.Namespace) -> dict:
    # region # create config dictionary
    config_dict = {
        'pipeline'   : args.pipeline,
        'version'    : args.version_fixed,
        'git_version': args.version_git,
        'workdir'    : args.working_directory,
        'samples'    : args.sample_list,
        'smk_params' : {
            'log_directory'  : args.logdir,
            'profile'        : args.profile,
            'rule_directives': args.rule_directives,
            'threads'        : args.threads_sample,
            'batch'          : args.batch,
            # 'docker'         : args.docker
        },
        'parameters' : {  # TODO: refactor to params
            'chewbbaca'         : {'bsr_theshold'             : args.bsr_threshold,
                                   'size_threshold'           : args.size_threshold,
                                   'scheme_dir'               : args.scheme,
                                   'prodigal_training_file'   : args.prodigal,
                                   'allele_caller'            : {'name': 'chewBBACA', 'version': '2.0.12'},
                                   'allele_hasher'            : "CRC32 unsigned integer",
                                   'max_fraction_missing_loci': args.max_fraction_missing_loci
                                   },
            'clustering'        : {'clustering_method'         : args.clustering_method,
                                   'distance_threshold'        : args.distance_threshold,
                                   'address_range'             : args.address_range,
                                   'comparison_allele_database': args.comparison_db,
                                   'joining_threshold'         : args.joining_threshold,
                                   'grapetree_distance_method' : args.distance_method
                                   },
            'frameshift_removal': {'remove_frameshifts': args.remove_frameshifts,
                                   'mode'              : args.frameshift_mode,
                                   'threshold'         : args.allele_length_threshold
                                   }
        }
    }
    # endregion # create config dictionary

    # region # create conditional config dictionary
    if args.reads:
        assembly_dict = {
            'fastp'  : {'length_required': args.min_trimmed_length},
            'shovill': {'assembler'     : args.assembler,
                        'output_options': args.shovill_output_options,
                        'depth'         : args.shovill_depth,
                        'tmpdir'        : args.shovill_tmpdir,
                        'extraopts'     : args.shovill_extraopts,
                        'modules'       : args.shovill_modules
                        }
        }
        config_dict['parameters'].update(assembly_dict)
    # endregion # create conditional config dictionary

    # write config dictionary to yaml
    with open(args.config_file, 'w') as outfile:
        yaml_dump(data = config_dict, stream = outfile, default_flow_style = False, sort_keys = False)
    print(f"Config file saved to {args.config_file}")

    return config_dict


def check_config(config: dict) -> bool:
    # check required paths
    required_paths_to_check = {
        "config.workdir"                 : config.get('workdir', ''),
        "config.samples"                 : config.get('samples', ''),
        "config.smk_params.log_directory": config.get('smk_params', {}).get('log_directory', ''),
        # "config.smk_params.profile"      : config.get('smk_params', {}).get('profile', '')  # TODO: remove legacy check after a year and activate this line
    }
    missing_required_paths = {key: path for key, path in required_paths_to_check.items() if not os.path.exists(path)}
    if missing_required_paths:
        for key, path in missing_required_paths.items():
            print(f"ERROR: the config-defined path does not exist: {key} -> {path}")
        sys.exit(1)

    # legacy check
    if not os.path.exists(config.get('smk_params').get('profile')):
        print(f"WARNING: the config_chewiesnake.yaml still holds only the snakemake profile (dir)name. Please update the value of 'profile' to the full path of the profile directory where the config.yaml is located")

    # check dependency paths - keep in sync with evaluate_arguments()
    dependency_paths_to_check = {
        "config.parameters.chewbbaca.scheme_dir"                 : config.get('parameters', {}).get('chewbbaca', {}).get('scheme_dir', ''),
        "config.parameters.chewbbaca.prodigal_training_file"     : config.get('parameters', {}).get('chewbbaca', {}).get('prodigal_training_file', ''),
        "config.parameters.clustering.comparison_allele_database": config.get('parameters', {}).get('clustering', {}).get('comparison_allele_database', '') if config.get('parameters', {}).get('clustering', {}).get('comparison_allele_database', '') != 'NULL' else '/',
        # if string is NULL skip test by checking root
    }
    missing_dependency_paths = {key: path for key, path in dependency_paths_to_check.items() if not os.path.exists(path)}
    if missing_dependency_paths:
        # print(f"WARNING: You are using an old config structure from v3.2.x or earlier. Please create a new config_chewiesnake.yaml") if not config.get('params', None) else None  # TODO: activate when refactoring to params is complete; update version in message
        for key, path in missing_dependency_paths.items():
            print(f"ERROR: path for argument {key} does not exist: {path}")
            if key == "--amrfinder_db":
                print(f"Make sure amrfinder_db path exists. In doubt run amrfinder -u to get the latest release")
        sys.exit(1)

    return True


def build_snakemake_call(args: argparse.Namespace) -> str:
    # force mode
    force_mode = "--forceall" if args.forceall else ""
    force_mode = "--force" if args.force else force_mode

    # user-specified force target; force_targets are mutually exclusive
    force_target = args.forceall if args.forceall else ""
    force_target = args.force if args.force else force_target

    # all-rule force target, add new all rules here
    force_target = "all_noreport" if args.noreport else force_target
    force_target = "all_database_comparison" if args.comparison else force_target
    force_target = "all_json" if args.json else force_target
    force = f"{force_mode} {force_target}"

    # other workflow options
    batch_rule = "summary_report"
    batch = f"--batch {batch_rule}={args.batch}" if args.batch else ""
    threads = "--cores" if args.profile == "none" else ""  # use all available cores, if no profile is specified
    threads = f"--cores {args.threads}" if args.threads > 0 else threads  # override cores by commandline arg if provided
    dryrun = "--quiet --dryrun" if args.dryrun else ""  # TODO: add rules to --quiet, once snakemake is updated over v7.5.0
    unlock = "--unlock" if args.unlock else ""

    # create command
    command = f"snakemake --profile {args.profile} --snakefile {args.snakefile} --configfile {args.config_file} " \
              f"{batch} {threads} {dryrun} {unlock} {force}"

    if args.use_conda:
        frontend = "conda" if args.conda_frontend else "mamba"
        command += f" --use-conda --conda-prefix {args.condaprefix} --conda-frontend {frontend}"

    return command


# %% Main

def main():
    # create commandline parser
    parser = argument_parser()

    # parse arguments and create namespace object
    args = parser.parse_args()

    # append settings to args namespace
    args.pipeline = "chewieSnake"
    args.repo_path = os.path.dirname(os.path.realpath(__file__))
    args.config_file = os.path.join(args.working_directory, f"config_{args.pipeline.lower()}.yaml") if args.config == "/" else args.config

    if args.config == "/":
        # evaluate arguments
        args = evaluate_arguments(args = args)

        # write a new config file
        config = create_config(args = args)
    else:
        # load config preset
        print(f"Config file preset used from {args.config_file}")
        with open(args.config_file, 'r') as file:
            config = yaml_load(file)

        # update args with config or fall back to defaults
        args.sample_list = config.get('samples')
        args.logdir = config.get('smk_params', {}).get('log_directory', args.logdir)
        args.profile = config.get('smk_params', {}).get('profile', args.profile) if args.profile == "none" else args.profile  # if --profile argument is not provided, i.e. profile is default, then use config profile
        args.threads_sample = config.get('threads_sample', args.threads) if args.threads == 1 else args.threads  # if --threads_sample argument is not provided, i.e. profile is default, then use config threads_sample
        args.reads = True if config.get('shovill') else False  # presence of config branch shovill is a proxy for switch args.reads
        args.snakefile = os.path.join(args.repo_path, "chewieSnake_readbased.smk") if args.reads else os.path.join(args.repo_path, "chewieSnake_fromcontigs.smk")

        # check config
        check_config(config = config)

    print("Executing the read-based version including assembly step") if args.reads else print("Executing the contig-based version")

    # other checks
    check_sample_list(sample_list = config.get('samples'))
    check_scheme_lock(scheme = args.scheme)

    # run snakemake workflow
    smk_command = build_snakemake_call(args = args)
    print(smk_command)
    subprocess.call(args = smk_command, shell = True, cwd = args.logdir)  # start snakemake from logdir


# %% Main Call

if __name__ == '__main__':
    main()
