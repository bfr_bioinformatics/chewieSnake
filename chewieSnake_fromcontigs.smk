# %% ChewieSnake: a Snakemake pipeline for cgMLST allele calling and allele hashing

# Python Standard Packages
import os
from datetime import datetime
import shutil

# Other Packages
import pandas as pd
import yaml

# %% Settings -------------------------------------------------------

shell.executable("bash")

# Set snakemake main workdir variable
workdir: config["workdir"]

# Import rule directives
with open(config["smk_params"]["rule_directives"],"r") as stream:
    try:
        rule_directives = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        rule_directives = {'rule': ''}

# Collect sample names from sample list
samples_raw = pd.read_csv(config["samples"], sep = "\t", dtype = str).set_index('sample', inplace = False)  # use two steps to set first column as index_col in case samples are integers or a scientific notation
samples = samples_raw[~samples_raw.index.str.match('^#.*')]  # remove samples that are commented-out
samples_column = "assembly"

# Legacy Fallback for old config_chewiesnake.yaml files  # TODO: activate when refactoring to params is complete; remove legacy check after a year
# if not config.get('params', None):
#   config['params'] = config.pop('parameters')
pass

# %% Functions ------------------------------------------------------

def _get_assembly(wildcards, column_name = samples_column):
    return samples.loc[wildcards.sample, [column_name]].dropna()[0]


# %% All and Local Rules  -------------------------------------------

localrules: all,all_noreport,all_json,all_chewbbaca,all_hashed,all_unhashed,all_report,all_clustering,all_database_comparison

rule all:
    input:
        expand("cgmlst/{sample}/results_alleles.tsv",sample = samples.index),
        "cgmlst/allele_profiles.tsv",
        "cgmlst/distance_matrix.tsv",
        "cgmlst/grapetree_mstree.tre",
        "cgmlst/allele_statistics.tsv",
        "cgmlst/error_counts.tsv",
        "cgmlst/timestamps.tsv",
        "cgmlst/hashids.tsv",
        expand("cgmlst/{sample}/allele_profiles.tsv",sample = samples.index),
        expand("cgmlst/{sample}/allele_length.tsv",sample = samples.index) if config["parameters"]["frameshift_removal"]["remove_frameshifts"] else [],# frameshift detection
        "cgmlst/summary_masked_loci.tsv" if config["parameters"]["frameshift_removal"]["remove_frameshifts"] else [],# frameshift detection
        expand("cgmlst/json/{sample}.chewiesnake.json",sample = samples.index),# json files
        "reports/cgmlst_report.html"

rule all_noreport:
    input:
        expand("cgmlst/{sample}/results_alleles.tsv",sample = samples.index),
        "cgmlst/allele_profiles.tsv",
        "cgmlst/distance_matrix.tsv",
        "cgmlst/grapetree_mstree.tre",
        "cgmlst/allele_statistics.tsv",
        "cgmlst/error_counts.tsv",
        "cgmlst/timestamps.tsv",
        "cgmlst/hashids.tsv",
        expand("cgmlst/{sample}/allele_profiles.tsv",sample = samples.index),
        expand("cgmlst/{sample}/allele_length.tsv",sample = samples.index) if config["parameters"]["frameshift_removal"]["remove_frameshifts"] else [],# frameshift detection
        "cgmlst/summary_masked_loci.tsv" if config["parameters"]["frameshift_removal"]["remove_frameshifts"] else [],# frameshift detection
        expand("cgmlst/json/{sample}.chewiesnake.json",sample = samples.index)  # json files

rule all_json:
    input:
        expand("cgmlst/json/{sample}.chewiesnake.json",sample = samples.index)  # json files

rule all_chewbbaca:
    input:
        "cgmlst/error_counts.tsv",
        expand("cgmlst/{sample}/results_alleles.tsv",sample = samples.index)

rule all_hashed:
    input:
        "cgmlst/error_counts.tsv",
        expand("cgmlst/{sample}/results_alleles.tsv",sample = samples.index),
        "cgmlst/distance_matrix.tsv"

rule all_unhashed:
    input:
        expand("cgmlst/{sample}/results_alleles.tsv",sample = samples.index),
        "cgmlst/allele_profiles.tsv",
        "cgmlst/distance_matrix.tsv",
        "cgmlst/distance_matrix_unhashed.tsv",
        "cgmlst/grapetree_mstree_unhashed.tre"
        "cgmlst/allele_statistics.tsv",
        "cgmlst/error_counts.tsv"

rule all_report:
    input:
        "cgmlst/error_counts.tsv",
        expand("cgmlst/{sample}/results_alleles.tsv",sample = samples.index),
        "cgmlst/allele_profiles.tsv",
        "cgmlst/distance_matrix.tsv",
        "reports/cgmlst_report.html"

rule all_clustering:
    input:
        "cgmlst/error_counts.tsv",
        expand("cgmlst/{sample}/results_alleles.tsv",sample = samples.index),
        "cgmlst/allele_profiles.tsv",
        "cgmlst/distance_matrix.tsv",
        "cgmlst/distance_matrix_unhashed.tsv",
        "cgmlst/clustering"

rule all_database_comparison:
    input:
        "cgmlst/allele_statistics.tsv",
        "cgmlst_joined/allele_profiles.tsv",
        "cgmlst_joined/allele_profiles_filtered.tsv",
        "cgmlst_joined/distance_matrix.tsv",
        "cgmlst_joined/distance_matrix_filtered.tsv",
        "cgmlst/timestamps.tsv",
        "cgmlst_joined/hashids.tsv",
        expand("cgmlst/{sample}/allele_profiles.tsv",sample = samples.index),
        expand("cgmlst/{sample}/allele_length.tsv",sample = samples.index) if
        config["parameters"]["frameshift_removal"]["remove_frameshifts"] else [],# frameshift detection
        expand("cgmlst/json/{sample}.chewiesnake.json",sample = samples.index),# json files
        "reports/cgmlst_comparison_report.html"


# %% Import Default Rules -------------------------------------------

include: "rules/allele_calling.smk"
include: "rules/grapetree.smk"
include: "rules/clustering.smk"
include: "rules/reporting.smk"
include: "rules/joining.smk"
include: "rules/hashid.smk"
include: "rules/json.smk"

# %% Logging Information --------------------------------------------

def pipeline_status_message(status: str) -> tuple:
    RESET = "\033[0m"
    RED = "\033[31m"
    GREEN = "\033[32m"

    timestamp = datetime.now()
    script_id = os.environ.get('script_id', 'unset')
    status_file = os.path.join(config['workdir'], f"pipeline_status_{config['pipeline'].lower()}.txt")
    status_chunk = f"{timestamp.strftime('%F %H:%M')}\t{snakemake.logging.logger.logfile}\t{os.environ.get('CONDA_PREFIX', 'CONDA_PREFIX_is_unset')}\t{os.environ.get('USER', 'unknown')}:{os.uname().nodename}\tShellScriptId:[{script_id}]\n"

    if status == "running":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"[START] {os.path.abspath(__file__)}"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "success":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{GREEN}[SUCCESS]{RESET} Pipeline status: Workflow finished successfully"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "error":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{RED}[ERROR]{RESET} Pipeline status: An error occurred"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"

    return status_file, status, message


onstart:
    status_file, status, message = pipeline_status_message(status = 'running')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onsuccess:
    status_file, status, message = pipeline_status_message(status = 'success')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onerror:
    status_file, status, message = pipeline_status_message(status = 'error')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)
