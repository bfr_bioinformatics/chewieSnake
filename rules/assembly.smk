# rules trimming ------------

rule run_fastp:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards, 'fq2')
    output:
        r1 = "trimmed/{sample}_R1.fastq.gz",
        r2 = "trimmed/{sample}_R2.fastq.gz",
        json = "trimmed/reports/{sample}.json",
        html = "trimmed/reports/{sample}.html"
    message: "Running fastp on sample {wildcards.sample}"
    conda: "../envs/fastp.yaml"
    log: "logs/fastp_{sample}.log"
    threads: config["smk_params"]["threads"]
    params:
        length_required = config["parameters"]["fastp"]["length_required"]
    shell:
        "fastp --in1 {input.r1} --out1 {output.r1} --in2 {input.r2} --out2 {output.r2} --detect_adapter_for_pe --json {output.json} --html {output.html} --thread {threads} --report_title 'Sample {wildcards.sample}' --verbose --length_required {params.length_required} 2>&1 | tee {log}"


rule parse_fastp:
    input:
        json = "trimmed/reports/{sample}.json",
        html = "trimmed/reports/{sample}.html"
    output:
        tsv = "trimmed/reports/{sample}.tsv"
    message: "Parsing fastp json report"
    params:
        workdir = config["workdir"]
    run:
        with open(input.json,'r') as handle:
          data = json.load(handle)
          
        link_path = os.path.join(params.workdir, input.html)
        header = "sample\ttotal_reads_before\ttotal_bases_before\ttotal_reads_after\ttotal_bases_after\tq20_rate_after\tq30_rate_after\tduplication_rate\tinsert_size_peak\tlink_to_report" # TODO: \tadapter_percentage"
        datalist = [wildcards.sample, data["summary"]["before_filtering"]["total_reads"],data["summary"]["before_filtering"]["total_bases"],data["summary"]["after_filtering"]["total_reads"],data["summary"]["after_filtering"]["total_bases"],data["summary"]["after_filtering"]["q20_rate"],data["summary"]["after_filtering"]["q30_rate"],data["duplication"]["rate"],data["insert_size"]["peak"], link_path]   # TODO: ,str(float(data["adapter_cutting"]["adapter_trimmed_bases"])/float(data["summary"]["before_filtering"]["total_bases"])*100)]
        with open (output.tsv,"w") as outfile:
            outfile.write(header+"\n")
            writer=csv.writer(outfile, delimiter='\t')
            writer.writerow(datalist)


# optional:
rule collect_fastp_stats:
    input:
        expand('trimmed/reports/{sample}.tsv', sample=samples.index)
    output:
        "reports/fastp_summary.tsv"
    message: "Collecting fastp stats"
    shell:
        """
        cat {input[0]} | head -n 1 > {output}
        for i in {input}; do 
            cat ${{i}} | tail -n +2 >> {output}
        done
        """

rule qc_html_report:
    input:
        workdir = config["workdir"],
        stats = "reports/fastp_summary.tsv",
    output:
        "reports/fastp_summary.html"
    conda: "../envs/rmarkdown.yaml"
    message: "Creating QC Rmarkdown report"
    script:
        "../scripts/write_QC_report.Rmd"


# assembly rules -------------------------------------------------------

rule assemble:
  input:
    r1 = "trimmed/{sample}_R1.fastq.gz",
    r2 = "trimmed/{sample}_R2.fastq.gz",
  output:
    contigs = "Assembly/{sample}/contigs.fa"
  log: "logs/{sample}_assembly.log"
  threads: config["smk_params"]["threads"]
  params:
      outdir = directory("Assembly/{sample}"),
      assembler = config["parameters"]["shovill"]["assembler"],
      output_options = config["parameters"]["shovill"]["output_options"],
      extraopts = config["parameters"]["shovill"]["extraopts"],
      modules = config["parameters"]["shovill"]["modules"]
  conda: "../envs/shovill.yaml"
  message: "Running rule assemble on sample {wildcards.sample}"
  shell:
      """shovill --force {params.modules} {params.output_options} --assembler {params.assembler} --cpus {threads} --outdir {params.outdir} --R1 {input.r1} --R2 {input.r2}"""


rule rename_contigfile:
    input:
        contigs = "Assembly/{sample}/contigs.fa"
    output:
        contigs = "Assembly/assembly/{sample}.fasta"
    message: "Running rule rename_contigfile on sample {wildcards.sample}"
    shell:
        "cp {input} {output}"


