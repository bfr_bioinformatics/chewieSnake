# rules that create an rmarkdown report

rule report:
    input:
        distance_matrix = "cgmlst/distance_matrix.tsv",
        allele_stats = "cgmlst/allele_statistics.tsv",
        nwk  = "cgmlst/grapetree_mstree.tre",
        timestamps = "cgmlst/timestamps.tsv",
        hashids="cgmlst/hashids.tsv",
        masked_loci =  "cgmlst/summary_masked_loci.tsv" if config["parameters"]["frameshift_removal"]["remove_frameshifts"] else [], # frameshift detection
    output:
        report = "reports/cgmlst_report.html"
    log: "logs/report_chewiesnake.log"
    params:
        exportpath = "cgmlst/exported_trees",
        export = True,
    conda: "../envs/rmarkdown.yaml"
    message: "Creating chewieSnake report"
    script:
        "../scripts/report.Rmd"


rule comparison_report:
    input:
        workdir = config["workdir"],
        sample_meta = "cgmlst_joined/samples_related_meta.tsv",
        samples_related = "cgmlst_joined/samples_related.txt",
        match_overview = "cgmlst_joined/match_summary.txt",
        distance_matrix_filtered = "cgmlst_joined/distance_matrix_filtered.tsv",
        distance_matrix = "cgmlst_joined/distance_matrix.tsv",
        allele_stats = "cgmlst/allele_statistics.tsv",
        mstree_all = "cgmlst_joined/grapetree_mstree.tre",
        mstree_filtered = "cgmlst_joined/grapetree_mstree_filtered.tre",
        hashids="cgmlst/hashids.tsv",
    output:
        report = "reports/cgmlst_comparison_report.html"
    log: "logs/report_chewiesnake_comparison.log"
    message: "Creating chewieSnake comparison report"
    params:
        name = "external",
        outdir = "cgmlst_joined/"
    conda: "../envs/rmarkdown.yaml"
    script:
        "../scripts/report_comparison.Rmd"
