# Additional rules

# rules for allele length analysis

# once per dataset
rule median_allele_length:
    input:
        scheme_dir = config["parameters"]["chewbbaca"]["scheme_dir"],
    output:
        scheme_length_profile = "cgmlst/scheme_length_profile.tsv"
    message: "Compute median allele length per locus"
    script:
        "../scripts/compute_median_allelelength.R"

# extract allele length
rule get_sample_allele_length:
    input:
        contig_info = "cgmlst/{sample}/results_contigsInfo.tsv"
    output:
        sample_length_profile = "cgmlst/{sample}/allele_length.tsv"
    message: "Compute allele length per locus for sample {wildcards.sample}"
    script:
        "../scripts/get_allelelength.R"

# extract allele length
rule mask_frameshift_alleles:
    input:
        sample_length_profile = "cgmlst/{sample}/allele_length.tsv",
        scheme_length_profile = "cgmlst/scheme_length_profile.tsv",
        allele_profile = "cgmlst/{sample}/allele_profiles.tmp"
    output:
        allele_profile_masked = "cgmlst/{sample}/allele_profiles.fsremoved.tsv",
        masked_sites = "cgmlst/{sample}/masked_loci.tsv"
    message: "Mask frameshift alleles for sample {wildcards.sample}"
    params:
        mode = config["parameters"]["frameshift_removal"]["mode"], # e.g."relative" or absolute 
        threshold = config["parameters"]["frameshift_removal"]["threshold"], #e.g. 0.1 # 0.01 # 10 
    script:
        "../scripts/mask_alleles.R"

