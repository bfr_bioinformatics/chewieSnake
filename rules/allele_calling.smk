ruleorder: run_chewbbaca_fromcontigs > run_chewbbaca_fromreads


rule run_chewbbaca_fromreads:
    input:
        contigs = "Assembly/assembly/{sample}.fasta"
    output:
        allele_profile = "cgmlst/{sample}/results_alleles.tsv",
        allele_stats = "cgmlst/{sample}/results_statistics.tsv",
        contig_info = "cgmlst/{sample}/results_contigsInfo.tsv"
    message: "Running chewbbaca for sample {wildcards.sample}"
    log: "logs/chewbbaca_{sample}.log"
    threads: 1000  # must be higher than cores to avoid parallel execution
    conda: "../envs/chewbbaca.yaml"
    params:
        outdir = "cgmlst/{sample}",
        bsr_threshold = config["parameters"]["chewbbaca"]["bsr_theshold"],#0.6
        size_threshold = config["parameters"]["chewbbaca"]["size_threshold"],#--st [ST] size threshold, default at 0.2 means alleles with size variation of +-20 percent will be tagged as ASM/ALM
        scheme_dir = config["parameters"]["chewbbaca"]["scheme_dir"],#"/cephfs/abteilung4/deneke/ref/chewbakka/enterobase_senterica_cgmlst",
        scheme_lock = os.path.join(config["parameters"]["chewbbaca"]["scheme_dir"],".lock"),
        prodigal_training_file = config["parameters"]["chewbbaca"]["prodigal_training_file"]  #"/home/DenekeC/software/chewBBACA/CHEWBBACA/prodigal_training_files/Salmonella_enterica.trn"
    shell:
        """
        if [[ ! -d {params.scheme_lock} ]];then
            # lock scheme
            mkdir -p {params.scheme_lock}

            # run chewBBaca
            echo "{workflow.basedir}/chewBBACA/CHEWBBACA/chewBBACA.py AlleleCall --st {params.size_threshold} --fr --bsr {params.bsr_threshold} -i {input} -g {params.scheme_dir} -o {params.outdir} --cpu {threads} --ptf {params.prodigal_training_file} -v &>> {log}" | tee {log}
            {workflow.basedir}/chewBBACA/CHEWBBACA/chewBBACA.py AlleleCall --st {params.size_threshold} --fr --bsr {params.bsr_threshold} -i {input} -g {params.scheme_dir} -o {params.outdir} --cpu {threads} --ptf {params.prodigal_training_file} -v &>> {log}

            # unlock scheme
            rm -r {params.scheme_lock}
        else 
            echo "ERROR: Scheme {params.scheme_dir} is locked!" | tee {log}
            exit 1
        fi
        """

rule run_chewbbaca_fromcontigs:
    input:
        lambda wildcards: _get_assembly(wildcards,'assembly')
    output:
        allele_profile = "cgmlst/{sample}/results_alleles.tsv",
        allele_stats = "cgmlst/{sample}/results_statistics.tsv",
        contig_info = "cgmlst/{sample}/results_contigsInfo.tsv"
    message: "Running chewbbaca for sample {wildcards.sample}"
    log: "logs/chewbbaca_{sample}.log"
    threads: 1000  # must be higher than cores to avoid parallel execution
    conda: "../envs/chewbbaca.yaml"
    params:
        outdir = "cgmlst/{sample}",
        bsr_threshold = config["parameters"]["chewbbaca"]["bsr_theshold"],# default = 0.6
        size_threshold = config["parameters"]["chewbbaca"]["size_threshold"],#--st [ST] size threshold, default at 0.2 means alleles with size variation of +-20 percent will be tagged as ASM/ALM
        scheme_dir = config["parameters"]["chewbbaca"]["scheme_dir"],
        scheme_lock = os.path.join(config["parameters"]["chewbbaca"]["scheme_dir"],".lock"),
        prodigal_training_file = config["parameters"]["chewbbaca"]["prodigal_training_file"]
    shell:
        """
        if [[ ! -d {params.scheme_lock} ]];then
            # lock scheme
            echo "mkdir -p {params.scheme_lock}"
            mkdir -p {params.scheme_lock}

            # run chewBBaca
            echo "{workflow.basedir}/chewBBACA/CHEWBBACA/chewBBACA.py AlleleCall --st {params.size_threshold} --fr --bsr {params.bsr_threshold} -i {input} -g {params.scheme_dir} -o {params.outdir} --cpu {threads} --ptf {params.prodigal_training_file} -v"  | tee {log}
            {workflow.basedir}/chewBBACA/CHEWBBACA/chewBBACA.py AlleleCall --st {params.size_threshold} --fr --bsr {params.bsr_threshold} -i {input} -g {params.scheme_dir} -o {params.outdir} --cpu {threads} --ptf {params.prodigal_training_file} -v &>> {log}

            # unlock scheme
            rm -r {params.scheme_lock}
        else 
            echo "ERROR: Scheme {params.scheme_dir} is locked!" | tee {log}
            exit 1
        fi
        """

# ----------

# clean profile: rename sample name instead of assembly file name, replace special chewbbaca entries with "-", fix file ending
rule clean_profile:
    input:
        "cgmlst/{sample}/results_alleles.tsv"
    output:
        "cgmlst/{sample}/allele_profiles.tmp"
    message: "Cleaning allele profile for sample {wildcards.sample}"
    log: "logs/clean_profile_{sample}.log"
    shell:
        """
        head -n 1 {input} | sed 's/^FILE/#FILE/' > {output}
        paste <(echo {wildcards.sample}) <(tail -n 1 {input} | cut -f 2- | sed 's/INF-//g' | sed -e 's/ALM\|ASM\|LNF\|NIPH\|NIPHEM\|PLOT[0-9]/-/g' | sed 's/NA/-/g' | sed -e '$a\\') >> {output}
        """

# ---------

rule fix_results_statistics:
    input:
        stats = "cgmlst/{sample}/results_statistics.tsv"
    output:
        stats_fixed = "cgmlst/{sample}/allele_statistics.tsv"
    message: "Fixing stats file for sample {wildcards.sample}"
    shell:
        """
        head -n 1 {input} | sed 's/^Genome/sample/'> {output}
        paste <(echo {wildcards.sample}) <(tail -n 1 {input} | cut -f 2- | sed -e '$a\\') >> {output}
        """

rule collect_allele_statistics:
    input:
        stats = expand("cgmlst/{sample}/allele_statistics.tsv",sample = samples.index)
    output:
        summary_tsv = "cgmlst/allele_statistics.tsv"
    message: "Collecting allele statistics"
    run:
        with open(output.summary_tsv,'wb') as outfile:
            for i, fname in enumerate(input.stats):
                with open(fname,'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile,outfile)  # Block copy rest of file from input to output without parsing

# rules for frameshift detection -------

# once per dataset
rule median_allele_length:
    input:
        scheme_dir = config["parameters"]["chewbbaca"]["scheme_dir"],
    output:
        scheme_length_profile = "cgmlst/scheme_length_profile.tsv"
    message: "Compute median allele length per locus"
    conda: "../envs/rmarkdown.yaml"
    script:
        "../scripts/compute_median_allelelength.R"

# extract allele length
rule get_sample_allele_length:
    input:
        contig_info = "cgmlst/{sample}/results_contigsInfo.tsv"
    output:
        sample_length_profile = "cgmlst/{sample}/allele_length.tsv"
    message: "Compute allele length per locus for sample {wildcards.sample}"
    script:
        "../scripts/get_allelelength.R"

# mask_frameshift_alleles
rule mask_frameshift_alleles:
    input:
        sample_length_profile = "cgmlst/{sample}/allele_length.tsv",
        scheme_length_profile = "cgmlst/scheme_length_profile.tsv",
        allele_profile = "cgmlst/{sample}/allele_profiles.tmp"
    output:
        allele_profile_masked = "cgmlst/{sample}/allele_profiles.fsremoved.tsv",
        masked_sites = "cgmlst/{sample}/masked_loci.tsv"
    message: "Mask frameshift alleles for sample {wildcards.sample}"
    params:
        mode = config["parameters"]["frameshift_removal"]["mode"],# relative
        threshold = config["parameters"]["frameshift_removal"]["threshold"]  # 0.1
    script:
        "../scripts/mask_alleles.R"

# summary of frameshift
rule summary_masked_loci:
    input:
        masked_sites = expand("cgmlst/{sample}/masked_loci.tsv",sample = samples.index)
    output:
        masked_sites = "cgmlst/summary_masked_loci.tsv"
    message: "Summary of masked loci"
    params:
        mode = config["parameters"]["frameshift_removal"]["mode"],# relative
        threshold = config["parameters"]["frameshift_removal"]["threshold"]  # 0.1
    script:
        "../scripts/summary_masked_loci.R"

# ---------

rule get_allele_hashes:
    input:
        "cgmlst/{sample}/allele_profiles.fsremoved.tsv" if config["parameters"]["frameshift_removal"]["remove_frameshifts"] == True else "cgmlst/{sample}/allele_profiles.tmp"
    output:
        "cgmlst/{sample}/allele_profiles.tsv"
    message: "Hash allele profile for sample {wildcards.sample}"
    log: "logs/get_allele_hashes_{sample}.log"
    params:
        scheme_dir = config["parameters"]["chewbbaca"]["scheme_dir"]
    shell:
        "{workflow.basedir}/scripts/alleleprofile_hasher.py --profile {input} --database {params.scheme_dir} --outfile {output} --force &> {log}"

# ---------

rule collect_allele_profiles:
    input:
        profile = expand("cgmlst/{sample}/allele_profiles.tsv",sample = samples.index)
    output:
        profile = "cgmlst/allele_profiles.tsv"
    message: "Merging allele profiles"
    run:
        with open(output.profile,'wb') as outfile:
            for i, fname in enumerate(input.profile):
                with open(fname,'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile,outfile)  # Block copy rest of file from input to output without parsing

rule collect_hashed_allele_profiles:
    input:
        profile = expand("cgmlst/{sample}/allele_profiles_hashed.tsv",sample = samples.index)
    output:
        profile = "cgmlst/hashids.tsv"
    message: "Merging hashed allele profiles"
    run:
        with open(output.profile,'wb') as outfile:
            for i, fname in enumerate(input.profile):
                with open(fname,'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile,outfile)  # Block copy rest of file from input to output without parsing

# ---------

rule merge_unhashed_allele_profiles:
    input:
        profile = expand("cgmlst/{sample}/allele_profiles.tmp",sample = samples.index)
    output:
        profile = "cgmlst/allele_profiles_unhashed.tsv"
    message: "Merging unhashed allele profiles"
    run:
        with open(output.profile,'wb') as outfile:
            for i, fname in enumerate(input.profile):
                with open(fname,'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile,outfile)  # Block copy rest of file from input to output without parsing

rule get_creation_date_sample:
    input:
        "cgmlst/{sample}/results_alleles.tsv"
    output:
        "cgmlst/{sample}/timestamps.tsv"
    message: "Get profile file creation date for sample {wildcards.sample}"
    shell:
        """
        echo -e "sample\tdate" > {output}
        date=`stat -c %y {input} | cut -f 1 -d ' '`
        echo -e "{wildcards.sample}\t$date" >> {output}
        """

rule collect_creation_date:
    input:
        sample_timestamps = expand("cgmlst/{sample}/timestamps.tsv",sample = samples.index)
    output:
        timestamps = "cgmlst/timestamps.tsv"
    message: "Collecting timestamp information"
    run:
        with open(output.timestamps,'wb') as outfile:
            for i, fname in enumerate(input.sample_timestamps):
                with open(fname,'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile,outfile)  # Block copy rest of file from input to output without parsing

# ---------

rule check_error_alleles:
    input:
        "cgmlst/allele_profiles_unhashed.tsv"
    output:
        "cgmlst/error_counts.tsv"
    message: "Checking for error alleles"
    shell:
        """
        (grep 'ERROR' {input} | cut -f 1 || echo) | sed '/^$/d' > {output}
        if [[ -s {output} ]]; then 
            echo "Error: at least one profile contains an error allele, see "; 
            exit 1
       else
            echo "No errors in file "
        fi
        """
