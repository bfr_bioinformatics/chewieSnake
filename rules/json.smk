# rule for json creation

rule export_json:
    input:
        allele_profile = "cgmlst/{sample}/allele_profiles.tsv",
        allele_stats = "cgmlst/{sample}/allele_statistics.tsv",
        timestamps = "cgmlst/{sample}/timestamps.tsv",
        hashid = "cgmlst/{sample}/allele_profiles_hashed.tsv"
    output:
        json = "cgmlst/json/{sample}.chewiesnake.json"
    params:
        configfile = "config_chewiesnake.yaml"
    message: "json export for sample {wildcards.sample}"
    log: "logs/export_json_{sample}.log"
    shell:
        """
        echo "{workflow.basedir}/scripts/json_writer.py --sample_name {wildcards.sample} --config {params.configfile} --hashid {input.hashid} --allele_stats {input.allele_stats} --allele_profile {input.allele_profile} --timestamps {input.timestamps} --logfile {log} --outfile {output.json}"
              {workflow.basedir}/scripts/json_writer.py --sample_name {wildcards.sample} --config {params.configfile} --hashid {input.hashid} --allele_stats {input.allele_stats} --allele_profile {input.allele_profile} --timestamps {input.timestamps} --logfile {log} --outfile {output.json}
        """
