# rules for hashid creation

rule hashids_from_allele_profiles:
    input:
        profile = "cgmlst/{sample}/allele_profiles.tsv"
    output:
        hashid = "cgmlst/{sample}/allele_profiles_hashed.tsv"
    message: "Computing hashed sequence types"
    shell:
        """
        {workflow.basedir}/scripts/hashID.py --profile {input.profile} --outfile {output.hashid}
        """

rule hashids_from_joined_allele_profiles:
    input:
        allele_profile_joined_filtered = "cgmlst_joined/allele_profiles_filtered.tsv"
    output:
        hashid = "cgmlst_joined/hashids.tsv"
    message: "Computing hashed sequence types for joined allele profiles"
    shell:
        "{workflow.basedir}/scripts/hashID.py --profile {input.allele_profile_joined_filtered} --outfile {output.hashid}"
