rule run_grapetree_distance:
    input:
        "cgmlst/allele_profiles.tsv"
    output:
        distance_matrix_tsv = "cgmlst/distance_matrix.tsv",
    conda: "../envs/grapetree3.yaml"
    message: "Running rule grapetree_distance"
    params:
        distance_option = config["parameters"]["clustering"]["grapetree_distance_method"],#
    shell:
        """
        grapetree -p {input} -m distance --missing {params.distance_option} | sed -E 's/[[:space:]]{{2,}}/ /' | tail -n+2 > {output.distance_matrix_tsv}
        """

rule run_grapetree_tree:
    input:
        "cgmlst/allele_profiles.tsv"
    output:
        mstree = "cgmlst/grapetree_mstree.tre",
    conda: "../envs/grapetree3.yaml"
    message: "Running rule grapetree_tree"
    shell:
        """
        grapetree -p {input} -m MSTreeV2 > {output.mstree}
        """

# -----------

rule run_grapetree_distance_unhashed:
    input:
        "cgmlst/allele_profiles_unhashed.tsv"
    output:
        distance_matrix_tsv = "cgmlst/distance_matrix_unhashed.tsv"
    conda: "../envs/grapetree3.yaml"
    message: "Running rule grapetree_distance"
    params:
        distance_option = config["parameters"]["clustering"]["grapetree_distance_method"],#
    shell:
        """
        grapetree -p {input} -m distance --missing {params.distance_option} | sed -E 's/[[:space:]]{{2,}}/ /' | tail -n+2 > {output.distance_matrix_tsv}
        """

rule run_grapetree_tree_unhashed:
    input:
        "cgmlst/allele_profiles_unhashed.tsv"
    output:
        mstree = "cgmlst/grapetree_mstree_unhashed.tre",
    conda: "../envs/grapetree3.yaml"
    message: "Running rule grapetree_tree"
    shell:
        """
        grapetree -p {input} -m MSTreeV2 > {output.mstree}
        """

rule run_grapetree_nj:
    input:
        "cgmlst/allele_profiles.tsv"
    output:
        njtree = "cgmlst/grapetree_nj.tre"
    conda: "../envs/grapetree3.yaml"
    message: "Running rule grapetree"
    log: "logs/run_grapetree_nj.log"
    shell:
        """
        grapetree -p {input} -m NJ > {output.njtree}
        """
