# merge with other files ---------

rule join_allele_profiles:
    input:
        allele_profile_query = "cgmlst/allele_profiles.tsv",
        allele_profiles_comparison = config["parameters"]["clustering"]["comparison_allele_database"]
    output:
        allele_profile_joined = "cgmlst_joined/allele_profiles.tsv"
    message: "Merging allele profile local with profile database"
    shell:
        """
        cat {input.allele_profile_query} > {output} # keep header for first file with #
        grep -v 'FILE' {input.allele_profiles_comparison} >> {output}
        """

# old without header
#grep -v 'FILE' {input.allele_profiles_comparison} > {output}
#grep -v 'FILE' {input.allele_profile_query} >> {output}

# rule merged_grapetree: distance matrix
rule grapetree_joined:
    input:
        allele_profiles_joined = "cgmlst_joined/allele_profiles.tsv"
    output:
        distance_matrix = "cgmlst_joined/distance_matrix.tsv",
        mstree = "cgmlst_joined/grapetree_mstree.tre"
    message: "Grapetree on joined allele profiles"
    conda: "../envs/grapetree3.yaml"
    params:
        distance_option = config["parameters"]["clustering"]["grapetree_distance_method"],#
    shell:
        """
            grapetree -p {input.allele_profiles_joined} -m distance --missing {params.distance_option} | sed -E 's/[[:space:]]{{2,}}/ /' | tail -n +2 > {output.distance_matrix}
            grapetree -p {input.allele_profiles_joined} -m MSTreeV2 > {output.mstree}
        """

rule find_clusters:
    input:
        distance_matrix = "cgmlst_joined/distance_matrix.tsv",
    output:
        samples_related = "cgmlst_joined/samples_related.txt",
        match_overview = "cgmlst_joined/match_summary.txt",
        distance_matrix_filtered = "cgmlst_joined/distance_matrix_filtered.tsv",
        sample_meta = "cgmlst_joined/samples_related_meta.tsv",
    message: "Find clusters between current and database samples"
    log: "logs/find_clusters.log"
    params:
        threshold = config["parameters"]["clustering"]["joining_threshold"],# threshold for joining with pre-computed allele database
        name = "query_data",# newly created dataset is termed query_data (in comparison to comparison_db)
        outdir = "cgmlst_joined/",
        samplesheet = config["samples"],
        clustering_method = config["parameters"]["clustering"]["clustering_method"]
    conda: "../envs/rmarkdown.yaml"
    script:
        "../scripts/DistanceMatrix_reduce.R"

# rule filter allele profiles
rule filter_allele_profiles:
    input:
        samples_related = "cgmlst_joined/samples_related.txt",
        allele_profile_joined = "cgmlst_joined/allele_profiles.tsv"
    output:
        allele_profile_joined_filtered = "cgmlst_joined/allele_profiles_filtered.tsv"
    message: "Filtering allele profiles"
    shell:
        """
            head -n 1 {input.allele_profile_joined} > {output} # keep header
            sed 's/^/^/' {input.samples_related} | grep --color=never -f - {input.allele_profile_joined} >> {output}
        """

# ------------------------------------
# rule grapetree_selected
rule grapetree_selected:
    input:
        allele_profile_joined_filtered = "cgmlst_joined/allele_profiles_filtered.tsv"
    output:
        njtree = "cgmlst_joined/grapetree_nj_filtered.tre"
    message: "Running grapetree on selected samples to create tree"
    conda: "../envs/grapetree.yaml"
    shell:
        """
            count=`wc -l {input.allele_profile_joined_filtered} | cut -f 1 -d ' '`
            if [[ $count -gt 2 ]]; then 
                grapetree -p {input.allele_profile_joined_filtered} -m MSTreeV2 > {output.njtree}
            else
                echo "Only single sample, creating dummy mstree"
                touch {output.njtree}
            fi
        """

# rule merged_grapetree: minimum spanning tree
rule grapetree_mstree_filtered:
    input:
        allele_profile_joined_filtered = "cgmlst_joined/allele_profiles_filtered.tsv"
    output:
        mstree = "cgmlst_joined/grapetree_mstree_filtered.tre"
    message: "Grapetree on joined and filtered allele profiles"
    conda: "../envs/grapetree3.yaml"
    shell:
        """
            count=`wc -l {input.allele_profile_joined_filtered} | cut -f 1 -d ' '`
            if [[ $count -gt 2 ]]; then 
                grapetree -p {input.allele_profile_joined_filtered} -m MSTreeV2 > {output.mstree}
            else
                echo "Only single sample, creating dummy mstree"
                touch {output.mstree}
            fi
        """
