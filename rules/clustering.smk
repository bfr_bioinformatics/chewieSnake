# find clusters between sets of data

rule clustering:
    input: "cgmlst/distance_matrix.tsv"
    output: directory("cgmlst/clustering")
    message: "Computing cluster types and addresses"
    log: "logs/custering.log"
    params:
        cutoffs = config["parameters"]["clustering"]["address_range"],
        clustering_method = config["parameters"]["clustering"]["clustering_method"],
    shell: "Rscript {workflow.basedir}/scripts/Clustering_DistanceMatrix.R -i {input} -o {output} --cutoff_values {params.cutoffs} --method {params.method} --force | tee {log} 2>&1"
