# ChewieSnake: A pipeline for cgMLST allele calling and allele hashing

[![DOI](https://img.shields.io/static/v1?label=DOI&message=10.3389/fmicb.2021.649517&color=blue)](https://doi.org/10.3389/fmicb.2021.649517)
[![Bioconda](https://img.shields.io/conda/vn/bioconda/chewiesnake?label=BioConda&color=green)](https://anaconda.org/bioconda/chewiesnake)
[![BioConda Install](https://img.shields.io/conda/dn/bioconda/chewiesnake?label=BioConda%20Install&color=green)](https://anaconda.org/bioconda/chewiesnake)
[![DockerHub](https://img.shields.io/docker/v/bfrbioinformatics/chewiesnake/latest?label=Docker)](https://hub.docker.com/r/bfrbioinformatics/chewiesnake)
[![DockerHub Pulls](https://img.shields.io/docker/pulls/bfrbioinformatics/chewiesnake?label=Docker%20Pulls)](https://hub.docker.com/r/bfrbioinformatics/chewiesnake)
[![DockerHub Image Size](https://img.shields.io/docker/image-size/bfrbioinformatics/chewiesnake/latest?label=Docker%20Image%20Size)](https://hub.docker.com/r/bfrbioinformatics/chewiesnake)

[TOC]

## 📖 Description

ChewieSnake is an end-to-end cgMLST based workflow for analyzing a set of assemblies (or reads) and obtaining an easy-to-interpret cgMLST report.

* ChewieSnake is  a snakemake workflow that performs cgMLST allele calling for a set of assembled genomes using [chewBBACA](https://www.ncbi.nlm.nih.gov/pubmed/29543149). 
* The workflow converts the resulting allele profiles into a **nomenclature-free** allele profile using allele hashes (following the idea of [sistr](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101)).
* The allele profiles of all input genomes are converted into an allele distance matrix, a minimum spanning tree and a neighbor-joining tree using [grapetree](https://genome.cshlp.org/content/28/9/1395).
* The allele numbers in the **nomenclature-free** allele profiles are derived from the hashed allele sequences.
  Therefore they can be directly compared between two or more laboratories without the need of an explicit exchange of the genome data, allele nomenclature or allele sequences.
* If no assembly data is available, chewieSnake can assemble reads into draft genomes using _fastp_ and _shovill_. See the `--reads` option.
* Allele distances are hierarchically clustered and for each sample a _cluster address_ as well as a unique (hashed) sequenced ID is provided.
* The chewieSnake workflow returns highly-informative, interactive HTML reports.
* The hashed allele profiles allow an inter-lab comparison with the `chewieSnake_join` workflow.

### Latest News

* [2023-05-24] We switched the Docker deployment strategy to docker-compose.

## Citation

> Deneke C, Uelze L, Brendebach H, Tausch SH and Malorny B (2021)
> Decentralized Investigation of Bacterial Outbreaks Based on Hashed cgMLST.
> Front. Microbiol. 12:649517. [doi:10.3389/fmicb.2021.649517](https://doi.org/10.3389/fmicb.2021.649517)

## Table of contents

* [DAG](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#dags-directed-acyclic-graphs-of-workflows)
* [Installation](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#installation)
* [Set-up](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#set-up)
* [Usage (assembly-based version)](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#usage-assembly-based-version)
* [Usage (read-based flavour)](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#usage-read-based-flavour)
* [Usage of chewieSnake_join workflow](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#running-the-chewiesnake_join-workflow)
* [Output description](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#output-description)
* [JSON structure](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#json-structure)
* [Test execution on Salmonella test data](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#test-execution-on-salmonella-test-data)
* [Test execution of chewieSnake join](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#test-of-chewiesnake_join)
* [Validating results](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#validating-of-results)
* [Standalone scripts](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#standalone-scripts)

## DAGs (directed acyclic graphs) of workflows

### DAG of chewieSnake

![](docs/dag_chewiesnake.png)

### DAG of chewieSnake_join

![](docs/dag_chewiesnakejoin.png)

## Installation

You can install chewieSnake by installing the Bioconda package, by installing the Docker container or by cloning this repository and installing all dependencies with conda.

chewieSnake relies on the conda package manager for all dependencies.
Please set up conda on your system as explained [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html).

chewieSnake requires the package manager [mamba](https://anaconda.org/conda-forge/mamba) to resolve all software dependencies.
Install it via `conda install mamba` to your base environment first.

The installation of reference schema from the BfR file server or other suitable sources is mandatory (see chapter [_From Source_](#from-source)). 

#### Placeholders for Paths in this Manual

| Placeholder              | Path                                                                                                                                              |
|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| `<path_to_conda>`        | is the conda installation folder, type `conda info --base` to retrieve its absolute path, typically `~/anaconda3` or `~/miniconda3`               |
| `<path_to_envs>`         | is the folder that holds your conda environments, typically `<path_to_conda>/envs`                                                                |
| `<path_to_installation>` | is the parent folder of the chewieSnake repository                                                                                                |
| `<path_to_chewiesnake>`  | is the base folder of the chewieSnake repository, i.e. `<path_to_installation>/chewieSnake`                                                       |
| `<path_to_schema>`       | is the parent folder of your databases, by default, chewieSnake uses `<path_to_chewiesnake>/schema`, but you are free to choose a custom location |
| `<path_to_data>`         | is the working directory for an chewieSnake analysis typically containing a subfolder `<path_to_data>/fasta` with your fasta assemblies           |

### 🐍 From Bioconda

```
mamba create -n chewiesnake -c r -c bioconda -c conda-forge chewiesnake
```

After installation, the wrappers `chewiesnake` and `chewiesnake_join` as well as some [standalone scripts](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#Standalone-scripts) will become available.

### 💻 From Source

To install the latest stable version of chewieSnake, please clone the git repository on your system.

```
cd <path_to_installation>
git clone https://gitlab.com/bfr_bioinformatics/chewieSnake
```

#### Manual Conda Environment Setup

Please initialize a conda base environment containing `snakemake` and `mamba` (mamba is faster in resolving dependencies), then:

```
mamba env create -f <path_to_chewiesnake>/envs/chewiesnake.yaml
```

This creates an environment named `chewiesnake` containing all [dependencies](/envs/chewiesnake.yaml).

### Install the Docker container

*Prerequisite:*
Install the Docker engine for your favourite operating system (e.g. [Ubuntu Linux](https://docs.docker.com/engine/install/ubuntu/)). Check for co-installation of the plugins `docker-compose` and `docker-buildx` (the default on main installation routes).

Download the _latest_ version of chewieSnake from [Docker Hub](https://hub.docker.com/r/bfrbioinformatics/chewiesnake/tags) and note down the Docker Image ID on your system  (hereafter referred as $docker_image_id) with the shell commands:

```
docker pull bfrbioinformatics/chewiesnake:latest
docker images bfrbioinformatics/chewiesnake:latest -q
```

or with the `docker-compose` strategy, execute the remote installation script which will create an `./chewieSnake` subfolder in your current working directory:

```shell
cd <path_to_installation>
bash <(curl -s https://gitlab.com/bfr_bioinformatics/chewieSnake/-/raw/master/docker/get_chewiesnake_docker.sh)
```

and start-up the containers:

```shell
export LOCAL_USER_ID=$(id -u $USER)
export CHEWIESNAKE_HOST_PATH=<path_to_data>
docker compose --file <path_to_chewiesnake>/docker/docker-compose.yaml up --detach
```

To process data and write results, Docker needs a *volume mapping* from a host directory containing your sequence data (`<path_to_data>`) to the Docker container (`/chewieSnake/analysis`).
Your sample list (`samples.tsv`) needs to be located within `<path_to_data>` and contain relative paths (or fixed paths from the container perspective) to your NGS assemblies in the same or another child directory. 
You may generate a Docker-compatible sample list in your host directory (`<path_to_data>/samples.tsv`) by executing the `create_sampleSheet.sh` from the container with the following terminal commands:

```
host:<path_to_data>$ ls fasta/
sample_1.fasta   sample_2.fasta   sample_3.fasta
```

```shell
docker exec --user $LOCAL_USER_ID chewiesnake_app  \
  /chewieSnake/scripts/create_sampleSheet.sh  \
  --mode assembly  \
  --fastxDir /chewieSnake/analysis/fasta  \
  --outDir /chewieSnake/analysis
```

With the following command, chewieSnake is started within the Docker container and will process any option appended:

```shell
docker exec --user $LOCAL_USER_ID chewiesnake_app  \
  micromamba run -n chewiesnake  \
  /chewieSnake/chewieSnake.py  \
  --docker <path_to_data>  \
  --working_directory /chewieSnake/analysis  \
  --sample_list /chewieSnake/analysis/samples.tsv  \
  --<any_other_chewieSnake_options>
```

**Notes on Docker images:**
The Docker image _latest_ represents the latest build from the Gitlab repository.
It requires additional reference databases (also provided via the [download script](/scripts/chewieSnake_download_EFSA_schemas.sh)) as well as a set of test data files (fasta) for validation purposes via Docker volume binds.
These volume binds are provided by the docker-compose setup.

**Notes on Docker usage:**
The container path `/chewieSnake/analysis` is fixed and may not be altered.
Any subdirectories of `<path_to_data>` will be available as subdirectories under `/chewieSnake/analysis/`.
Our container is able to write results with the Linux user and group ID of your choice (`UID` and `GID`, respectively) to blend into your host file permission setup.
With the environment variable `LOCAL_USER_ID=$(id -u $USER)` the UID of the currently executing user is inherited, change it according to your needs.

For reference, please consult the `/chewieSnake/scripts/chewieSnake_wrapper.sh` module `run_docker_compose()`.
This module may need to be uncommented/activated for execution in the function `run_modules()`.
Advanced users may copy the wrapper script to their host directory (prerequisite: `docker compose up`, see described above) for a more convenient execution.
Adapt `chewieSnake_wrapper.sh` and the run sheet `chewieSnake_runs.tsv` according to your needs:

```shell
docker cp chewiesnake_app:/chewieSnake/scripts/chewieSnake_wrapper.sh ./
docker cp chewiesnake_app:/chewieSnake/scripts/helper_functions.sh    ./
docker cp chewiesnake_app:/chewieSnake/scripts/chewieSnake_runs.tsv   ./
```

## Set-up

For setting up a chewieSnake analysis you need a cgMLST scheme and a sample sheet with all samples to be analyzed.

### Install cgMLST schema

Different options exist for installing a cgMLST schema

#### Get a schema from Enterobase, bigsDB, cgMLST.org, Innuendo and prepare with chewBBACA

See [below](https://gitlab.com/bfr_bioinformatics/chewieSnake/-/blob/master/README.md#cgmlst-databases) for available resources and the [chewBBACA wiki](https://github.com/B-UMMI/chewBBACA/wiki/1.-Schema-Creation) for instructions how to prepare a scheme.

#### Create your own schema from scratch

see [chewBBACA wiki](https://github.com/B-UMMI/chewBBACA/wiki/3.-cgMLST-definition) for details

#### Retrieve already prepared schemas

Selected publically available cgMLST schemas for
[Salmonella enterica](https://gitlab.bfr.berlin/bfr_bioinformatics/chewiesnake_resources/-/raw/main/cgmlst_schemes/Salmonella_enterobase_senterica_cgmlst.zip),
[Escherichia coli](https://gitlab.bfr.berlin/bfr_bioinformatics/chewiesnake_resources/-/raw/main/cgmlst_schemes/Enterobase_Ecoli_cgmlst.zip) and
[Campylobacter jejuni](https://gitlab.bfr.berlin/bfr_bioinformatics/chewiesnake_resources/-/raw/main/cgmlst_schemes/Campylobacter_cgmlst_chewbbacaonline.zip)
have been prepared for usage with chewBBACA / chewieSnake and can be found at their respective link.
A prepared scheme for _Listeria monocytogenes_ can be supplied on request.

Please download and unzip the schema of your choice.
Specify the full path to the schema's directory when executing chewieSnake.

#### Retrieve schemas from chewieNS / Innuendo

These schemes are already prepared for use with chewBBACA.

See [chewbbaca.online](http://chewbbaca.online/) for available schemas.

We provide a small wrapper script with essential functions for downloading and subsetting schemas according to [EFSA One Health WGS System](https://efsa.onlinelibrary.wiley.com/doi/10.2903/sp.efsa.2022.EN-7413) guidelines.
Please execute and review the instructions of [chewieSnake_download_EFSA_schemas.sh](/scripts/chewieSnake_download_EFSA_schemas.sh).

### Create sample sheet

#### For assembly fasta files

All data must be supplied in a sample sheet (samples.tsv). It requires the following entries:

```
sample    assembly
sample_1    <path_to_data>/assembly_1.fasta
sample_2    <path_to_data>/assembly_2.fasta
```

The sample sheet can be updated and analysis will be run only on new samples.
The sample sheets can be created for all fasta files in a directory with the script create_sampleSheet.sh

```
cd <path_to_chewiesnake>/test_data/chewiesnake/data
<path_to_chewiesnake>/scripts/create_sampleSheet.sh --mode assembly
```

#### For raw fastq files (for read-based flavor)

All data must be supplied in a sample sheet (samples.tsv). It requires the following entries:

```
sample    fq1   fq2
sample_1    <path_to_data>/sample1_R1.fastq.gz  <path_to_data>/sample1_R2.fastq.gz
sample_2    <path_to_data>/sample2_R1.fastq.gz  <path_to_data>/sample2_R2.fastq.gz
```

Again, the sample sheet can be created with the helper script:

```
cd $testDIR/assemblies
<path_to_chewiesnake>/scripts/create_sampleSheet.sh --mode flex ## using a flexible scheme where the part before the first '_' denotes the sample name sample_*_R[12]_*.fastq.gz
<path_to_chewiesnake>/scripts/create_sampleSheet.sh --mode illumina ## using the illulima naming scheme sample_S[0-9]_L001_R[12]_001.fastq.gz
<path_to_chewiesnake>/scripts/create_sampleSheet.sh --mode ncbi ## using ncbi style names: sample_[12].fastq.gz
```

The Snakemake workflow is set-up such that you can update the sample sheets at any time and allele calling is only performed on new samples. The report always contains the analysis of all samples.

### Prodigal training file (provided by chewBBACA)

* chewBBACA provides for a number of species a prodigal training file: It is located at `<path_to_chewiesnake>/chewBBACA/CHEWBBACA/prodigal_training_files`
* see also [here](https://gitlab.com/bfr_bioinformatics/chewieSnake/tree/master/chewBBACA/CHEWBBACA/prodigal_training_files) for the available training files
* see training files are also provided [here](https://gitlab.bfr.berlin/bfr_bioinformatics/chewiesnake_resources/-/tree/main/prodigal_files)

## Usage (assembly-based version)

The execution needs the path to the config file and to the Snakefile of this repository (`$mydir/bakcharak/Snakefile`). Furthermore you can specify the number of threads.

### Activate conda (installation see above)

Unless you installed chewieSnake via docker or BioConda, you need to activate your conda environment before running chewieSnake:

```
conda activate chewiesnake
```

### Using the wrapper

You can use the python wrapper `chewieSnake.py` for creating the config file and subsequent execution of chewieSnake.

```
chewieSnake.py  -t THREADS -l SAMPLE_LIST -d WORKING_DIRECTORY --scheme SCHEME --prodigal PRODIGAL --report [--dryrun]
```

* Execution will create a config file config.yaml in your WORKING_DIRECTORY
* Use flag `--dryrun` to test command and write config file without executing any actual calculations
* THREADS: Number of CPUs to use
* SAMPLE_LIST: sample sheet as described above
* WORKING_DIRECTORY: where the result files should be stored
* SCHEME: Location of cgMLST/wgMLST scheme as described above, i.e. <path_to_schema>
* PRODIGAL: Location of prodigal training file as described [above](https://gitlab.com/bfr_bioinformatics/chewieSnake/blob/master/README.md#prodigal-training-file-provided-by-chewbbaca)

Note: when you install via BioConda or Docker, please use `chewiesnake` instead of `chewieSnake.py` to run the wrapper.

#### Details of wrapper

<details><summary>help file</summary>
<p>

```
$ python chewieSnake.py --help
usage: chewieSnake.py [-h] [--sample_list SAMPLE_LIST]
                      [--working_directory WORKING_DIRECTORY]
                      [--scheme SCHEME] [--prodigal PRODIGAL] [--reads]
                      [--bsr_threshold BSR_THRESHOLD]
                      [--size_threshold SIZE_THRESHOLD]
                      [--max_fraction_missing_loci MAX_FRACTION_MISSING_LOCI]
                      [--distance_method DISTANCE_METHOD]
                      [--clustering_method CLUSTERING_METHOD]
                      [--distance_threshold DISTANCE_THRESHOLD]
                      [--address_range ADDRESS_RANGE] [--comparison]
                      [--comparison_db COMPARISON_DB]
                      [--joining_threshold JOINING_THRESHOLD]
                      [--remove_frameshifts]
                      [--allele_length_threshold ALLELE_LENGTH_THRESHOLD]
                      [--frameshift_mode FRAMESHIFT_MODE]
                      [--min_trimmed_length MIN_TRIMMED_LENGTH]
                      [--assembler ASSEMBLER]
                      [--shovill_output_options SHOVILL_OUTPUT_OPTIONS]
                      [--shovill_extraopts SHOVILL_EXTRAOPTS]
                      [--shovill_modules SHOVILL_MODULES]
                      [--shovill_depth SHOVILL_DEPTH]
                      [--shovill_tmpdir SHOVILL_TMPDIR] [--noreport] [--json]
                      [--rule_directives RULE_DIRECTIVES] [--use_conda]
                      [--conda_frontend] [--condaprefix CONDAPREFIX]
                      [--threads THREADS] [--threads_sample THREADS_SAMPLE]
                      [--force FORCE] [--forceall FORCEALL]
                      [--profile PROFILE] [--dryrun] [--unlock]
                      [--logdir LOGDIR] [--version]

optional arguments:
  -h, --help            show this help message and exit
  --sample_list SAMPLE_LIST, -l SAMPLE_LIST
                        [REQUIRED] List of samples to analyze, as a two column
                        tsv file with columns sample and assembly paths. Can
                        be generated with provided script
                        create_sampleSheet.sh
  --working_directory WORKING_DIRECTORY, -d WORKING_DIRECTORY
                        [REQUIRED] Working directory where results are saved
  --scheme SCHEME       [REQUIRED] Path to directory of the cgmlst scheme
  --prodigal PRODIGAL   [REQUIRED] Path to prodigal_training_file (provided in
                        chewbbaca, e.g. <path_to_chewiesnake>/CH
                        EWBBACA/prodigal_training_files/Salmonella_enterica.tr
                        n
  --reads               Input data is reads. Assemble (using shovill) prior to
                        allele calling (default is contigs)
  --bsr_threshold BSR_THRESHOLD
                        blast scoring ratio threshold to use; default: 0.6
  --size_threshold SIZE_THRESHOLD
                        size threshold, default at 0.2 means alleles with size
                        variation of +-20 percent will be tagged as ASM/ALM ,
                        default: 0.2
  --max_fraction_missing_loci MAX_FRACTION_MISSING_LOCI
                        maximum fraction of missing loci. If a sample has
                        fewer loci than it is marked as FAIL, default: 0.05
  --distance_method DISTANCE_METHOD
                        Grapetree distance method; default: 3
  --clustering_method CLUSTERING_METHOD
                        The agglomeration method to be used for hierarchical
                        clustering. This should be (an unambiguous
                        abbreviation of) one of "ward.D", "ward.D2", "single",
                        "complete", "average" (= UPGMA), "mcquitty" (= WPGMA),
                        "median" (= WPGMC) or "centroid" (= UPGMC); default:
                        single
  --distance_threshold DISTANCE_THRESHOLD
                        A single distance threshold for the extraction of sub-
                        trees; default: 10
  --address_range ADDRESS_RANGE
                        A comma separated set of cutoff values for defining
                        the clustering address; default:
                        "1,5,10,20,50,100,200,1000"
  --comparison          Compare these results to pre-computed allele database
  --comparison_db COMPARISON_DB
                        Path to pre-computed allele database for comparison
  --joining_threshold JOINING_THRESHOLD
                        A distance threshold for joining data with
                        comparsion_db; default: 30
  --remove_frameshifts, -f
                        remove frameshift alleles by deviating allele length
  --allele_length_threshold ALLELE_LENGTH_THRESHOLD
                        Maximum tolerated allele length deviance compared to
                        median allele length of locus; choose integer for
                        "absolute frameshift mode and float (0..1) for
                        "relative" frameshift mode ; default=0.1
  --frameshift_mode FRAMESHIFT_MODE
                        Whether to consider absolute or relative differences
                        of allele length for frameshifts removal. Choose from
                        "absolute" and "relative", default="relative"
  --min_trimmed_length MIN_TRIMMED_LENGTH
                        Minimum length of a read to keep, default: 15
  --assembler ASSEMBLER
                        Assembler to use in shovill, choose from megahit
                        velvet skesa spades; default: spades
  --shovill_output_options SHOVILL_OUTPUT_OPTIONS
                        Extra output options for shovill; default: ""
  --shovill_extraopts SHOVILL_EXTRAOPTS
                        Extra options for shovill; default: ""
  --shovill_modules SHOVILL_MODULES
                        Module options for shovill, choose from --noreadcorr
                        --trim --nostitch --nocorr --noreadcorr; default: "--
                        noreadcorr"
  --shovill_depth SHOVILL_DEPTH
                        Sub-sample --R1/--R2 to this depth. Disable with
                        --depth 0; default: 100
  --shovill_tmpdir SHOVILL_TMPDIR
                        Fast temporary directory (default: "")
  --noreport            Do not create html report
  --json                Calculate only allele hashes and generate JSON output
  --rule_directives RULE_DIRECTIVES
                        Process DAG with rule grouping and/or rule
                        prioritization via Snakemake rule directives YAML;
                        default: <path_to_chewiesnake>/profiles/
                        smk_directives.yaml equals no directives
  --use_conda           Utilize "--useconda" option, i.e. all software
                        dependencies are available in a single env
  --conda_frontend      Do not use mamba but conda as frontend to create
                        individual module' software environments
  --condaprefix CONDAPREFIX, -c CONDAPREFIX
                        Path of default conda environment, enables recycling
                        of built environments; default:
                        <path_to_chewiesnake>/conda_env
  --threads THREADS, -t THREADS
                        Number of Threads/Cores to use. This overrides the
                        "<chewieSnake >/profiles" settings." "Note that samples
                        can only be processed sequentially due to the required
                        database access. However the allele calling can be
                        executed in parallel for the different loci
  --threads_sample THREADS_SAMPLE, -p THREADS_SAMPLE
                        Number of Threads to use per sample in multi-threaded
                        rules; default: 4
  --force FORCE         Snakemake force. Force recalculation of output (rule
                        or file) specified here
  --forceall FORCEALL   Snakemake force. Force recalculation of all steps
  --profile PROFILE     (A) Full path or (B) directory name under
                        "<chewieSnake>/profiles" of a Snakemake config.yaml
                        with Snakemake parameters, e.g. available CPUs and
                        RAM. Default: "bfr.hpc"
  --dryrun, -n          Snakemake dryrun. Only calculate graph without
                        executing anything
  --unlock              Unlock a Snakemake execution folder if it had been
                        interrupted
  --logdir LOGDIR       Path to directory where .snakemake output is to be
                        saved
  --version, -V         Print program version
```

</p>
</details>

#### Explicit call of the snakemake workflow (skipping the wrapper)

```
conda activate chewiesnake
snakemake -p -s <path_to_chewiesnake>/chewieSnake_fromcontigs.smk --cores 20 -f all --keep-going --configfile /path/to/config.yaml
```

See the [Snakemake documentation](https://snakemake.readthedocs.io) for further details.

### Usage (read-based flavour)

To call the read-based flavour is very similar. Simply put `--reads` to your command. Make sure that the provided sample sheet contains the raw reads. More options are available for trimming and assembly (see below).

```
chewieSnake.py -t THREADS --reads -l SAMPLE_LIST -d WORKING_DIRECTORY -c CONDAPREFIX --scheme SCHEME --prodigal PRODIGAL --report [--comparison_db] [--dryrun] 
```

* Execution will create a config file config.yaml in your WORKING_DIRECTORY
* Use flag `--dryrun` to test command and write config file
* THREADS: Number of CPUs to use
* SAMPLE_LIST: sample sheet as described above
* WORKING_DIRECTORY: where the result files should be stored
* SCHEME: Location of cgMLST/wgMLST scheme as described above
* PRODIGAL: Location of prodigal trainining file as described above

### Comparing dataset to a pre-computed allele database

Using the `--comparison` allows the comparison of the current (query) data with previously computed data. Modify the above command by adding the parameters

* --comparison _(Compare query results to pre-computed allele database)_
* --comparison_db COMPARISON_DB _(Path to pre-computed allele profile file for comparison)_
* --joining_threshold INT _(At which allele distance should query and comparison db sample's be joined (relevant for joined clustering))_

```
chewieSnake.py -t THREADS --comparison -l SAMPLE_LIST -d WORKING_DIRECTORY -c CONDAPREFIX --scheme SCHEME --prodigal PRODIGAL --report --comparison_db COMPARISON_DB --joining_threshold JOINING_THRESHOLD [--dryrun] 
```

### chewieSnake_join workflow

_chewieSnake_join_ is an independent workflow for the compilation and joined analysis of results previously obtained from chewieSnake at different laboratories. Once chewieSnake results are collected, it allows a centralized clustering and provides an easy-to-communicate clustering report. See [join_report](https://bfr_bioinformatics.gitlab.io/chewieSnake/report_chewiesnake_join.html) for an example.

Among the features of _chewieSnake_join_, it clusters all samples, it assigns easy-to-communicate cluster names between laboratories, and provides a highly informative report including detailed reports on each cluster.

#### Setting up

You need to exchange the files `allele_profiles.tsv`, `allele_statistics.tsv` and `timestamps.tsv` from all participating laboratories.
The paths to all files need to be stored in an _allele sheet_, a tsv-file with following header:

```
sender  profiles        statistics      timestamps
```

If all files are organized in subfolders to a common directory `databasedir` you may create the _allele sheet_ using the script:

```
create_alleledbSheet.sh path/to/databasedir
```

#### Running the chewieSnake_join workflow

You can run the chewieSnake_join workflow using the following commands

```
chewieSnake_join.py --sample_list SAMPLE_LIST --working_directory WORKING_DIRECTORY --distance_threshold DISTANCE_THRESHOLD [--external_cluster_names EXTERNAL_CLUSTER_NAMES] [ --serovar_info SEROVAR_INFO] [--dryrun]
```

* Important options

```
--sample_list/-l SAMPLE_LIST
                       List of allele profiles to analyze, containg a tab
                       separated file with the columns: sender profiles
                       statistics timestamps
--working_directory/-d WORKING_DIRECTORY
                       Working directory where results are saved
--distance_threshold DISTANCE_THRESHOLD
                       A single distance threshold for the extraction of
                       sub-trees; default = 10
--external_cluster_names EXTERNAL_CLUSTER_NAMES
                       A tab seperated file with external cluster names, e.g.
                       Epi clusters. Must contain one representative sample
                       per cluster: sample<tab>cluster_name
--serovar_info SEROVAR_INFO
                       A tab seperated file with serovar or clade names:
                       sample<tab>cluster_name
--dryrun/-n
                       Perform a dry-run to test your settings
```        

Note: use `chewiesnake_join` when installing via bioconda or docker.

#### Details of chewieSnake_join wrapper

<details><summary>help file</summary>
<p>

```
$ python chewieSnake_join.py --help           
usage: chewieSnake_join.py [-h] [--sample_list SAMPLE_LIST]
                           [--working_directory WORKING_DIRECTORY]
                           [--snakefile SNAKEFILE]
                           [--clustering_method CLUSTERING_METHOD]
                           [--distance_threshold DISTANCE_THRESHOLD]
                           [--cluster_names CLUSTER_NAMES]
                           [--subcluster_thresholds SUBCLUSTER_THRESHOLDS]
                           [--subcluster_names SUBCLUSTER_NAMES]
                           [--external_cluster_names EXTERNAL_CLUSTER_NAMES]
                           [--serovar_info SEROVAR_INFO] [--noreport]
                           [--report_only] [--cluster]
                           [--distance_method DISTANCE_METHOD]
                           [--species_shortname SPECIES_SHORTNAME]
                           [--use_conda] [--conda_frontend]
                           [--condaprefix CONDAPREFIX] [--threads THREADS]
                           [--force FORCE] [--forceall FORCEALL]
                           [--profile PROFILE] [--dryrun] [--unlock]
                           [--logdir LOGDIR] [--version]

optional arguments:
  -h, --help            show this help message and exit
  --sample_list SAMPLE_LIST, -l SAMPLE_LIST
                        [REQUIRED] List of samples to analyze, as a two column
                        tsv file with columns sample and assembly. Can be
                        generated with provided script create_sampleSheet.sh
  --working_directory WORKING_DIRECTORY, -d WORKING_DIRECTORY
                        [REQUIRED] Working directory where results are saved
  --snakefile SNAKEFILE, -s SNAKEFILE
                        Path to Snakefile of chewieSnake pipeline, default is
                        path to Snakefile in same directory
  --clustering_method CLUSTERING_METHOD
                        The agglomeration method to be used for hierarchical
                        clustering. This should be (an unambiguous
                        abbreviation of) one of "ward.D", "ward.D2", "single",
                        "complete", "average" (= UPGMA), "mcquitty" (= WPGMA),
                        "median" (= WPGMC) or "centroid" (= UPGMC); default:
                        single
  --distance_threshold DISTANCE_THRESHOLD
                        A single distance threshold for the exctraction of
                        sub-trees; default: 10
  --cluster_names CLUSTER_NAMES
                        A file with potential names for cluster names, one
                        name per line, default:
                        repo_path/scripts/cluster_names_reservoir.txt
  --subcluster_thresholds SUBCLUSTER_THRESHOLDS
                        A list of distance thresholds for subclustering;
                        default: [3]
  --subcluster_names SUBCLUSTER_NAMES
                        A list of name types for nomenclature of subclusters;
                        choose from "numerical" and "greek-alphabet"; one
                        entry for each subcluster_threshold; default: ["greek-
                        alphabet"]
  --external_cluster_names EXTERNAL_CLUSTER_NAMES, -e EXTERNAL_CLUSTER_NAMES
                        A tab seperated file with external cluster names, e.g.
                        Epi clusters. Must contain one representative sample
                        per cluster: sample<tab>cluster_name
  --serovar_info SEROVAR_INFO
                        A tab seperated file with serovar or clade names:
                        sample<tab>cluster_name
  --noreport            Do not create html report
  --report_only         Only create html report
  --cluster             Assign cluster type and cluster addresses
  --distance_method DISTANCE_METHOD
                        Grapetree distance method; default: 3
  --species_shortname SPECIES_SHORTNAME
                        Abbreviated Species name for cluster names; default:
                        SE
  --use_conda           Utilize "--useconda" option, i.e. all software
                        dependencies are available in a single env
  --conda_frontend      Do not use mamba but conda as frontend to create
                        individual module' software environments
  --condaprefix CONDAPREFIX, -c CONDAPREFIX
                        Path of default conda environment, enables recycling
                        of built environments; default:
                        <path_to_chewiesnake>/conda_env
  --threads THREADS, -t THREADS
                        Number of Threads/Cores to use. This overrides the
                        "<chewieSnake >/profiles" settings." "Note that samples
                        can only be processed sequentially due to the required
                        database access. However the allele calling can be
                        executed in parallel for the different loci
  --force FORCE         Snakemake force. Force recalculation of output (rule
                        or file) specified here
  --forceall FORCEALL   Snakemake force. Force recalculation of all steps
  --profile PROFILE     (A) Full path or (B) directory name under
                        "<chewieSnake_join>/profiles" of a Snakemake
                        config.yaml with Snakemake parameters, e.g. available
                        CPUs and RAM. Default: "bfr.hpc"
  --dryrun, -n          Snakemake dryrun. Only calculate graph without
                        executing anything
  --unlock              Unlock a Snakemake execution folder if it had been
                        interrupted
  --logdir LOGDIR       Path to directory where .snakemake output is to be
                        saved
  --version, -V         Print program version
```

</p>
</details>

## Output description

### chewieSnake

* cgmlst/distance_matrix.tsv: The combined and hashed allele profiles
* cgmlst/distance_matrix.tsv: The allele distance matrix computed with grapetree (grapetree's method can be changed with --distance_method ); in grapetree format
* cgmlst/grapetree_mstree: The minimum spanning tree provided by grapetree
* reports/allele_distance_matrix.tsv: allele distance matrix reformated for use in excel etc.
* reports/report.html: Interactive html report containing allele statistics, allele distance table, allele distance matrix, cluster types and cluster address, and visualization of the minimum-spanning tree
* Reports for the provided test data are available in this repo:
  * [cgMLST report](https://bfr_bioinformatics.gitlab.io/chewieSnake/report_chewiesnake.html)
  * [cgMLST comparison report](https://bfr_bioinformatics.gitlab.io/chewieSnake/report_chewiesnake_comparison.html)

##### JSON structure

The hashed allele profile, technical run metadata and statistics are exported as a JSON file for each sample (e.g. `cgmlst/json/SAMPLENAME.chewiesnake.json`):

```
.
├── run_metadata:
│   ├── pipeline: chewieSnake
│   ├── timestamp: '2023-05-16 17:15:06.845109'
│   ├── sample_description:
│   │   ├── sample: GMI15-001-DNA
│   │   ├── fasta_file: "/chewieSnake/test_data/chewiesnake/data/GMI15-001-DNA.fasta"
│   │   ├── fasta_md5: 71d25d82b94d24759f5daf7d4d21cab7
│   │   ├── allele_profile: "/chewieSnake/analysis/cgmlst/GMI15-001-DNA/allele_profiles.tsv"
│   │   ├── allele_profile_md5: 437543bc0f2453bbf1bd5c4e379c95fb
│   │   └── timestamp_analysis: '2023-05-16'
│   ├── pipeline_metadata:
│   │   ├── host_system: 0dac3163eac2
│   │   └── config:     # here, the config_chewiesnake.yaml is appended
│   │       ├── pipeline: chewieSnake
│   │       ├── version: '3.2'
│   │       ├── git_version: '3.2'
│   │       ├── workdir: "/chewieSnake/analysis"
│   │       ├── samples: "/chewieSnake/analysis/samples.tsv"
│   │       ├── smk_params:
│   │       │   ├── log_directory: "/chewieSnake/analysis"
│   │       │   ├── profile: bfr.hpc
│   │       │   ├── rule_directives: "/chewieSnake/profiles/smk_directives.yaml"
│   │       │   └── threads: 4
│   │       └── parameters:
│   │           ├── chewbbaca:
│   │           │   ├── bsr_theshold: 0.6
│   │           │   ├── size_threshold: 0.2
│   │           │   ├── scheme_dir: "/chewieSnake/schema/Salmonella_enterica/Salmonella_enterica_INNUENDO_cgMLST"
│   │           │   ├── prodigal_training_file: "/chewieSnake/schema/Salmonella_enterica/Salmonella_enterica_INNUENDO_cgMLST/Salmonella_enterica.trn"
│   │           │   ├── allele_caller:
│   │           │   │   ├── name: chewBBACA
│   │           │   │   └── version: 2.0.12
│   │           │   ├── allele_hasher: CRC32 unsigned integer
│   │           │   └── max_fraction_missing_loci: 0.05
│   │           ├── clustering:
│   │           │   ├── clustering_method: single
│   │           │   ├── distance_threshold: 10
│   │           │   ├── address_range: '1,5,10,20,50,100,200,1000'
│   │           │   ├── comparison_allele_database: 'NULL'
│   │           │   ├── joining_threshold: 30
│   │           │   └── grapetree_distance_method: 3
│   │           └── frameshift_removal:
│   │               ├── remove_frameshifts: false
│   │               ├── mode: relative
│   │               └── threshold: 0.1
│   └── database_information:
│       ├── scheme_name: Salmonella_enterica_INNUENDO_cgMLST
│       ├── species: Salmonella_enterica
│       ├── scheme_dir: "/chewieSnake/schema/Salmonella_enterica/Salmonella_enterica_INNUENDO_cgMLST"
│       ├── scheme_md5: 437543bc0f2453bbf1bd5c4e379c95fb
│       └── count_loci: 3255
├── allele_profile: [type: list]   
│   ├── locus: "SC0831.fasta"
│   ├── allele_crc32: 2486472784
│   ├── locus: "SEN0401.fasta"
│   ├── allele_crc32: 2964597398
│   └── ...
└── allele_stats:
    ├── EXC: 2928
    ├── INF: 0
    ├── LNF: 43
    ├── PLOT: 0
    ├── NIPH: 18
    ├── ALM: 8
    ├── ASM: 3
    ├── loci_found: 2928
    ├── loci_missing: 72
    ├── loci_total: 3000
    ├── loci_missing_fraction: 0.024
    ├── hashid: 3631736318
    ├── max_fraction_missing_loci: 0.05
    └── allele_qc: PASS
```

### Extra output for comparison_db option

* reports/cgmlst_comparison_report.html: Interactive clustering report including a comparison and clustering with the allele database specified in --comparison_db (see example [here](https://bfr_bioinformatics.gitlab.io/chewieSnake/report_chewiesnake_comparison.html))
* cgmlst_joined/match_summary.txt: Summary of all matches below joining threshold between query and comparison data
* cgmlst_joined/samples_related_meta.tsv: Table of query and matched comparison db samples
* cgmlst_joined/distance_matrix_filtered.tsv: Distance matrix of matching data
* cgmlst_joined/grapetree_mstree_filtered.tre: Minimum spanning tree of matching data

### chewieSnake_join

* all merged data can be found in `merged_db`
* allele_profiles.tsv: The joined allelel profiles of all partners
* distance_matrix.tsv: The distance matrix resulting from allele_profiles.tsv
* grapetree_mstree.tre: The minimum-spanning tree from allele_profiles.tsv
* report.html: An interactive HTML report with all information on clusters, sample association to clusters, orphan clusters, and much more
* sample_cluster_information.tsv: A tabular file containing all essential information on the performed clustering
* sender_sample_info.tsv: A file relating each sample to the laboratory of origin
* The directory `clustering` contains the detailed information per cluster

## Test execution on Salmonella test data

Test data is provided within this repository (and also with the conda package and the docker container).
It is also available [here](https://gitlab.bfr.berlin/bfr_bioinformatics/chewiesnake_resources/-/raw/main/testdata/chewiesnake/assemblies.tar.gz).
Please also review the "[Automated Testing](Automated Testing)" section

0. Define paths of data and software

```
chewiesnake_path= "" # <path_to_chewiesnake>
testDIR=path/to/test_data
```

1. Download Salmonella cgMLST scheme from [here](https://gitlab.bfr.berlin/bfr_bioinformatics/chewiesnake_resources/-/raw/main/cgmlst_schemes/Salmonella_enterobase_senterica_cgmlst.zip) (see above):

```
mkdir -p <path_to_chewiesnake>/schema && cd <path_to_chewiesnake>/schema
wget -O Salmonella_enterobase_senterica_cgmlst.zip https://gitlab.bfr.berlin/bfr_bioinformatics/chewiesnake_resources/-/raw/main/cgmlst_schemes/Salmonella_enterobase_senterica_cgmlst.zip
unzip Salmonella_enterobase_senterica_cgmlst.zip
```

You should now have a folder `enterobase_senterica_cgmlst`

2. Download and extract the test data

```sh
cd $testDIR
wget -O assemblies.tar.gz https://gitlab.bfr.berlin/bfr_bioinformatics/chewiesnake_resources/-/raw/main/testdata/chewiesnake/assemblies.tar.gz
tar -xzvf assemblies.tar.gz
```

NOTE: the data is also contained in <path_to_chewiesnake>/test_data/chewiesnake/data

3. Create sample sheet

Navigate to the test data directory (either in the repository or downloaded from [here](https://gitlab.com/bfr_bioinformatics/chewieSnake/-/tree/master/test_data/chewiesnake/data))

```
create_sampleSheet.sh --mode assembly
```

A file samples.tsv is returned.

4. Make a dry-run

```
conda activate chewiesnake
chewieSnake  -t 10 -l $testDIR/assemblies/samples.tsv -d $testDIR/chewieSnake --scheme <path_to_chewiesnake>/schema/enterobase_senterica_cgmlst --prodigal <path_to_chewiesnake>/chewBBACA/CHEWBBACA/prodigal_training_files/Salmonella_enterica.trn --dryrun
```

The screen now displays a number of rules to be executed.

NOTE: If you cloned the repository, chewieSnake is available via `<path_to_chewiesnake>/chewieSnake.py`.

5. Run

```
conda activate chewiesnake
<path_to_chewiesnake>/chewieSnake.py  -t 10 -l $testDIR/assemblies/samples.tsv -d $testDIR/chewieSnake --scheme <path_to_chewiesnake>/schema/enterobase_senterica_cgmlst --prodigal <path_to_chewiesnake>/chewBBACA/CHEWBBACA/prodigal_training_files/Salmonella_enterica.trn
```

If the final output reads "Workflow finished, no error" everything went fine.

## Test of chewieSnake_join

Test data is supplied under https://gitlab.com/bfr_bioinformatics/chewieSnake/-/tree/master/test_data/chewiesnake_join/data

1. create an allele sheet

```
./create_alleledbSheet.sh {path2repo}/test_data/chewiesnake_join/data
```

This creates an allele sheet which is a tabular file with columns sender profiles statistics timestamps

* The values in sender correspond to the desired partner names
* The paths in profiles are the paths to the (hashed) alelle profiles of each partner
* The paths in profiles are the paths to the allele statistics of each partner. These files must exist but can be empty
* The paths in profiles are the paths to time stamps of each partner (produced by chewieSnake) but can also describe the sampling dates in the format YYYY-MM-DD (e.g. 2020-01-01)

The resulting tab separated file `samples.tsv` is the input for the chewieSnake_join workflow.

2. Call the pipeline

* Please adapt all paths to your repo path
* Activate your conda environment

Call:

```
./chewieSnake_join.py -l {path2repo}/test_data/chewiesnake_join/data/samples.tsv -d {path2repo}/test_data/chewiesnake_join/test_results --cluster --serovar_info {path2repo}/test_data/chewiesnake_join/sample_serovars.tsv --external_cluster_names {path2repo}/test_data/chewiesnake_join/epiclusters.tsv --threads 2 -n
```

Example reports for chewieSnake join (for the provided test data) can be found here:

* [overview report](https://bfr_bioinformatics.gitlab.io/chewieSnake/report_chewiesnake_join.html)
* [cluster report](https://bfr_bioinformatics.gitlab.io/chewieSnake/clustering/CT_0004/clusterreport.html)

## Validating of results

You can validate your results e.g. by comparing to the test data.

### Comparing the distance matrix

The distance matrix of the test data for comparison is available [here](https://gitlab.com/bfr_bioinformatics/chewieSnake/-/blob/master/test_data/chewiesnake/results/distance_matrix.tsv).

```
scripts/compare_distancematrices.R --matrix1 DISTANCEMATRIX_1 --matrix2 DISTANCEMATRIX_2 --outdir OUTDIR
```

The script expects two distances matrices of the same form (e.g. both either tab or space separated). See `scripts/compare_distancematrices.R --help` for all options.

It outputs two files

* `distance_comparison_table_all.tsv` All pairwise distances with matching samples from both distance matrices
* `distance_comparison_table_subset_belowthreshold_withmethoddifference.tsv`: Subset of all distances that are below the defined threshold and that are non-zero

### Comparing the clustering

You can also compare the clustering of your results to clustering obtained by a different method or threshold. Given a distance matrix, you can compute the clustering using

```
Clustering_DistanceMatrix.R --input DISTANCE_MATRIX --outputdirectory OUTPUTDIRECTORY --cutoff_values 5,10,20,50,100,200,1000
```

See `Clustering_DistanceMatrix.R --help` for more information.

It returns a tsv-file with the cluster membership of each sample at all supplied thresholds. This file (merged with a similar file from another method) can be the input for a clustering comparison using e.g.  http://www.comparingpartitions.info.

The cluster addresses of the test data can be found [here](https://gitlab.com/bfr_bioinformatics/chewieSnake/-/blob/master/test_data/chewiesnake/results/cluster_addresses.tsv), although the small sample size will not provide a good clustering comparison data set.

## Standalone scripts

Some modules from chewieSnake are available as standalone scripts and can be run regardless of the chewieSnake workflow.

* Allele hashing

```
alleleprofile_hasher.py --profile PROFILE --database DATABASE --outfile OUTFILE
```

See `alleleprofile_hasher.py --help` for more information.

* Hashed Sequence type

```
hashID.py --profile PROFILE --outfile OUTFILE
```

See `hashID.py --help` for more information.

* Clustering

```
Clustering_DistanceMatrix.R --input DISTANCE_MATRIX --outputdirectory OUTPUTDIRECTORY --cutoff_values 5,10,20,50,100,200,1000
```

See `Clustering_DistanceMatrix.R --help` for more information.

Note: If chewieSnake was not installed via conda, they are available in the `scripts/` folder.

### Automated Testing

```
bash scripts/chewieSnake_test.sh -n         # dryrun of the testing script with helpful environment information
bash scripts/chewieSnake_test.sh --dryrun   # dryrun of the snakemake workflows, this already creates the directories
bash scripts/chewieSnake_test.sh --auto     # non-interactive testing of all modules
```

Un-comment modules in the `Main` [section](scripts/chewieSnake_test.sh) of the script for more tests.

### Running chewieSnake in Batches

```
bash scripts/chewieSnake_wrapper.sh -n         # dryrun of the batch script with helpful environment information
bash scripts/chewieSnake_wrapper.sh --dryrun   # dryrun of the snakemake workflows, this already creates the directories
bash scripts/chewieSnake_wrapper.sh --auto     # non-interactive testing of all modules
```

The wrapper script along with the [run table chewieSnake_runs.tsv](scripts/chewieSnake_runs.tsv) allows to run single or consecutive rounds of chewieSnake.
Non-dryrun script executions along with detailed environment information will be logged to scripts/chewieSnake_wrapper.log.

1. **Manual Mode**: the variable `dir_data` in the `define_paths()` function of `chewieSnake_wrapper.sh` _is set_.
  The module selector `run_modules()` will be called only once.
2. **Batch Mode**: the variable `dir_data` in the `define_paths()` function of `chewieSnake_wrapper.sh` _is not set_.
  The module selector `run_modules()` will be called on every run (every line not starting with a #) in the table `chewieSnake_runs.tsv`.

## ⚖️ License

ChewieSnake is distributed under the terms of the BSD 3-Clause License.

## ✉️ Authors and Contributors

* Carlus Deneke (@Deneke)
* Holger Brendebach (@Brendy)
* Simon Tausch (@SimonHTausch)

## References

* snakemake: https://snakemake.readthedocs.io/en/stable/
  Köster, Johannes and Rahmann, Sven. “Snakemake - A scalable bioinformatics workflow engine”. Bioinformatics 2012.
* chewBBACA: https://github.com/B-UMMI/chewBBACA
  Silva M, Machado M, Silva D, Rossi M, Moran-Gilad J, Santos S, Ramirez M, Carriço J. 15/03/2018. M Gen 4(3): doi:10.1099/mgen.0.000166
* sistr: https://github.com/phac-nml/sistr_cmd
  Citation: The Salmonella In Silico Typing Resource (SISTR): an open web-accessible tool for rapidly typing and subtyping draft Salmonella genome assemblies. Catherine Yoshida, Peter Kruczkiewicz, Chad R. Laing, Erika J. Lingohr, Victor P.J. Gannon, John H.E. Nash, Eduardo N. Taboada. PLoS ONE 11(1): e0147101. doi: 10.1371/journal.pone.0147101
* grapetree: https://github.com/achtman-lab/GrapeTree
  Z Zhou, NF Alikhan, MJ Sergeant, N Luhmann, C Vaz, AP Francisco, JA Carrico, M Achtman (2018) "GrapeTree: Visualization of core genomic relationships among 100,000 bacterial pathogens", Genome Res; doi: https://doi.org/10.1101/gr.232397.117

### cgMLST databases

* Enterobase: http://enterobase.warwick.ac.uk/
* chewBBACA: http://chewbbaca.online/
* cgmlst.org: https://www.cgmlst.org/ncs
* BIGSdb: https://bigsdb.pasteur.fr/
