#!/usr/bin/env python3

# Python Standard Packages
import argparse
import subprocess
import os
import sys

# Other Packages
from yaml import dump as yaml_dump


# %% Functions

def argument_parser(pipeline: str = "chewieSnake",
                    repo_path: str = os.path.dirname(os.path.realpath(__file__))) -> argparse:
    parser = argparse.ArgumentParser()
    # region # parse arguments
    # path arguments
    parser.add_argument('-l', '--sample_list', default = "/", type = os.path.abspath, help = '[REQUIRED] List of samples to analyze, as a two column tsv file with columns sample and assembly paths. Can be generated with provided script create_sampleSheet.sh')
    parser.add_argument('-d', '--working_directory', default = "/", type = os.path.abspath, help = '[REQUIRED] Working directory where results are saved')
    parser.add_argument('-s', '--snakefile', default = os.path.join(repo_path, "chewieSnake_join.smk"), help = 'Path to Snakefile of chewieSnake pipeline, default is path to Snakefile in same directory')
    # grapetree arguments
    parser.add_argument('--distance_method', default = 3, type = int, help = 'Grapetree distance method; default: 3')
    parser.add_argument('--clustering_method', default = "single",
                        help = 'The agglomeration method to be used for hierarchical clustering. This should be (an unambiguous abbreviation of) one of "ward.D", "ward.D2", "single", "complete", "average" (= UPGMA), "mcquitty" (= WPGMA), "median" (= WPGMC) or "centroid" (= UPGMC); default: "single"')
    parser.add_argument('--distance_threshold', default = 10, type = int, help = 'A single distance threshold for the extraction of sub-trees; default: 10')
    # TODO: add here, in config and as rule parser.add_argument('--address_range', help='A comma separated set of cutoff values for defining the clustering address (Default: 1,5,10,20,30,50,100,200,500,990,1000)', default="1,5,10,20,30,50,100,200,500,990,1000", required=False)
    # clustering parameters
    parser.add_argument('--cluster_names', default = os.path.join(repo_path, "scripts", "cluster_names_reservoir.txt"), help = 'A file with potential names for cluster names, one name per line, default: repo_path/scripts/cluster_names_reservoir.txt', )
    parser.add_argument('--subcluster_thresholds', default = [3], help = 'A list of distance thresholds for subclustering; default: [3]')
    parser.add_argument('--subcluster_names', default = ["greek-alphabet"], help = 'A list of name types for nomenclature of subclusters; choose from "numerical" and "greek-alphabet"; one entry for each subcluster_threshold; default: ["greek-alphabet"]')
    parser.add_argument('-e', '--external_cluster_names', default = os.path.join(repo_path, "scripts", "epi_clusters.tsv"), type = os.path.abspath,
                        help = 'A tab seperated file with external cluster names, e.g. Epi clusters. Must contain one representative sample per cluster: sample<tab>cluster_name ')
    parser.add_argument('--serovar_info', default = os.path.join(repo_path, "scripts", "sample2serovar.tsv"), type = os.path.abspath, help = 'A tab seperated file with serovar or clade names: sample<tab>cluster_name ')
    parser.add_argument('--species_shortname', default = "SE", help = 'Abbreviated Species name for cluster names; default: SE')
    parser.add_argument('--cluster', default = False, action = 'store_true', help = 'Assign cluster type and cluster addresses')
    # chewiesnake_join arguments
    parser.add_argument('--noreport', default = False, action = 'store_true', help = 'Do not create html report')
    parser.add_argument('--report_only', default = False, action = 'store_true', help = 'Only create html report')
    # conda arguments
    parser.add_argument('--use_conda', default = False, action = 'store_true', help = 'Utilize the Snakemake "--useconda" option, i.e. Smk rules require execution with a specific conda env')
    parser.add_argument('--conda_frontend', default = False, action = 'store_true', help = 'Do not use mamba but conda as frontend to create individual conda environments')
    parser.add_argument('--condaprefix', default = os.path.join(repo_path, "conda_env"), help = f'Path of default conda environment, enables recycling of built environments; default: {os.path.join(repo_path, "conda_env")}', metavar = 'CONDA_PREFIX')
    # cpu arguments
    parser.add_argument('-t', '--threads', default = 0, type = int,
                        help = f'Number of Threads/Cores to use. This overrides the "<{pipeline}>/profiles" settings. Note that samples can only be processed sequentially due to the required database access. However, the allele calling can be executed in parallel for the different loci')
    parser.add_argument('-p', '--threads_sample', default = 4, type = int, help = 'Number of Threads to use per sample in multi-threaded rules; default: 1', metavar = 'THREADS')
    parser.add_argument('-b', '--batch', default = "", type = str, help = 'Compute sample list in batches. Please state a fraction string, e.g. 1/3', metavar = 'FRACTION')
    parser.add_argument('-f', '--force', default = False, type = str, help = 'Snakemake force. Force recalculation of output (rule or file) specified here', metavar = 'RULE')
    parser.add_argument('--forceall', default = False, type = str, help = 'Snakemake force. Force recalculation of all steps', metavar = 'RULE')
    parser.add_argument('--profile', default = "none", type = str, help = f'(A) Full path or (B) directory name under "<{pipeline}>/profiles" of a Snakemake config.yaml with Snakemake parameters, e.g. available CPUs and RAM; default: "bfr.hpc"')
    parser.add_argument('-n', '--dryrun', default = False, action = 'store_true', help = 'Snakemake dryrun. Only calculate graph without executing anything')
    parser.add_argument('--unlock', default = False, action = 'store_true', help = 'Unlock a Snakemake execution folder if it had been interrupted')
    parser.add_argument('--logdir', default = "/", type = os.path.abspath, help = 'Path to directory where .snakemake output is to be saved')
    parser.add_argument('-V', '--version', default = False, action = 'store_true', help = 'Print program version')
    # endregion # parse arguments
    return parser


def evaluate_arguments(args: argparse.Namespace) -> argparse.Namespace:
    # version
    args.version_fixed = open(os.path.join(args.repo_path, 'VERSION'), 'r').readline().strip()
    try:
        args.version_git = subprocess.check_output(["git", "describe", "--always"], stderr = subprocess.DEVNULL, cwd = args.repo_path).decode('ascii').strip()
    except:
        args.version_git = args.version_fixed

    if args.version:
        print(f"{args.pipeline} version\t{args.version_fixed}")
        if not args.version_fixed == args.version_git:
            print(f"commit           version\t{args.version_git.lstrip('v')}")
        sys.exit(0)
    else:
        print(f"You are using {args.pipeline} version {args.version_fixed} ({args.version_git})")

    # check required arguments
    required_paths_to_check = {
        "-d/--working_directory": args.working_directory,
        "-l/--sample_list"      : args.sample_list,
    }
    missing_required_paths = {key: path for key, path in required_paths_to_check.items() if path == '/'}
    if missing_required_paths:
        for key, path in missing_required_paths.items():
            print(f"ERROR: the following argument is required: {key}")
        sys.exit(1)

    # check dependency path
    dependency_paths_to_check = {
        "-l/--sample_list"        : args.sample_list,
        "--cluster_names"         : args.cluster_names,
        "--external_cluster_names": args.external_cluster_names
    }
    missing_dependency_paths = {key: path for key, path in dependency_paths_to_check.items() if not os.path.exists(path)}
    if missing_dependency_paths:
        for key, path in missing_dependency_paths.items():
            print(f"ERROR: path for argument {key} does not exist: {path}")
        sys.exit(1)

    # create working directory if it does not exist
    if not os.path.exists(args.working_directory):
        os.makedirs(args.working_directory)

    # set snakemake logdir
    args.logdir = args.working_directory if args.logdir == "/" else args.logdir
    print(f"Snakemake logs are saved in {args.logdir}/.snakemake/log")

    # find snakemake profile: first check relative paths, then absolute paths to make args.profile absolute
    if os.path.exists(os.path.join(args.repo_path, "profiles", args.profile)):
        smk_profile = os.path.join(args.repo_path, "profiles", args.profile)
    elif os.path.exists(os.path.realpath(args.profile)):
        smk_profile = os.path.realpath(args.profile)
    elif os.path.exists(os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.join(args.repo_path, "profiles", "bfr.hpc")):
        smk_profile = os.path.join(args.repo_path, "profiles", "bfr.hpc")
    else:
        print(f"ERROR: path to Snakemake config.yaml {args.profile} does not exist")
        sys.exit(1)
    args.profile = smk_profile

    # other checks
    if os.stat(args.serovar_info).st_size == 0:
        print("ERROR: No serovar file provided")
        args.do_serovar_info = False
    else:
        args.do_serovar_info = True

    #  initialize sample2cluster file if necessary
    mapping_sample2clustername_file = os.path.join(args.working_directory, "merged_db", "mapping_sample2clustername.tsv")
    if not os.path.exists(mapping_sample2clustername_file):
        print(f"Initializing file: {mapping_sample2clustername_file}")
        if not os.path.exists(os.path.dirname(mapping_sample2clustername_file)):
            try:
                os.makedirs(os.path.dirname(mapping_sample2clustername_file))
            except:
                print("Directory merged_db could NOT be created")
                sys.exit(1)
        with open(mapping_sample2clustername_file, "w") as f:
            bytereturn = f.write("sample\tcluster_name\n")

    # return modified args
    return args


def create_config(args: argparse.Namespace) -> dict:
    # region # create config dictionary
    config_dict = {
        'pipeline'   : args.pipeline,
        'version'    : args.version_fixed,
        'git_version': args.version_git,
        'workdir'    : args.working_directory,
        'samples'    : args.sample_list,
        'smk_params' : {
            'log_directory': args.logdir,
            'profile'      : args.profile,
            'threads'      : args.threads_sample,
        },
        'parameters' : {  # TODO: refactor to params
            'clustering_method'             : args.clustering_method,
            'distance_threshold'            : args.distance_threshold,
            'grapetree_distance_method'     : args.distance_method,
            'cluster_names_reservoir'       : args.cluster_names,
            'subcluster_distance_thresholds': args.subcluster_thresholds,
            'subcluster_name_types'         : args.subcluster_names,
            'external_cluster_names'        : args.external_cluster_names,
            'serovar_info'                  : args.serovar_info,
            'do_serovar_info'               : do_serovar_info,
            'species_shortname'             : args.species_shortname
        }
    }
    # endregion # create config dictionary

    # write config dictionary to yaml
    with open(args.config_file, 'w') as outfile:
        yaml_dump(data = config_dict, stream = outfile, default_flow_style = False, sort_keys = False)
    print(f"Config file saved to {args.config_file}")

    return config_dict


def build_snakemake_call(args: argparse.Namespace) -> str:
    # force mode
    force_mode = "--forceall" if args.forceall else ""
    force_mode = "--force" if args.force else force_mode

    # user-specified force target; force_targets are mutually exclusive
    force_target = args.forceall if args.forceall else ""
    force_target = args.force if args.force else force_target

    # all-rule force target, add new all rules here
    force_target = "all_noreport" if args.noreport else force_target
    force_target = "all_clustering" if args.cluster else force_target
    force_target = "all_reportonly" if args.report_only else force_target
    force = f"{force_mode} {force_target}"

    # other workflow options
    threads = "--cores" if args.profile == "none" else ""  # use all available cores, if no profile is specified
    threads = f"--cores {args.threads}" if args.threads > 0 else threads  # override cores by commandline arg if provided
    dryrun = "--quiet --dryrun" if args.dryrun else ""  # TODO: add rules to --quiet, once snakemake is updated
    unlock = "--unlock" if args.unlock else ""

    # create command
    command = f"snakemake --profile {args.profile} --snakefile {args.snakefile} " \
              f"--configfile {args.config_file} " \
              f"{batch} {threads} {dryrun} {unlock} {force}"

    if args.use_conda:
        frontend = "conda" if args.conda_frontend else "mamba"
        command += f" --use-conda --conda-prefix {args.condaprefix} --conda-frontend {frontend}"

    return command


# %% Main

def main():
    # create commandline parser
    parser = argument_parser()

    # parse arguments and create namespace object
    args = parser.parse_args()

    # append settings to args namespace
    args.pipeline = "chewieSnake_join"
    args.repo_path = os.path.dirname(os.path.realpath(__file__))
    args.config_file = os.path.join(args.working_directory, "config_chewiesnake.yaml") if args.config == "/" else args.config

    # evaluate arguments
    args = evaluate_arguments(args = args)

    # write a new config file
    config = create_config(args = args)

    # run snakemake workflow
    smk_command = build_snakemake_call(args = args)
    print(smk_command)
    subprocess.call(args = smk_command, shell = True, cwd = args.logdir)  # start snakemake from logdir

    # call again for checkpoints  # TODO: only if not an error
    if not args.dryrun:
        subprocess.call(args = smk_command, shell = True, cwd = args.logdir)  # start snakemake from logdir


# %% Main Call

if __name__ == '__main__':
    main()
